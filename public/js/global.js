(function($) {

    function Global() {
        this.selectors = Global._selectors;
        this.templates = Global._templates;
        this.messages = Global._messages;

        this.$checkboxes = $(this.selectors.checkbox);
        this.$selects = $(this.selectors.select);
        this.$calendars = $(this.selectors.calendars);
        this.$calendarControls = $(this.selectors.calendarControls);
        this.$adminCurrentItems = $(this.selectors.adminCurrentItems);
        this.$adminDeleteItem = $(this.selectors.adminDeleteItem);
        this.$adminEditItem = $(this.selectors.adminEditItem);
        this.$modalMessageBox = $(this.selectors.modalMessageBox);
        this.$modalConfirmBox = $(this.selectors.modalConfirmBox);

        this.init();
    }

    Global._selectors = {
        checkbox: 'input[type="checkbox"]',
        select: 'select',
        validatedFields: 'input,select,textarea,label',
        submitFields: '[type=submit]',
        jqueryDropDownSelect: '.jq-selectbox__dropdown',
        calendars: '.calendar input',
        calendarControls: '.calendar .controls',
        adminCurrentItems: '.container.row-fluid > .span4 li',
        adminDeleteItem: '#subtract',
        adminEditItem: '#edit',
        modalMessageBox: '#messageBox',
        modalConfirmBox: '#confirmBox',
        modalTextBlock: '.modal-body p',
        modalDeleteButton: '.modal-footer .btn.delete'
    };

    Global._templates = {
        emptyOption: '<option value=""></option>',
        emptyListItem: '<li></li>'
    };

    Global._messages = {
        selectItem: 'Select an item from the list',
        palatiumCareData: 'Sorry, you can\'t modify data from Palatium Care',
        deleteConfirm: 'Are you sure you want to delete it ?'
    };

    Global.prototype.init = function() {
        this.$checkboxes.styler();
        this.$selects.styler();
        this.$selects.each(this.prepareSelects);
        this.$calendars.datepicker();
        this.$adminCurrentItems.on('click', $.proxy(this.onAdminListItemsClick, this));
        this.$adminDeleteItem.on('click', $.proxy(this.onAdminDeleteItemClick, this));
        this.$adminEditItem.on('click', $.proxy(this.onAdminEditItemClick, this));
        this.$calendarControls.on('click', $.proxy(this.onCalendarControlClick, this));
        $(this.selectors.validatedFields).not(this.selectors.submitFields).jqBootstrapValidation();
    };

    Global.prototype.onAdminListItemsClick = function(e) {
        this.$adminCurrentItems.removeClass('active');

        var element = $(e.currentTarget);
        element.addClass('active');
        this.adminListItemId = element.data('id');
        this.palatiumCareId = element.data('palatium-care-id');

        this.$adminDeleteItem.attr(
            'href',
            this.$adminDeleteItem.attr('href').replace(/\/delete\/(\d*$)/, '/delete/' + this.adminListItemId)
        );

        this.$adminEditItem.attr(
            'href',
            this.$adminEditItem.attr('href').replace(/\/edit\/(\d*$)/, '/edit/' + this.adminListItemId)
        );
    };

    Global.prototype.onAdminDeleteItemClick = function(e) {
        var element =  $(e.currentTarget);
        element.blur();

        if (this.checkAdminListItem()) {
            this.$modalConfirmBox.find(this.selectors.modalTextBlock).text(this.messages.deleteConfirm);
            this.$modalConfirmBox.find(this.selectors.modalDeleteButton).attr('href', element.attr('href'));
            this.$modalConfirmBox.modal();
        }

        return false;
    };

    Global.prototype.onAdminEditItemClick = function(e) {
        $(e.currentTarget).blur();

//        return this.checkAdminListItem();
    };

    Global.prototype.checkAdminListItem = function() {
        if (this.adminListItemId === undefined) {
            this.$modalMessageBox.find(this.selectors.modalTextBlock).text(this.messages.selectItem);
            this.$modalMessageBox.modal();

            return false;
        }

        if (this.palatiumCareId) {
            this.$modalMessageBox.find(this.selectors.modalTextBlock).text(this.messages.palatiumCareData);
            this.$modalMessageBox.modal();

            return false;
        }

        return true;
    };

    Global.prototype.onCalendarControlClick = function(e) {
        $(e.currentTarget).find('input').trigger('focus');
    };

    Global.prototype.prepareSelects = function() {
        if($(this).find('option').length < 1 ){
            $(this).append(this.templates.emptyOption);
            $(this).parent().find(Global.selectors.jqueryDropDownSelect).find('ul').append(this.templates.emptyListItem);
        }
    };

    $(function() {
        new Global();
    });

})(jQuery);