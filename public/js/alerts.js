(function($) {

    function Alerts(element, serverDateTime, selectors, alert) {
        this.$element = element;
        this.selectors = $.extend({}, Alerts._selectors, selectors);
        this.messages = Alerts._messages;
        this.serverDateTime = serverDateTime;
        this.emulatedDateTime = null;

        this.emulatedButtonID = "f4bc12";
        this.emulatedLocation = "Test Location";
        this.updateTimeInterval = 5000;
        this.timeCounterInterval = 1000;

        this.urls = {
            getAlerts: "/"+alert+"/update",
            createAlert: '/receiver',
            resetAlert: '/receiver'
        };

        this.$activeAlerts = $(this.selectors.activeAlerts);
        this.$onlineUsers = $(this.selectors.onlineUsers);
        this.$generateAlert = $(this.selectors.generateAlert);
        this.$resetAlert = $(this.selectors.resetAlert);
        this.$modalMessageBox = $(this.selectors.modalMessageBox);
        this.$modalConfirmBox = $(this.selectors.modalConfirmBox);
        this.$allCheckbox = $(this.selectors.allCheckbox);

        this.init();
    }

    Alerts._selectors = {
        activeAlerts: '#active-alerts',
        onlineUsers: '#users-online',
        generateAlert: '#generate-alert',
        resetAlert: '#reset-alert',
        deleteAlert: '.delete-alert',
        tableRows: 'tr:not(:first)',
        tableFirstColumn: 'td:first-child',
        modalMessageBox: '#messageBox',
        modalConfirmBox: '#confirmBox',
        modalTextBlock: '.modal-body p',
        modalDeleteButton: '.modal-footer .btn.delete',
        deviceForm: '#device-form',
        allCheckbox: 'input[name="allCheckbox"]'
    };

    Alerts._messages = {
        deleteConfirm: 'Are you sure you want to delete it ?'
    };

    Alerts.prototype.init = function() {
        this.$generateAlert.bind('click', $.proxy(this.onGenerateAlertClick, this));
        this.$resetAlert.bind('click', $.proxy(this.onResetAlertClick, this));
        this.$activeAlerts.on('click', this.selectors.deleteAlert, $.proxy(this.onDeleteAlertClick, this));
        this.$allCheckbox.on('change', $.proxy(this.onAllCheckboxChange, this));
        this.setTime(this.serverDateTime);
        setInterval($.proxy(this.updateData, this), this.updateTimeInterval);
        setInterval($.proxy(this.timeCounter, this), this.timeCounterInterval);
    };

    Alerts.prototype.onGenerateAlertClick = function() {
        this.emulatedDateTime = (new Date()).toISOString();
        $.ajax({
            url: this.urls.createAlert,
            type: 'POST',
            context: this,
            data: JSON.stringify(
                {
                    "buttonID": this.emulatedButtonID,
                    "location": this.emulatedLocation,
                    "deviceType": "Push Alert",
                    "type": "Alert",
                    "residents": [{"firstName": "John", "lastName" : "Doe"}],
                    "buttonPress": true,
                    "buttonLowBattery": false,
                    "near": "Test near",
                    "dateTime": this.emulatedDateTime
                }
            )
        });
    };

    Alerts.prototype.onResetAlertClick = function() {
        $.ajax({
            url: this.urls.resetAlert,
            type: 'POST',
            context: this,
            data: JSON.stringify(
                {
                    "buttonID": this.emulatedButtonID,
                    "buttonPress": true,
                    "resetPress": true,
                    "buttonLowBattery": false,
                    "dateTime": this.emulatedDateTime
                }
            )
        });
    };

    Alerts.prototype.onDeleteAlertClick = function(e) {
        this.$modalConfirmBox.find(this.selectors.modalTextBlock).text(this.messages.deleteConfirm);
        this.$modalConfirmBox.find(this.selectors.modalDeleteButton).attr('href', $(e.currentTarget).attr('href'));
        this.$modalConfirmBox.modal();

        return false;
    };

    Alerts.prototype.onAllCheckboxChange = function(e) {
        $('input[name="devicesIds[]"]').prop('checked', $(e.currentTarget).prop('checked')).trigger('refresh');
    };

    Alerts.prototype.updateData = function() {
        $.ajax({
            url: this.urls.getAlerts,
            type:'POST',
            context:this,
            data: {
                "devicesIds": this.getDevicesIds()
            },
            success: function(data) {
                data = JSON.parse(data);
                this.$activeAlerts.html(data.alerts);
                this.$onlineUsers.html(data.users);
                this.setTime(data.serverDateTime);
            }
        });
    };

    Alerts.prototype.setTime = function(serverDateTime) {
        this.$element.find(this.selectors.tableRows).each(function() {
            var _time = $(this).find('td').eq(6).text();
            if(!_time) {
                _time = $(this).find('td').eq(5).text()
                    ? $(this).find('td').eq(5).text()
                    : $(this).find('td').eq(4).text();
            }

            if (_time.indexOf(":") > 0) {
                $(this).find('td').eq(0).text(
                    Alerts.prototype.secondsToString((new Date(serverDateTime) / 1000) - (new Date(_time) / 1000))
                );
            }
        })
    };

    Alerts.prototype.timeCounter = function() {
        this.$element.find(this.selectors.tableFirstColumn).each(function() {
            var _time = $(this).text();
            $(this).text('');

            if (_time.length > 1) {
                var _minutes = parseFloat(_time.slice(-5,-3));
                var _seconds = parseFloat(_time.slice(-2)) + 1;
                var _hours = _time.length > 6 ? parseFloat(_time) + ":" : "";

                if (_seconds > 59) {
                    _seconds = 0;
                    _minutes += 1;
                }

                if (_minutes > 59) {
                    _minutes = 0;
                    _hours += 1;
                }

                if (_hours.length < 3 && _hours.length > 1) {
                    _hours = '0' + _hours;
                }

                if (_minutes < 10 && _minutes > -1) {
                    _minutes = '0' + _minutes
                }

                if (_seconds < 10 && _seconds > -1) {
                    _seconds = '0' + _seconds;
                }

                $(this).text(_hours + _minutes + ':' + _seconds);
            }
        });
    };

    Alerts.prototype.secondsToString = function(seconds) {

        var _hours = Math.floor(((seconds % 31536000) % 86400) / 3600);
        var _minutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
        var _seconds = (((seconds % 31536000) % 86400) % 3600) % 60;

        _hours = _hours.toString().length < 2 ? '0' + _hours : _hours;
        _minutes = _minutes < 10 ? '0' + _minutes : _minutes;
        _seconds = _seconds < 10 ? '0' + _seconds : _seconds;

        return _hours + ":" + _minutes + ":" + _seconds;
    };

    Alerts.prototype.getDevicesIds = function() {
        var devicesIds = [];
        $(this.selectors.deviceForm).find('input:checked').each(function() {
            devicesIds.push($(this).val());
        });

        return devicesIds;
    };

    $.fn.handleAlerts = function(serverDateTime, selectors, alert) {
        selectors = selectors || {};
        alert = alert == 'system' ? 'system' : 'index';

        new Alerts($(this), serverDateTime, selectors, alert);
    };
})(jQuery);
