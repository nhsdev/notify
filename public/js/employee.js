(function($) {

    function Employee(element, role, mobileRole, selectors) {
        this.selectors = $.extend({}, Employee.selectors, selectors);
        this.$element = element;
        this.mobileRole = mobileRole;
        this.role = role;

        this.$voIpFieldsWrapper = element.find(this.selectors.voIpFieldsWrapper);
        this.$voIpRequiredFields = element.find(this.selectors.voIpRequiredFields);
        this.$rolesSelect = element.find(this.selectors.rolesSelect);

        this.init();
    }

    Employee.selectors = {
        voIpFieldsWrapper: '.voIp-fields-wrapper',
        voIpRequiredFields: '.voIp-fields-wrapper input[type=text]',
        rolesSelect: '[name=role]'
    };

    Employee.prototype.init = function() {
        this.displayVoIpFields();
        this.$rolesSelect.on('change', $.proxy(this.onRoleSelectChange, this));
    };

    Employee.prototype.onRoleSelectChange = function(e) {
        this.role = $(e.currentTarget).val();
        this.displayVoIpFields();
    };

    Employee.prototype.displayVoIpFields = function() {
        if (this.role == this.mobileRole) {
            this.$voIpFieldsWrapper.show(300);
            this.$voIpRequiredFields.attr('required', 'required');
        } else {
            this.$voIpFieldsWrapper.hide(300);
            this.$voIpRequiredFields.removeAttr('required');
        }
    };

    $.fn.handleEmployee = function(role, mobileRole, selectors) {
        selectors = selectors || {};

        new Employee($(this),role, mobileRole, selectors);
    };

})(jQuery);
