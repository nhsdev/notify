(function($) {

    function Reports(element, selectors) {
        this.selectors = $.extend({}, Reports.selectors, selectors);
        this.messages = Reports.messages;
        this.attributes = Reports.attributes;
        this.$element = element;

        this.$hoursUp = element.find(this.selectors.hoursUp);
        this.$hoursDown = element.find(this.selectors.hoursDown);
        this.$secondsUp = element.find(this.selectors.secondsUp);
        this.$secondsDown = element.find(this.selectors.secondsDown);
        this.$hoursInput = element.find(this.selectors.hoursInput);
        this.$secondsInput = element.find(this.selectors.secondsInput);
        this.$resetFieldsButton = element.find(this.selectors.resetFieldsButton);
        this.$searchButton = element.find(this.selectors.searchButton);
        this.$generateReportButton = element.find(this.selectors.generateReportButton);
        this.$menuItems = element.find(this.selectors.menuItems);

        this.init();
    }

    Reports.selectors = {
        hoursUp: '.hour .up',
        hoursDown: '.hour .down',
        secondsUp: '.seconds .up',
        secondsDown: '.seconds .down',
        hoursInput: '.hour input',
        secondsInput: '.seconds input',
        searchButton: '.btn.search, .btn.pdf, .btn.excel',
        generateReportButton: '.btn.pdf, .btn.excel',
        resetFieldsButton: '.btn.reset-search',
        dateField: 'input[name^="date"]',
        hoursField: 'input[name^="hours"]',
        minutesField: 'input[name^="minutes"]',
        formHorizontal: 'form.form-horizontal',
        selectedOption: 'option:selected',
        selectWithId: 'select[name$="Id"]',
        jquerySelect: '.jq-selectbox__select-text',
        menuItems: '.span2 ul li'
    };

    Reports.attributes = {
        classes: {
            active: 'active',
            iconWhite: 'icon-white'
        }
    };

    Reports.messages = {
        incorrectDateValue: 'Incorrect date value'
    };

    Reports.prototype.init = function() {
        this.$hoursUp.on('click', $.proxy(this.onHoursUpClick, this));
        this.$hoursDown.on('click', $.proxy(this.onHoursDownClick, this));
        this.$secondsUp.on('click', $.proxy(this.onSecondsUpClick, this));
        this.$secondsDown.on('click', $.proxy(this.onSecondsDownClick, this));
        this.$hoursInput.on('keyup', $.proxy(this.onHoursInputKeyUp, this));
        this.$secondsInput.on('keyup', $.proxy(this.onSecondsInputKeyUp, this));
        this.$resetFieldsButton.on('click', $.proxy(this.onResetFieldsButtonClick, this));
        this.$searchButton.on('click', $.proxy(this.onSearchButtonClick, this));
        this.$generateReportButton.on('click', $.proxy(this.onGenerateReportButtonClick, this));
        this.$menuItems.on('mouseenter', $.proxy(this.onMenuItemsMouseEnter, this));
        this.$menuItems.on('mouseleave', $.proxy(this.onMenuItemsMouseLeave, this));
    };

    Reports.prototype.onHoursUpClick = function(e) {
        var inputElement = $(e.currentTarget).parent().find('input');

        if (!inputElement.val()) {
            inputElement.val('1');
        } else if (inputElement.val() == 23) {
            inputElement.val('00');
        } else {
            inputElement.val(parseFloat(inputElement.val()) + 1);
        }
    };

    Reports.prototype.onHoursDownClick = function(e) {
        var inputElement = $(e.currentTarget).parent().find('input');

        if (!inputElement.val()) {
            inputElement.val('0');
        } else if (inputElement.val() == 0) {
            inputElement.val('23');
        } else {
            inputElement.val(parseFloat(inputElement.val()) - 1);
        }
    };

    Reports.prototype.onSecondsUpClick = function(e) {
        var inputElement = $(e.currentTarget).parent().find('input');

        if (!inputElement.val()) {
            inputElement.val('1');
        } else if (inputElement.val() == 59) {
            inputElement.val('00');
        } else {
            inputElement.val(parseFloat(inputElement.val()) + 1);
        }
    };

    Reports.prototype.onSecondsDownClick = function(e) {
        var inputElement = $(e.currentTarget).parent().find('input');

        if (!inputElement.val()) {
            inputElement.val('0');
        } else if (inputElement.val() == 0) {
            inputElement.val('59');
        } else {
            inputElement.val(parseFloat(inputElement.val()) - 1);
        }
    };

    Reports.prototype.onHoursInputKeyUp = function(e) {
        var element = $(e.currentTarget);

        element.val(element.val().replace(/[^0-9]/g, ''));
        if (element.val() > 23) {
            element.val(23);
        }
    };

    Reports.prototype.onSecondsInputKeyUp = function(e) {
        var element = $(e.currentTarget);

        element.val(element.val().replace(/[^0-9]/g, ''));
        if (element.val() > 59) {
            element.val(59);
        }
    };

    Reports.prototype.onResetFieldsButtonClick = function(e) {
        var $form = $(e.currentTarget).parents(this.selectors.formHorizontal);

        $form.find(this.selectors.dateField).attr('value', '');
        $form.find(this.selectors.hoursField).attr('value', '00');
        $form.find(this.selectors.minutesField).attr('value', '00');

        var select = $form.find(this.selectors.selectWithId);
        select.find(this.selectors.selectedOption).attr('value', '');
        select.next().find(this.selectors.jquerySelect).html('');
    };

    Reports.prototype.onSearchButtonClick = function(e) {
        var $form = $(e.currentTarget).parents(this.selectors.formHorizontal);

        var date = '';
        var errorMessage = this.messages.incorrectDateValue;
        var isValid = true;

        $form.find(this.selectors.dateField).each(function() {
            date = $(this).val();
            if (date && !Reports.prototype.validateDate(date)) {
                isValid = false;
                alert(errorMessage);
                return false;
            }

            return true;
        });

        return isValid;
    };

    Reports.prototype.onGenerateReportButtonClick = function(e) {
        var $form = $(e.currentTarget).parents(this.selectors.formHorizontal);
        var action = $form.attr('action');

        $form.attr('action', $(e.currentTarget).attr('formaction'));
        $form.submit();
        $form.attr('action', action);

        return false;
    };

    Reports.prototype.onMenuItemsMouseEnter = function(e) {
        var element = $(e.currentTarget).find('a');
        if (!element.hasClass(this.attributes.classes.active)) {
            element.find('i').addClass(this.attributes.classes.iconWhite);
        }

    };

    Reports.prototype.onMenuItemsMouseLeave = function(e) {
        var element = $(e.currentTarget).find('a');
        if (!element.hasClass(this.attributes.classes.active)) {
            element.find('i').removeClass(this.attributes.classes.iconWhite);
        }

    };

    Reports.prototype.validateDate = function(date) {
        return Boolean(date.match(/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d/));
    };

    $.fn.handleReports = function(selectors) {
        selectors = selectors || {};

        new Reports($(this), selectors);
    };
})(jQuery);