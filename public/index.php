<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
defined('APP_ENV') || define('APP_ENV', getenv('APP_ENV') ? getenv('APP_ENV') : 'prod');

defined('API_LOGS') || define('API_LOGS', true);
defined('PUSH_LOGS') || define('PUSH_LOGS', true);

defined('PROJECT_VERSION') || define('PROJECT_VERSION', '3.0');

chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
