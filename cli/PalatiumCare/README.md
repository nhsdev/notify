Palatium Care Interaction
=========================

Description
------------

There are two jobs to integrate with The Palatium Care:
Data synchronization (daily) and Alerts import (every five seconds)


### Synchronization

Sync Devices (buttons), Locations, Residents data from palatium care with notify DB.
Daily run. The following string must be added in crontab:

    0 2 * * * /usr/bin/php /var/www/notify/cli/PalatiumCare/palatium-care-interaction.php sync

Run every 5 minutes.
    */5 * * * * /usr/bin/php /var/www/notify/cli/PalatiumCare/palatium-care-interaction.php sync

So, Sync script will be running every day at 2am

### Alerts Import

Receive Alerts data every 5 minutes from Palatium Care. Based on this data, we create or close alerts through http method /receive.

We use the bash script to start this job (because cron allows only once a minute run):

    ./alerts-import.sh


To run alerts import bash script the following string must be added in /etc/init.d/alertsimp:

    #! /bin/sh
    ### BEGIN INIT INFO
    # Provides:          alerts-import.sh
    # Required-Start:    $remote_fs $syslog udev mysql
    # Required-Stop:     $remote_fs $syslog udev mysql
    # Default-Start:     2 3 4 5
    # Default-Stop:      0 1 6
    # Short-Description: alerts-import script load
    # Description:       Enable service provided by alerts-import.sh.
    ### END INIT INFO

    sh /var/www/notify/cli/PalatiumCare/alerts-import.sh &


Thus, When the system start, we run alerts-import script and initial db sync.

### Finally

    Every 5 seconds will be running alerts import script and a sync script will be running once a day by cron scheduler