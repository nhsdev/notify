<?php

class PalatiumCareInteraction
{
    private static $instance = null;

    /* config.json */
    private $config = array();
    /* config/autoload/connection.local.php */
    private $dbConfigPath = null;

    private $logPath = null;
    private $type = 'alerts-import';

    /** @var PDO */
    private $dbConnection;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {}

    private function init()
    {
        $notifyRootPath = dirname(dirname(__DIR__));

        $this->config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);
        $this->dbConfigPath = $notifyRootPath . '/config/autoload/connection.local.php';
        $this->logPath = $notifyRootPath . '/data/logs/cli';

        if ($this->config['log']['enabled']) {
            $this->checkLogFolder();
        }

        $this->setDbConnection();
    }

    private function setDbConnection()
    {
        if (!is_readable($this->dbConfigPath)) {
            $message = sprintf('Database config file: "%s" does not exist or has no rights to read', $this->dbConfigPath);
            $this->log($message);
            throw new InvalidArgumentException($message);
        }

        $config = include $this->dbConfigPath;

        $dbConfig = $config['doctrine']['connection']['orm_default']['params'];

        if (!is_array($dbConfig)) {
            throw new OutOfBoundsException(
                sprintf('Incorrect data format of the config file')
            );
        }

        $this->dbConnection = new PDO(
            sprintf("mysql:host=%s;dbname=%s", $dbConfig['host'], $dbConfig['dbname']),
            $dbConfig['user'],
            $dbConfig['password']
        );

        $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private function sendHttpRequest($url, $timeout, $type = 'get', $params = null, array $headers = null)
    {
        $this->log(sprintf('Send %s request on %s', strtoupper($type), $url));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        if ($type == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);
        curl_close($ch);

        $this->log('Response from ' . $url);
        $this->log($response);

        return $response;
    }

    private function checkLogFolder()
    {
        /* data/log folder check*/
        $commonLogFolderPath = dirname($this->logPath);
        if (!is_dir($commonLogFolderPath)) {
            umask(0);
            mkdir($commonLogFolderPath, 0777, true);
        }

        /* data/log/cli folder check */
        if (!is_dir($this->logPath)) {
            umask(0);
            mkdir($this->logPath, 0777, true);
        }
    }

    private function log($message)
    {
        if (!$this->config['log']['enabled']) {
            return;
        }

        $date = new DateTime();
        $logFilePath = sprintf(
            $this->config['log']['format'],
            $this->logPath, $this->type, $date->format('Y'), $date->format('m'), $date->format('d')
        );

        file_put_contents(
            $logFilePath,
            print_r(
                sprintf($this->config['log']['messageFormat'], $date->format('Y-m-d H:i:s'), $message) . PHP_EOL,
                true
            ),
            FILE_APPEND
        );
    }

    private function processAlerts()
    {
        $this->log('Start alerts import from Palatium Care --------------');

        $openedAlerts = $this->dbConnection
            ->query("SELECT a.palatium_care_id AS palatiumCareId, a.closed AS closed, b.public_id AS buttonId
                        FROM alerts a JOIN buttons b ON a.button_id = b.id
                    WHERE a.palatium_care_id IS NOT NULL"
            )->fetchAll(PDO::FETCH_ASSOC);

        try {
            $obtainedAlerts = $this->getAlerts(
                $this->sendHttpRequest(
                    $this->config['palatiumCareHost'] . $this->config['alertsImport']['action'],
                    $this->config['alertsImport']['responseTimeout'],
                    'post',
                    $this->config['alertsImport']['params']
                )
            );
        } catch (LogicException $exception) {
            $this->log('Fatal: can\'t import alerts from Palatium Care -------------');
            return;
        }

        $idsForInsert = array_keys($obtainedAlerts);
        $idsForClosing = [];

        foreach ($openedAlerts as $alert) {
            if (in_array($alert['palatiumCareId'], $idsForInsert)) {
                if (is_null($alert['closed'])) {
                    /* is not a new alert and must be removed */
                    unset($obtainedAlerts[$alert['palatiumCareId']]);
                }
            } elseif (is_null($alert['closed'])) {
                /* this alert must be closed */
                $idsForClosing[] = $alert['buttonId'];
            }
        }

        foreach ($idsForClosing as $btnId) {
          $this->closeAlert($btnId);
        }

        /* now $obtainedAlerts consists of only new alerts */
        $this->createNewAlerts($obtainedAlerts);
        $this->log('Finish alerts import from Palatium Care -------------');
    }

    private function getAlerts($data)
    {
        $columnNames = $this->config['alertsImport']['columns'];
        $result = array();

        foreach (explode("\n", $data) as $key => $row) {
            $alertData = explode("\t", $row);

            /* check column names match and continue if ok */
            if ($key == 0) {
                if (!$this->checkColumnNames($alertData, $columnNames)) {
                    throw new LogicException();
                }
                continue;
            }

            if ($key == 1 || empty(trim($row))) {
                continue;
            }

            if (sizeof($alertData) == sizeof($columnNames)) {
                $result[$alertData[0]] = array_combine($columnNames, $alertData);
            } else {
                $this->log('Incorrect sync data format');
            }

        }

        return $result;
    }

    private function createNewAlerts(array $alerts)
    {
        if (empty($alerts)) {
            return;
        }

        /* buttons -> near correspondence */
        $nearButtons = array();
        foreach ($alerts as $alert) {
            $nearButtons[(int) $alert['DeviceId']] = substr($alert['Near'], 0 , 255);
        }

        if (empty($nearButtons)) {
            return;
        }

        $buttons = $this->dbConnection->query(
            sprintf(
                'SELECT public_id as buttonId, palatium_care_id AS palatiumCareId FROM buttons WHERE palatium_care_id IN (%s)',
                implode(',', array_keys($nearButtons))
            )
        )->fetchAll(PDO::FETCH_ASSOC);

        foreach ($buttons as $button) {
            $this->sendHttpRequest(
                $this->config['notify']['url'],
                $this->config['notify']['responseTimeout'],
                'post',
                json_encode(
                    array(
                        'buttonID' => $button['buttonId'],
                        'palatiumCareId' => $button['palatiumCareId'],
                        'near' => $nearButtons[$button['palatiumCareId']]
                    )
                ),
                array('Content-Type:application/json')
            );
        }
    }

    private function closeAlert($buttonId)
    {
        return $this->sendHttpRequest(
            $this->config['notify']['url'],
            $this->config['notify']['responseTimeout'],
            'post',
            json_encode(array('buttonID' => $buttonId, 'resetPress' => true)),
            array('Content-Type:application/json')
        );
    }

    private function processSync()
    {
        $this->log('Start data sync with Palatium Care --------------');

        $locationsToSync = $this->getDataToSync('locations');
        $residentsToSync = $this->getDataToSync('residents');
        $devicesToSync = $this->getDataToSync('devices');

        $this->upsetLocations($locationsToSync);
        $this->upsetDevices($devicesToSync, $residentsToSync);

        $this->log('Finish data sync with Palatium Care -------------');
    }

    private function upsetLocations(array $locations)
    {
        if (empty($locations)) {
            return;
        }


//        $serverLocations = array_map(function($location) {
//            return $location['palatium_care_id'];
//        }, $this->dbConnection
//            ->query("SELECT palatium_care_id FROM locations")
//            ->fetchAll(PDO::FETCH_ASSOC)
//        );

        $zoneId = $this->dbConnection
            ->query(sprintf("SELECT id FROM zones WHERE name = '%s'", $this->config['sync']['zone']))
            ->fetchColumn();

        $stmt = $this->dbConnection->prepare(
            'INSERT INTO locations (palatium_care_id, zone_id, name, enabled) VALUES (?, ?, ?, 1)
                ON DUPLICATE KEY UPDATE name = VALUES(name)'
        );

        foreach ($locations as $location) {

//            if (in_array($location['LocationId'], $serverLocations)) {
//                $key = array_search($location['LocationId'], $serverLocations);
//                unset($serverLocations[$key]);
//            }
            $stmt->execute(array($location['LocationId'], $zoneId, $location['LocationName']));
        }

//        $stmt = $this->dbConnection->prepare(
//            'DELETE FROM locations WHERE palatium_care_id = ?'
//        );
//
//        foreach ($serverLocations as $location) {
//            $stmt->execute(array($location));
//        }
    }

    private function upsetDevices(array $devices, array $residents)
    {
        if (empty($devices)) {
            return;
        }

//        $serverButtons = array_map(function($button) {
//                return $button['palatium_care_id'];
//            }, $this->dbConnection
//                ->query("SELECT palatium_care_id FROM buttons")
//                ->fetchAll(PDO::FETCH_ASSOC)
//        );


        $stmt = $this->dbConnection->prepare(
            'INSERT INTO buttons (palatium_care_id, location_id, public_id) VALUES (?, ?, ?)
                ON DUPLICATE KEY UPDATE palatium_care_id = VALUES(palatium_care_id), location_id = VALUES(location_id), public_id = VALUES(public_id)'
        );

        $exec = $this->dbConnection->prepare(
            'INSERT INTO devices (type, workflow) VALUES(?, ?)
                ON DUPLICATE KEY UPDATE type = VALUES(type)'
        );

        foreach ($devices as $device) {
            /*if (!in_array(trim($device['DeviceUseShortName']), $this->config['sync']['devices']['allowedTypes'])) {
                continue;
            }*/


//            if (in_array($device['DeviceId'], $serverButtons)) {
//                $key = array_search($device['DeviceId'], $serverButtons);
//                unset($serverButtons[$key]);
//            }

            if (!empty($device['DeviceUseShortName'])) {
                $exec->execute(array($device['DeviceUseShortName'], 'no action'));
            }


            $pcLocationId = null;

            if ($device['LocationId']) {
                $pcLocationId = $device['LocationId'];
            } elseif (isset($residents[$device['ResidentId']])) {
                $pcLocationId = $residents[$device['ResidentId']]['LocationId'];
            }

            $locationId = $pcLocationId ?
                $this->dbConnection
                    ->query(sprintf('SELECT id FROM locations WHERE palatium_care_id = %s', $pcLocationId))
                    ->fetchColumn()
                : null;

            try {
                $stmt->execute(array($device['DeviceId'], $locationId, $device['DeviceKey']));
            } catch (PDOException $e) {
                /* if bad location constraint */
                continue;
            }
        }


//        $stmt = $this->dbConnection->prepare(
//            'DELETE FROM buttons WHERE palatium_care_id = ?'
//        );
//
//        foreach ($serverButtons as $button) {
//            $stmt->execute(array($button));
//        }

    }

    private function getDataToSync($type)
    {
        $csvData = $this->sendHttpRequest(
            $this->config['palatiumCareHost'] . $this->config['sync'][$type]['action'],
            $this->config['sync']['responseTimeout']

        );

        $columnNames = $this->config['sync'][$type]['columns'];
        $result = array();

        foreach (explode(PHP_EOL, $csvData) as $key => $row) {
            $data = str_getcsv($row);

            /* check column names match and continue if ok */
            if ($key == 0) {
                if (!$this->checkColumnNames($data, $columnNames)) {
                    return $result;
                }
                continue;
            }

            if (empty(trim($row))) {
                continue;
            }

            if (sizeof($data) == sizeof($columnNames)) {
                $result[$data[0]] = array_combine($columnNames, $data);
            } else {
                $this->log('Incorrect sync data format');
            }
        }

        return $result;
    }

    private function checkColumnNames(array $data, array $columnNames)
    {
        foreach ($columnNames as $column) {
            if (!in_array($column, $data)) {
                $this->log(sprintf('There is no %s column', $column));
                return false;
            }
        }

        return true;
    }

    public function run()
    {
        $this->init();

        /* sync script*/
        if (isset($_SERVER['argv'][1]) && $_SERVER['argv'][1] === $this->config['sync']['argument']) {
            $this->type = 'data-sync';
            file_put_contents($this->config['lockFilePath'], 1);

            try {
                $this->processSync();
            } finally {
                unlink($this->config['lockFilePath']);
            }

            /* exit from sync script */
            return;
        }

        /* alerts import script */
//        if (!is_file($this->config['lockFilePath'])) {
//            $this->processAlerts();
//        }
    }
}

PalatiumCareInteraction::getInstance()->run();
