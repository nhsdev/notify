<?php

function parser ($path) {
  $fh = fopen($path, 'r');
  try {while (!feof($fh)) {
    $line = fgets($fh);
      if (0 === strpos($line, 'P')) {
        yield explode('|', str_replace(['{', '}'], '', trim($line)));
      }
  }}
  catch (Exception $e) {}
  finally {is_resource($fh) && fclose($fh);}
}

function fetcher ($path, $find) {
  $fh = fopen($path, 'r');
  try {foreach ($find as $dt) {
    fseek($fh, 0);
      while (!feof($fh)) {
        $line = fgets($fh);
          if (false !== strpos($line, $dt)) {
            $block = $line;
              while ("}\n" !== $line) {
                $line = fgets($fh);
                $block .= $line;
              }
            yield $block;
            continue;
          }
      }
  }}
  catch (Exception $e) {}
  finally {is_resource($fh) && fclose($fh);}
}

  $requests = ['POST' => [], 'PATCH' => []];

    foreach (parser($argv[1]) as list($method, $dt)) {
      $requests[$method][] = $dt;
    }
  $count = function ($a) {return count($a);};
  echo "POST: {$count($requests['POST'])}; PATCH: {$count($requests['PATCH'])}", PHP_EOL;

  $f = fetcher($argv[1], array_filter($requests['POST'], function ($dt) use ($requests) {
    return !in_array($dt, $requests['PATCH']);
  }));

    foreach ($f as $block) {
      echo $block;
    }