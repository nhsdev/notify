<?php
return array(
    'doctrine' => array(
        'driver' => array(
            // overriding zfc-user-doctrine-orm's config
            'zfcuser_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => __DIR__ . '/../src/WebApplication/Entity',
            ),

            'orm_default' => array(
                'drivers' => array(
                    'WebApplication\Entity' => 'zfcuser_entity',
                ),
            ),
        ),
    ),

    'zfcuser' => array(
        /* tell ZfcUser to use our own class */
        'user_entity_class' => 'WebApplication\Entity\User',
        /* tell ZfcUserDoctrineORM to skip the entities it defines */
        'enable_default_entities' => false,
    ),

    'view_manager' => array(
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/webapplication.phtml',
        ),
        'template_path_stack' => array(
            'web-application-views' => __DIR__ . '/../view',
        ),
//        'template_map' => array(
//            'web-application/report/login-history'=> __DIR__ . '/../view/web-application/report/index.phtml'
//        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'webapplication' => 'WebApplication\Controller\UsersController',
            'handsets' => 'WebApplication\Controller\HandsetController',
            'WebApplication\Controller\Index' => 'WebApplication\Controller\IndexController',
            'WebApplication\Controller\System' => 'WebApplication\Controller\SystemController',
            'zones' => 'WebApplication\Controller\ZoneController',
            'locations' => 'WebApplication\Controller\LocationController',
            'hops' => 'WebApplication\Controller\HopController',
            'reports' => 'WebApplication\Controller\ReportController',
            'WebApplication\Controller\Admin' => 'WebApplication\Controller\AdminController',
            'employees' => 'WebApplication\Controller\EmployeeController',
            'buttons' => 'WebApplication\Controller\ButtonController',
            'devices' => 'WebApplication\Controller\DeviceController'
        ),
    ),

    'controller_plugins' => array(
        'invokables' => array()
    ),

    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'WebApplication\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),

            'zfcuser' => array(
                'options' => array(
                    'route' => '/account',
                    'defaults' => array(
                        'controller' => 'webapplication',
                        'action'     => 'index',
                    ),
                ),
                'child_routes' => array(
                    'login' => array(
                        'options' => array(
                            'defaults' => array(
                                'controller' => 'webapplication',
                            ),
                        ),
                    ),
                    'logout' => array(
                        'options' => array(
                            'defaults' => array(
                                'controller' => 'webapplication',
                            ),
                        ),
                    ),
                    'authenticate' => array(
                        'options' => array(
                            'defaults' => array(
                                'controller' => 'webapplication',
                            ),
                        ),
                    ),
                ),
            ),

            'index' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/index[/:action][/:id[/]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*'
                    ),
                    'defaults' => array(
                        'controller' => 'WebApplication\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),

            'system' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/system[/:action][/:id[/]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*'
                    ),
                    'defaults' => array(
                        'controller' => 'WebApplication\Controller\System',
                        'action'     => 'index',
                    ),
                ),
            ),

            'admin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin[/:controller][/:action][/:id[/]]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*'
                    ),
                    'defaults' => array(
                        'controller' => 'WebApplication\Controller\Admin',
                        'action'     => 'index',
                    ),
                ),
            ),

            'report' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/reports[/:action][/:type[/]][/:page[/]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'type' => '(pdf|excel)',
                        'page' => '[0-9]*'
                    ),
                    'defaults' => array(
                        'controller' => 'reports',
                        'action'     => 'index',
                        'page' => 1
                    ),
                ),
            ),
        ),
    ),
);