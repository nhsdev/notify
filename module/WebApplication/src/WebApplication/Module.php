<?php
namespace WebApplication;

use Zend\Mvc\MvcEvent;
use WebApplication\Model\UserModel;
use WebApplication\Model\AlertModel;
use WebApplication\Model\HandsetModel;
use WebApplication\Model\ZoneModel;
use WebApplication\Model\LocationModel;
use WebApplication\Model\ButtonModel;
use WebApplication\Model\LoginHistoryModel;
use WebApplication\Model\HopModel;
use WebApplication\Model\AsteriskModel;
use WebApplication\Model\DeviceModel;
use WebApplication\Model\ResidentModel;
use WebApplication\Controller\Validation\UserValidator;
use WebApplication\Controller\Validation\ZoneValidator;
use WebApplication\Controller\Validation\HandsetValidator;
use WebApplication\Controller\Validation\LocationValidator;
use WebApplication\Controller\Validation\HopValidator;
use WebApplication\Controller\Validation\ButtonValidator;
use WebApplication\Controller\Validation\DeviceValidator;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/../../src/' . __NAMESPACE__,
                ),
            ),
        );
    }

//    public function onBootstrap( MVCEvent $e )
//    {
//        $eventManager = $e->getApplication()->getEventManager();
//        $em           = $eventManager->getSharedManager();
//        $em->attach(
//            'ZfcUser\Form\RegisterFilter',
//            'init',
//            function( $e )
//            {
//                $filter = $e->getTarget();
//                // do your form filtering here
//            }
//        );
//
//        // custom form fields
//
//        $em->attach(
//            'ZfcUser\Form\Register',
//            'init',
//            function($e)
//            {
//                /* @var $form \ZfcUser\Form\Register */
//                $form = $e->getTarget();
//                $form->add(
//                    array(
//                        'name' => 'username',
//                        'options' => array(
//                            'label' => 'Name',
//                        ),
//                        'attributes' => array(
//                            'type'  => 'text',
//                        ),
//                    )
//                );
//
//                $form->add(
//                    array(
//                        'name' => 'voipName',
//                        'options' => array(
//                            'label' => 'VoIP Name',
//                        ),
//                        'attributes' => array(
//                            'type'  => 'text',
//                        ),
//                    )
//                );
//                $form->add(
//                    array(
//                        'name' => 'voipPassword',
//                        'options' => array(
//                            'label' => 'VoIP Password',
//                        ),
//                        'attributes' => array(
//                            'type'  => 'password',
//                        ),
//                    )
//                );
//                $form->add(
//                    array(
//                        'name' => 'pin',
//                        'options' => array(
//                            'label' => 'PIN',
//                        ),
//                        'attributes' => array(
//                            'type'  => 'integer',
//                        ),
//                    )
//                );
////                $form->add(array(
////                    'type' => 'Zend\Form\Element\Checkbox',
////                    'name' => 'enabled',
////                    'options' => array(
////                        'label' => 'Enabled',
////                        'use_hidden_element' => true,
////                        'checked_value' => 1,
////                        'unchecked_value' => 0
////                    ),
////                ));
//            }
//        );
//
//        // here's the storage bit
//
//        $zfcServiceEvents = $e->getApplication()->getServiceManager()->get('zfcuser_user_service')->getEventManager();
//
//        $zfcServiceEvents->attach('register', function($e) {
//            $form = $e->getParam('form');
//            $user = $e->getParam('user');
//            /* @var $user \WebApplication\Entity\User */
//            $user->setUsername(  $form->get('username')->getValue() );
//            $user->setPin( $form->get('pin')->getValue() );
//            $user->setVoipName( $form->get('voipName')->getValue() );
//            $user->setVoipPassword( $form->get('voipPassword')->getValue() );
////            $user->setEnabled( $form->get('enabled')->getValue() );
//        });
//
//        // you can even do stuff after it stores
//        $zfcServiceEvents->attach('register.post', function($e) {
//            /*$user = $e->getParam('user');*/
//        });
//    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                /* models */
                'Model\User' => function($sm) {
                    return new UserModel($sm);
                },
                'Model\Alert' => function($sm) {
                    return new AlertModel($sm);
                },
                'Model\Handset' => function($sm) {
                    return new HandsetModel($sm);
                },
                'Model\Zone' => function($sm) {
                    return new ZoneModel($sm);
                },
                'Model\Location' => function($sm) {
                    return new LocationModel($sm);
                },
                'Model\Button' => function($sm) {
                    return new ButtonModel($sm);
                },
                'Model\LoginHistory' => function($sm) {
                    return new LoginHistoryModel($sm);
                },
                'Model\Hop' => function($sm) {
                    return new HopModel($sm);
                },
                'Model\Asterisk' => function($sm) {
                    return new AsteriskModel($sm);
                },
                'Model\Device' => function($sm) {
                    return new DeviceModel($sm);
                },
                'Model\Resident' => function($sm) {
                    return new ResidentModel($sm);
                },
                /* validations */
                'Validation\User' => function($sm) {
                    return new UserValidator($sm);
                },
                'Validation\Zone' => function($sm) {
                    return new ZoneValidator($sm);
                },
                'Validation\Handset' => function($sm) {
                    return new HandsetValidator($sm);
                },
                'Validation\Location' => function($sm) {
                    return new LocationValidator($sm);
                },
                'Validation\Hop' => function($sm) {
                    return new HopValidator($sm);
                },
                'Validation\Button' => function($sm) {
                    return new ButtonValidator($sm);
                },
                'Validation\Device' => function($sm) {
                    return new DeviceValidator($sm);
                }
            ),
        );
    }

}