<?php

namespace WebApplication\Controller;

use WebApplication\Model\UserModel;
use WebApplication\Model\AlertModel;
use WebApplication\Model\HandsetModel;
use WebApplication\Model\ZoneModel;
use WebApplication\Model\LocationModel;
use WebApplication\Model\ButtonModel;
use WebApplication\Model\LoginHistoryModel;
use WebApplication\Model\HopModel;
use WebApplication\Model\AsteriskModel;
use WebApplication\Model\DeviceModel;
use WebApplication\Model\ResidentModel;

trait ModelShortcut
{
    /** @return UserModel */
    protected function getUserModel()
    {
        return $this->getServiceLocator()->get('Model\User');
    }

    /** @return AlertModel */
    protected function getAlertModel()
    {
        return $this->getServiceLocator()->get('Model\Alert');
    }

    /** @return HandsetModel */
    protected function getHandsetModel()
    {
        return $this->getServiceLocator()->get('Model\Handset');
    }

    /** @return ZoneModel */
    protected function getZoneModel()
    {
        return $this->getServiceLocator()->get('Model\Zone');
    }

    /** @return LocationModel */
    protected function getLocationModel()
    {
        return $this->getServiceLocator()->get('Model\Location');
    }

    /** @return ButtonModel */
    protected function getButtonModel()
    {
        return $this->getServiceLocator()->get('Model\Button');
    }

    /** @return LoginHistoryModel */
    protected function getLoginHistoryModel()
    {
        return $this->getServiceLocator()->get('Model\LoginHistory');
    }

    /** @return HopModel */
    protected function getHopModel()
    {
        return $this->getServiceLocator()->get('Model\Hop');
    }

    /** @return AsteriskModel */
    protected function getAsteriskModel()
    {
        return $this->getServiceLocator()->get('Model\Asterisk');
    }

    /** @return DeviceModel */
    protected function getDeviceModel()
    {
        return $this->getServiceLocator()->get('Model\Device');
    }

    /** @return ResidentModel */
    protected function getResidentModel()
    {
        return $this->getServiceLocator()->get('Model\Resident');
    }
}