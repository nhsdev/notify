<?php

namespace WebApplication\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use WebApplication\Entity\User;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;
use Zend\Mvc\MvcEvent;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
abstract class AbstractAdminController extends AbstractActionController
{
    use ModelShortcut;
    use ValidatorShortcut;
    use UtilShortcut;

    const EMPLOYEE_URL  = '/admin/employees';
    const HANDSET_URL   = '/admin/handsets';
    const HOP_URL       = '/admin/hops';
    const LOCATION_URL  = '/admin/locations';
    const BUTTON_URL    = '/admin/buttons';
    const ZONE_URL      = '/admin/zones';
    const DEVICE_URL     = '/admin/devices';

    /** @Override */
    public function onDispatch(MvcEvent $e)
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute('zfcuser/login');
        }

        if ($this->zfcUserAuthentication()->getIdentity()->getRole() != User::ROLE_ADMIN) {
            return $this->redirect()->toRoute('home');
        }

        return parent::onDispatch($e);
    }
}