<?php

namespace WebApplication\Controller;

use WebApplication\Entity\Location;
use Zend\Http\Request;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;
use WebApplication\Entity\Button;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
class LocationController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'locations' => $this->getLocationModel()->findAll(),
        );
    }

    public function addAction()
    {
        $locations = $this->getLocationModel()->findAll();
        $zones = $this->getZoneModel()->findAll();

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'zoneId', 'behaviour', 'enabled'));

            $validationResult = $this->getLocationValidator()->validateCreate($params);
            if (!$validationResult->isValid()) {
                return array(
                    'locations' => $locations,
                    'behaviours' => Location::getBehaviours(),
                    'zones' => $zones,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getLocationModel()->create($params);
            $this->flashMessenger()->addMessage(sprintf('Location "%s" was created', $params['name']));

            return $this->redirect()->toUrl(self::LOCATION_URL);
        }

        return array(
            'locations' => $locations,
            'behaviours' => Location::getBehaviours(),
            'zones' => $zones
        );
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Location $location */
        if (!$location = $this->getLocation($id)) {
            return $this->redirect()->toUrl(self::LOCATION_URL);
        }

        $fromPalatiumCare = (bool) $location->getPalatiumCareId();

        $locations = $this->getLocationModel()->findAll();
        $zones = $this->getZoneModel()->findAll();

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'zoneId', 'behaviour', 'enabled'));
            $params['fromPalatiumCare'] = $fromPalatiumCare;

            $validationResult = $this->getLocationValidator()->validateUpdate($params, $location, $fromPalatiumCare);
            if (!$validationResult->isValid()) {
                return array(
                    'locations' => $locations,
                    'behaviours' => Location::getBehaviours(),
                    'zones' => $zones,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getLocationModel()->update($location, $params, $fromPalatiumCare);
            $this->flashMessenger()->addMessage(sprintf('Location "%s" was updated', $params['name']));

            return $this->redirect()->toUrl(self::LOCATION_URL);
        }

        return array(
            'locations' => $locations,
            'behaviours' => Location::getBehaviours(),
            'zones' => $zones,
            'params' => array(
                'name' => $location->getName(),
                'zoneId' => $location->getZone() ? $location->getZone()->getId() : null,
                'behaviour' => $location->getBehavior(),
                'enabled' => $location->getEnabled(),
                'fromPalatiumCare' => $fromPalatiumCare
            )
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');

        /** @var Location $location */
        if (!$location = $this->getLocation($id)) {
            return $this->redirect()->toUrl(self::LOCATION_URL);
        }

        if ($location->getPalatiumCareId()) {
            $this->flashMessenger()->addErrorMessage('Sorry, you can\'t delete location from Palatium Care');
            return false;
        }

        $this->getLocationModel()->delete($location);
        $this->flashMessenger()->addMessage(sprintf('Location "%s" was deleted', $location->getName()));

        return $this->redirect()->toUrl(self::LOCATION_URL);
    }

    private function getLocation($id)
    {
        /** @var Location $location */
        if (!$location = $this->getLocationModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Location with id: %s not found', $id));
            return false;
        }

        return $location;
    }
}