<?php

namespace WebApplication\Controller;

use WebApplication\Controller\Utils\ReportUtility;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
class ReportController extends AbstractAdminController
{
    public function employeeAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo', 'userId')
        );

        $page = $this->params()->fromRoute('page');
        $limit = 100;
        $offset = ($page - 1) * $limit;
        $pages = ceil($this->getAlertModel()->countAlertsStats($params) / $limit);
        $query = $this->getRequest()->getQuery()->toString();

        $params['offset'] = $offset;
        $params['limit'] = $limit;

        $alerts = $this->getAlertModel()->getAlertsStats($params);
        list($alertsIds, $avg) = (new ReportUtility('alerts'))->getAggregatedData($alerts);

        $paginator = new Paginator(new ArrayAdapter($alerts));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage($limit);

        return array(
            'users' => $this->getUserModel()->findAll(),
            'currentUser' => !empty($params['userId'])
                    ? $this->getUserModel()->findById($params['userId'])
                    : null,
            'alerts' => $alerts,
            'alertsIds' => $alertsIds,
            'dateFrom' => $params['dateFrom'],
            'hoursFrom' => $params['hoursFrom'],
            'minutesFrom' => $params['minutesFrom'],
            'dateTo' => $params['dateTo'],
            'hoursTo' => $params['hoursTo'],
            'minutesTo' => $params['minutesTo'],
            'avg' => $avg,
            'paginator' => $paginator,
            'pages' => $pages,
            'page' => $page,
            'query' => $query
        );
    }

    public function locationAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo', 'locationId')
        );

        $page = $this->params()->fromRoute('page');
        $limit = 100;
        $offset = ($page - 1) * $limit;
        $pages = ceil($this->getAlertModel()->countAlertsStats($params) / $limit);
        $query = $this->getRequest()->getQuery()->toString();

        $params['offset'] = $offset;
        $params['limit'] = $limit;

        $alerts = $this->getAlertModel()->getAlertsStats($params);
        list($alertsIds, $avg) = (new ReportUtility('alerts'))->getAggregatedData($alerts);

        $paginator = new Paginator(new ArrayAdapter($alerts));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage($limit);

        return array(
            'locations' => $this->getLocationModel()->findAll(),
            'currentLocation' => !empty($params['locationId'])
                    ? $this->getLocationModel()->findById($params['locationId'])
                    : null,
            'alerts' => $alerts,
            'alertsIds' => $alertsIds,
            'dateFrom' => $params['dateFrom'],
            'hoursFrom' => $params['hoursFrom'],
            'minutesFrom' => $params['minutesFrom'],
            'dateTo' => $params['dateTo'],
            'hoursTo' => $params['hoursTo'],
            'minutesTo' => $params['minutesTo'],
            'avg' => $avg,
            'paginator' => $paginator,
            'pages' => $pages,
            'page' => $page,
            'query' => $query
        );
    }

    public function summaryAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo')
        );

        $summary = $this->getAlertModel()->getSummaryStats($params);
        list($summary, $avg) = (new ReportUtility('summary'))->getAggregatedData($summary);

        return array(
            'summary' => $summary,
            'dateFrom' => $params['dateFrom'],
            'hoursFrom' => $params['hoursFrom'],
            'minutesFrom' => $params['minutesFrom'],
            'dateTo' => $params['dateTo'],
            'hoursTo' => $params['hoursTo'],
            'minutesTo' => $params['minutesTo'],
            'avg' => $avg
        );
    }

    public function loginAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo', 'userId', 'handsetId')
        );

        $params['user'] = !empty($params['userId'])
            ? $this->getUserModel()->findById($params['userId'])
            : null;

        $params['handset'] = !empty($params['handsetId'])
            ? $this->getHandsetModel()->findById($params['handsetId'])
            : null;

        $page = $this->params()->fromRoute('page');
        $limit = 100;
        $offset = ($page - 1) * $limit;
        $pages = ceil($this->getLoginHistoryModel()->countStatsData($params) / $limit);
        $query = $this->getRequest()->getQuery()->toString();

        $params['offset'] = $offset;
        $params['limit'] = $limit;

        $loginHistory = $this->getLoginHistoryModel()->getStatsData($params);
        $ids = (new ReportUtility('login'))->getAggregatedData($loginHistory);

        $paginator = new Paginator(new ArrayAdapter($loginHistory));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage($limit);

        return array(
            'users' => $this->getUserModel()->findAll(),
            'handsets' => $this->getHandsetModel()->findAll(),
            'currentUser' => $params['user'],
            'currentHandset' => $params['handset'],
            'loginHistory' => $loginHistory,
            'ids' => $ids,
            'dateFrom' => $params['dateFrom'],
            'hoursFrom' => $params['hoursFrom'],
            'minutesFrom' => $params['minutesFrom'],
            'dateTo' => $params['dateTo'],
            'hoursTo' => $params['hoursTo'],
            'minutesTo' => $params['minutesTo'],
            'paginator' => $paginator,
            'pages' => $pages,
            'page' => $page,
            'query' => $query
        );
    }


    public function generateSummaryReportAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo')
        );

        $type = $this->params('type');

        (new ReportUtility('summary'))->generateReport(
            $this->getAlertModel()->getSummaryStats($params),
            $type == 'excel' ? 'Excel2007' : 'PDF',
            $type == 'excel' ? 'xlsx' : 'pdf'
        );
    }

    public function generateEmployeeReportAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo', 'userId')
        );

        $type = $this->params('type');

        (new ReportUtility('alerts'))->generateReport(
            $this->getAlertModel()->getAlertsStats($params),
            $type == 'excel' ? 'Excel2007' : 'PDF',
            $type == 'excel' ? 'xlsx' : 'pdf',
            'employee'
        );
    }

    public function generateLocationReportAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo', 'locationId')
        );

        $type = $this->params('type');

        (new ReportUtility('alerts'))->generateReport(
            $this->getAlertModel()->getAlertsStats($params),
            $type == 'excel' ? 'Excel2007' : 'PDF',
            $type == 'excel' ? 'xlsx' : 'pdf',
            'location'
        );
    }

    public function generateLoginReportAction()
    {
        $params = $this->getParamsFromRequest(
            array('dateFrom', 'hoursFrom', 'minutesFrom', 'dateTo', 'hoursTo', 'minutesTo', 'userId', 'handsetId')
        );

        $params['user'] = !empty($params['userId'])
            ? $this->getUserModel()->findById($params['userId'])
            : null;

        $params['handset'] = !empty($params['handsetId'])
            ? $this->getHandsetModel()->findById($params['handsetId'])
            : null;

        $type = $this->params('type');

        (new ReportUtility('login'))->generateReport(
            $loginHistory = $this->getLoginHistoryModel()->getStatsData($params),
            $type == 'excel' ? 'Excel2007' : 'PDF',
            $type == 'excel' ? 'xlsx' : 'pdf'
        );
    }
}