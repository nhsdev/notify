<?php

namespace WebApplication\Controller;

use Zend\Http\Request;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;
use WebApplication\Entity\Handset;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
class HandsetController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'handsets' => $this->getHandsetModel()->findAll(),
            'users' => $this->getUserModel()->findBy(array('handset'=>null))
        );
    }

    public function addAction()
    {
        $handsets = $this->getHandsetModel()->findAll();
        $users = $this->getUserModel()->findBy(array('handset' => null));
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'displayName', 'publicId'));

            $validationResult = $this->getHandsetValidator()->validateCreate($params);
            if (!$validationResult->isValid()) {
                return array(
                    'handsets' => $handsets,
                    'users' => $users,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getHandsetModel()->create($params);
            $this->flashMessenger()->addMessage(sprintf('Handset "%s" was created', $params['name']));

            return $this->redirect()->toUrl(self::HANDSET_URL);
        }

        return array('handsets' => $handsets, 'users' => $users);
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Handset $handset */
        if (!$handset = $this->getHandsetModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Handset with id: %s not found', $id));
            return $this->redirect()->toUrl(self::HANDSET_URL);
        }

        $handsets = $this->getHandsetModel()->findAll();
        $users = $this->getUserModel()->findBy(array('handset' => null));
        if ($handset->getUser()) {
            array_unshift($users, $handset->getUser());
        }

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'displayName', 'publicId'));

            $validationResult = $this->getHandsetValidator()->validateUpdate($params, $handset);
            if (!$validationResult->isValid()) {
                return array(
                    'handsets' => $handsets,
                    'users' => $users,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getHandsetModel()->update($handset, $params);
            $this->flashMessenger()->addMessage(sprintf('Handset "%s" was updated', $params['name']));

            return $this->redirect()->toUrl(self::HANDSET_URL);
        }

        return array(
            'handsets' => $handsets,
            'users' => $users,
            'params' => array(
                'name' => $handset->getName(),
                'displayName' => $handset->getDisplayName(),
                'publicId' => $handset->getPublicId()
            )
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');
        /** @var Handset $handset */
        if (!$handset = $this->getHandsetModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Handset with id: %s not found', $id));
            return $this->redirect()->toUrl(self::HANDSET_URL);
        }

        $this->getHandsetModel()->delete($handset);
        $this->flashMessenger()->addMessage(sprintf('Handset "%s" was deleted', $handset->getName()));

        return $this->redirect()->toUrl(self::HANDSET_URL);
    }
}