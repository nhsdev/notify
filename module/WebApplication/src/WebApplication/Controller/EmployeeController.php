<?php

namespace WebApplication\Controller;

use Doctrine\Common\Collections\Criteria;
use Zend\Http\Request;
use WebApplication\Entity\User;
use ZfcUser\Options\ModuleOptions as ZfcUserModuleOptions;
use ServerApi\Controller\Plugin\PushNotificationPlugin;

/**
 * @method PushNotificationPlugin PushNotificationPlugin()
 */
class EmployeeController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'users' => $this->getUserModel()->findBy(array(), array('username' => Criteria::ASC)),
            'roles' => User::getRoles()
        );
    }

    public function addAction()
    {
        $users = $this->getUserModel()->findBy(array(), array('username' => Criteria::ASC));
        $zones = $this->getZoneModel()->findAll();
        $roles = User::getRoles();
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(
                array(
                    'email',
                    'username',
                    'voipName',
                    'voipPassword',
                    'voipCallerId',
                    'pin',
                    'enabled',
                    'role',
                    'password',
                    'passwordVerify',
                    'zonesIds'
                )
            );

            $validationResult = $this->getUserValidator()->validateCreate($params);
            if (!$validationResult->isValid()) {
                return array(
                    'users' => $users,
                    'zones' => $zones,
                    'roles' => $roles,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getUserModel()->create($params, $this->getOptions()->getPasswordCost());
            if ($params['role'] == User::ROLE_MOBILE_USER) {
                $this->getAsteriskModel()->insert($params);
            }

            $this->flashMessenger()->addMessage(sprintf('User "%s" was created', $params['username']));

            return $this->redirect()->toUrl(self::EMPLOYEE_URL);
        }

        return array('users' => $users, 'zones' => $zones, 'roles' => $roles);
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var User $user */
        if (!$user = $this->getUserModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('User with id: %s not found', $id));
            return $this->redirect()->toUrl(self::EMPLOYEE_URL);
        }

        $asteriskUser = $this->getAsteriskModel()->findByName($user->getVoipName());

        $users = $this->getUserModel()->findBy(array(), array('username' => Criteria::ASC));
        $zones = $this->getZoneModel()->findAll();
        $roles = User::getRoles();
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(
                array(
                    'email',
                    'username',
                    'voipName',
                    'voipPassword',
                    'voipCallerId',
                    'pin',
                    'enabled',
                    'role',
                    'password',
                    'passwordVerify',
                    'zonesIds'
                )
            );

            $validationResult = $this->getUserValidator()->validateUpdate($params, $user);
            if (!$validationResult->isValid()) {
                return array(
                    'users' => $users,
                    'zones' => $zones,
                    'roles' => $roles,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            if ($params['role'] == User::ROLE_MOBILE_USER && (!empty($params['voipPassword']) && $params['voipPassword'] != $user->getVoipPassword())) {
                if ($handset = $this->getHandsetModel()->findOneBy(array('user' => $user))) {
                    $pushToken = $handset->getPushToken();
                    $this->PushNotificationPlugin()->pushNotification(
                        array('action' => 'updateEmployee', 'id' => $user->getId()),
                        array($pushToken),
                        'Settings Updated',
                        0
                    );
                }
            }

            $this->getUserModel()->update($user, $params, $this->getOptions()->getPasswordCost());
            /** asterisk user update */
            if ($asteriskUser && $params['role'] != User::ROLE_MOBILE_USER) {
                $this->getAsteriskModel()->deleteById($asteriskUser['id']);
            } elseif ($asteriskUser && $params['role'] == User::ROLE_MOBILE_USER) {
                $this->getAsteriskModel()->updateById($asteriskUser['id'], $params);
            } elseif ($params['role'] == User::ROLE_MOBILE_USER) {
                $this->getAsteriskModel()->insert($params);
            }

            $this->flashMessenger()->addMessage(sprintf('User "%s" was updated', $params['username']));

            return $this->redirect()->toUrl(self::EMPLOYEE_URL);
        }

        return array(
            'users' => $users,
            'zones' => $zones,
            'roles' => $roles,
            'params' => array(
                'email' => $user->getemail(),
                'username' => $user->getUsername(),
                'voipName' => $user->getVoipName(),
                'voipCallerId' => $user->getVoipCallerId(),
                'pin' => $user->getPin(),
                'enabled' => $user->getEnabled(),
                'role' => $user->getRole(),
                'zonesIds' => $user->getZonesIds()
            )
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');

        /** @var User $user */
        if (!$user = $this->getUserModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('User with id: %s not found', $id));
            return $this->redirect()->toUrl(self::EMPLOYEE_URL);
        }

        if ($this->zfcUserAuthentication()->getIdentity()->getId() == $id) {
            $this->flashMessenger()->addErrorMessage('You can\'t delete yourself');
            return $this->redirect()->toUrl(self::EMPLOYEE_URL);
        }

        $asteriskUser = $this->getAsteriskModel()->findByName($user->getVoipName());

        $this->getUserModel()->delete($user);
        if ($asteriskUser) {
            $this->getAsteriskModel()->deleteById($asteriskUser['id']);
        }

        $this->flashMessenger()->addMessage(sprintf('User "%s" was deleted', $user->getUsername()));

        return $this->redirect()->toUrl(self::EMPLOYEE_URL);
    }

    /**
     * @return ZfcUserModuleOptions
     */
    public function getOptions()
    {
        return $this->getServiceLocator()->get('zfcuser_module_options');
    }
}