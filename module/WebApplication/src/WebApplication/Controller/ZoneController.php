<?php

namespace WebApplication\Controller;

use Zend\Http\Request;
use WebApplication\Entity\Zone;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
class ZoneController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'zones' => $this->getZoneModel()->findAll(),
            'sounds' => Zone::getSounds(),
        );
    }

    public function addAction()
    {
        $zones = $this->getZoneModel()->findAll();
        $sounds = Zone::getSounds();
        $soundIntervals = Zone::getSoundIntervals();
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'sound', 'soundCount', 'soundInterval'));

            $validationResult = $this->getZoneValidator()->validateCreate($params);
            if (!$validationResult->isValid()) {
                return array(
                    'zones' => $zones,
                    'sounds' => $sounds,
                    'soundIntervals' => $soundIntervals,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getZoneModel()->create(array('name' => $params['name'], 'sound' => $params['sound'], 'soundCount' => $params['soundCount'], 'soundInterval' => $params['soundInterval']));
            $this->flashMessenger()->addMessage(sprintf('Zone "%s" was created', $params['name']));

            return $this->redirect()->toUrl(self::ZONE_URL);
        }

        return array('zones' => $zones, 'sounds' => $sounds, 'soundIntervals' => $soundIntervals,);
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Zone $zone */
        if (!$zone = $this->getZoneModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Zone with id: %s not found', $id));
            return $this->redirect()->toUrl(self::ZONE_URL);
        }

        /*if ($zone->getName() == Zone::PALATIUM_CARE_ZONE) {
            $this->flashMessenger()->addErrorMessage(
                sprintf('You can\'t edit zone "%s" because it is used by Palatium Care', $zone->getName())
            );
            return $this->redirect()->toUrl(self::ZONE_URL);
        }*/

        $zones = $this->getZoneModel()->findAll();
        $sounds = Zone::getSounds();
        $soundIntervals = Zone::getSoundIntervals();
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'sound', 'soundCount', 'soundInterval'));

            $validationResult = $this->getZoneValidator()->validateUpdate($params, $zone);
            if (!$validationResult->isValid()) {
                return array(
                    'zones' => $zones,
                    'sounds' => $sounds,
                    'soundIntervals' => $soundIntervals,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getZoneModel()->update($zone, array('name' => $params['name'], 'sound' => $params['sound'], 'soundCount' => $params['soundCount'], 'soundInterval' => $params['soundInterval']));
            $this->flashMessenger()->addMessage(sprintf('Zone "%s" was updated', $params['name']));

            return $this->redirect()->toUrl(self::ZONE_URL);
        }

        return array(
            'zones' => $zones,
            'sounds' => $sounds,
            'soundIntervals' => $soundIntervals,
            'params' => array('name' => $zone->getName(), 'sound' => $zone->getSound(), 'soundCount' => $zone->getSoundCount(), 'soundInterval' => $zone->getSoundInterval())
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');
        /** @var Zone $zone */
        if (!$zone = $this->getZoneModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Zone with id: %s not found', $id));
            return $this->redirect()->toUrl(self::ZONE_URL);
        }

        if ($zone->getName() == Zone::PALATIUM_CARE_ZONE) {
            $this->flashMessenger()->addErrorMessage(
                sprintf('You can\'t delete zone "%s" because it is used by Palatium Care', $zone->getName())
            );
            return $this->redirect()->toUrl(self::ZONE_URL);
        }

        if ($this->getLocationModel()->findOneBy(array('zone' => $zone))) {
            $this->flashMessenger()->addErrorMessage(
                sprintf('You can\'t delete zone "%s" because it is used by locations', $zone->getName())
            );
            return $this->redirect()->toUrl(self::ZONE_URL);
        }

        $this->getZoneModel()->delete($zone);
        $this->flashMessenger()->addMessage(sprintf('Zone "%s" was deleted', $zone->getName()));

        return $this->redirect()->toUrl(self::ZONE_URL);
    }
}