<?php

namespace WebApplication\Controller\Utils;

use WebApplication\Controller\Utils\Report\AbstractReport;
use WebApplication\Controller\Utils\Report\AlertsReport;
use WebApplication\Controller\Utils\Report\LoginReport;
use WebApplication\Controller\Utils\Report\SummaryReport;

class ReportUtility
{
    /** @var AbstractReport */
    private $reportStrategy = null;

    public function __construct($report)
    {
        switch($report) {
            case 'alerts':
                $this->reportStrategy = new AlertsReport();
                break;
            case 'summary':
                $this->reportStrategy = new SummaryReport();
                break;
            case 'login':
                $this->reportStrategy = new LoginReport();
                break;
            default:
                throw new \InvalidArgumentException("Unsupported report type: $report");
        }
    }

    public function getAggregatedData(array $data)
    {
        return $this->reportStrategy->getAggregatedData($data);
    }

    public function generateReport(array $data, $type, $format, $reportType = false)
    {
        $this->reportStrategy->generateReport($data, $type, $format, $reportType);
    }
} 