<?php

namespace WebApplication\Controller\Utils\Report;


use WebApplication\Entity\LoginHistory;


class LoginReport extends AbstractReport
{
    /** @Override */
    public function getAggregatedData(array $data)
    {
        $ids = array();
        /** @var LoginHistory $row */
        foreach ($data as $row) {
            $ids[] = $row->getId();
        }

        return $ids;
    }

    /** @Override */
    public function generateReport(array $data, $type, $format, $reportType = false)
    {
        $phpExcel = new \PHPExcel();

        $phpExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '#')
            ->setCellValue('B1', 'Employee')
            ->setCellValue('C1', 'Handset')
            ->setCellValue('D1', 'Login')
            ->setCellValue('E1', 'Logout')
            ->setCellValue('F1', 'Duration');

        /** @var LoginHistory $record */
        foreach ($data as $key => $record) {
            $phpExcel->setActiveSheetIndex(0)
                ->setCellValue(sprintf('A%s', $key + 2), (string) ($key + 1))
                ->setCellValue(sprintf('B%s', $key + 2), (string) $record->getEmployee())
                ->setCellValue(sprintf('C%s', $key + 2), (string) $record->getHandset())
                ->setCellValue(
                    sprintf('D%s', $key + 2),
                    $record->getLogin() ? date_format($record->getLogin(), 'm/d/Y H:i:s') : '')
                ->setCellValue(
                    sprintf('E%s', $key + 2),
                    $record->getLogout() ? date_format($record->getLogout(), 'm/d/Y H:i:s') : '')
                ->setCellValue(
                    sprintf('F%s', $key + 2),
                    $record->getLogin() && $record->getLogout()
                        ? $record->getLogin()->diff($record->getLogout())->format('%d days:%h hours:%i minutes:%s seconds')
                        : ''
                );
        }

        $this->setReportStyles($phpExcel, $type);
        $this->sendGeneratedFile($phpExcel, 'login-history.' . $format, $type, $format);
    }

    /**
     * customize reports styles
     * @Override
     */
    protected function setReportStyles(\PHPExcel $phpExcel, $documentType)
    {
        $phpExcel->getActiveSheet()->setTitle('Login history');

        parent::setReportStyles($phpExcel, $documentType, 'A1:F1');

        $phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
    }
} 