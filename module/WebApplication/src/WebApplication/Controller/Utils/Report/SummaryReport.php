<?php

namespace WebApplication\Controller\Utils\Report;


class SummaryReport extends AbstractReport
{
    /** @Override */
    public function getAggregatedData(array $data)
    {
        if (empty($data)) {
            return array(array(), array('avg' => '00:00:00', 'min' => '00:00:00', 'max' => '00:00:00'));
        }

        $min = $max = 0;
        foreach ($data as &$item) {
            $item['avgTake'] = date('H:i:s', $this->calculateAvgDate($item['minTake'], $item['maxTake']));
            $min += strtotime($item['minTake']);
            $max += strtotime($item['maxTake']);
        }
        unset($item);

        $min = date('H:i:s', $min / sizeof($data));
        $max = date('H:i:s', $max / sizeof($data));
        $avg = date('H:i:s', $this->calculateAvgDate($min, $max));

        return array($data, array('avg' => $avg, 'min' => $min, 'max' => $max));
    }

    /** @Override */
    public function generateReport(array $data, $type, $format, $reportType = false)
    {
        $phpExcel = new \PHPExcel();

        $phpExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '#')
            ->setCellValue('B1', 'Employee')
            ->setCellValue('C1', 'Count')
            ->setCellValue('D1', 'Avg taken time')
            ->setCellValue('E1', 'Min taken time')
            ->setCellValue('F1', 'Max taken time');

        foreach ($data as $key => $record) {
            $phpExcel->setActiveSheetIndex(0)
                ->setCellValue(sprintf('A%s', $key + 2), (string) ($key + 1))
                ->setCellValue(sprintf('B%s', $key + 2), (string) $record['username'])
                ->setCellValue(sprintf('C%s', $key + 2), (string) $record['count'])
                ->setCellValue(
                    sprintf('D%s', $key + 2),
                    date('H:i:s', $this->calculateAvgDate($record['minTake'], $record['maxTake']))
                )
                ->setCellValue(sprintf('E%s', $key + 2), $record['minTake'])
                ->setCellValue(sprintf('F%s', $key + 2), $record['maxTake']);
        }

        $phpExcel->getActiveSheet()->setTitle("Details by date");

        $this->setReportStyles($phpExcel, $type);
        $this->sendGeneratedFile($phpExcel, 'summary-by-dates.' . $format, $type, $format);
    }

    /**
     * customize reports styles
     * @Override
     */
    protected function setReportStyles(\PHPExcel $phpExcel, $documentType)
    {
        $phpExcel->getActiveSheet()->setTitle('Details by date');

        parent::setReportStyles($phpExcel, $documentType, 'A1:F1');

        $phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    }
} 