<?php

namespace WebApplication\Controller\Utils\Report;


abstract class AbstractReport
{
    const PDF_TYPE = 'PDF';
    const EXCEL_TYPE = 'Excel2007';

    abstract public function getAggregatedData(array $data);

    abstract public function generateReport(array $data, $type, $format, $reportType = false);

    protected function sendGeneratedFile(\PHPExcel $phpExcel, $fileName, $type, $format)
    {
        header(sprintf('Content-Disposition: attachment; filename="%s"', $fileName));
        $format == 'xlsx' ? header("Content-Type: application/vnd.ms-excel") : header("Content-Type: application/pdf");
        header("Cache-Control: max-age=0");

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, $type);
        $objWriter->save("php://output");
        exit;
    }

    protected function calculateAvgDate($dateFrom, $dateTo)
    {
        return array_sum(array_map('strtotime', array($dateFrom, $dateTo))) / 2;
    }

    /**
     * customize reports styles
     */
    protected function setReportStyles(\PHPExcel $phpExcel, $documentType, $range)
    {
        if ($documentType == self::PDF_TYPE) {
            $phpExcel->getActiveSheet()->setPageMargins(
                (new \PHPExcel_Worksheet_PageMargins())->setLeft(0.5)->setRight(0.5)->setTop(0.5)->setBottom(0.5)
            );

            $phpExcel->getDefaultStyle()->getFont()->setSize(6);
            $phpExcel->getActiveSheet()->setShowGridlines(false);

            $phpExcel->getDefaultStyle()
                ->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setRGB('EFEFEF');
        }

        $phpExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $phpExcel->getActiveSheet()->getStyle($range)
                 ->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                 ->getStartColor()->setRGB('99CCD9');

        $phpExcel->getActiveSheet()->getStyle($range)->getFont()->getColor()->setRGB('FFFFFF');
    }
} 