<?php

namespace WebApplication\Controller\Utils\Report;

use WebApplication\Entity\Alert;

class AlertsReport extends AbstractReport
{
    /** @Override */
    public function getAggregatedData(array $data)
    {
        if (empty($data)) {
            return array(array(), array('response' => '00:00:00', 'close' => '00:00:00', 'complete' => '00:00:00'));
        }

        $ids = array();
        $responseTime = $closeTime = $completeTime = 0;
        /** @var Alert $alert */
        foreach ($data as $alert) {
            $ids[] = $alert->getId();
            $responseTime += strtotime($alert->getToTake());
            $closeTime += strtotime($alert->getToClose());
            $completeTime += strtotime($alert->getToComplete());
        }

        $avg = array(
            'response' => date("H:i:s", $responseTime / sizeof($data)),
            'close' => date("H:i:s", $closeTime / sizeof($data)),
            'complete' => date("H:i:s", $completeTime / sizeof($data))
        );

        return array($ids, $avg);
    }

    /** @Override */
    public function generateReport(array $data, $type, $format, $reportType = false)
    {
        $phpExcel = new \PHPExcel();

        $phpExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '#')
            ->setCellValue('B1', 'Employee')
            ->setCellValue('C1', 'Location')
            ->setCellValue('D1', 'Opened')
            ->setCellValue('E1', 'Taken')
            ->setCellValue('F1', 'To take')
            ->setCellValue('G1', 'Closed')
            ->setCellValue('H1', 'To close')
            ->setCellValue('I1', 'Completed')
            ->setCellValue('J1', 'To complete');

        /** @var Alert $record */
        foreach ($data as $key => $record) {
            $phpExcel->setActiveSheetIndex(0)
                ->setCellValue(sprintf('A%s', $key + 2), $key + 1)
                ->setCellValue(
                    sprintf('B%s', $key + 2),
                    $record->getEmployee() ? (string) $record->getEmployee()->getUsername() : ''
                )
                ->setCellValue(
                    sprintf('C%s', $key + 2),
                    (string) ($record->getLocation() ? $record->getLocation()->getName() : '')
                )
                ->setCellValue(
                    sprintf('D%s', $key + 2),
                    $record->getOpened() ? date_format($record->getOpened(), 'm/d/Y H:i:s') : ''
                )
                ->setCellValue(
                    sprintf('E%s', $key + 2),
                    $record->getTaken() ? date_format($record->getTaken(),'m/d/Y H:i:s') : ''
                )
                ->setCellValue(sprintf('F%s', $key + 2), $record->getToTake())
                ->setCellValue(
                    sprintf('G%s', $key + 2),
                    $record->getClosed() ? date_format($record->getClosed(),'m/d/Y H:i:s') : ''
                )
                ->setCellValue(sprintf('H%s', $key + 2), $record->getToClose())
                ->setCellValue(
                    sprintf('I%s', $key + 2),
                    $record->getCompleted() ? date_format($record->getCompleted(),'m/d/Y H:i:s') : ''
                )
                ->setCellValue(sprintf('J%s', $key + 2), $record->getToComplete());
        }

        $this->setReportStyles($phpExcel, $type, $reportType);
        $this->sendGeneratedFile($phpExcel, sprintf('details-by-%s.%s', $reportType, $format), $type, $format);
    }

    /**
     * customize reports styles
     * @Override
     */
    protected function setReportStyles(\PHPExcel $phpExcel, $documentType, $reportType = null)
    {
        $phpExcel->getActiveSheet()->setTitle('Details by ' . $reportType);

        parent::setReportStyles($phpExcel, $documentType, 'A1:J1');

        $phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
        $phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
    }
} 