<?php

namespace WebApplication\Controller\Validation;

use WebApplication\Entity\Device;

class DeviceValidator extends BaseValidator
{
    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getDeviceModel(), 'type', 'Type', 'Device');

        return $this;
    }

    public function validateUpdate(array $params, Device $device)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($device->getType() != $params['type']) {
            $this->verifyDuplicate($params, $this->getDeviceModel(), 'type', 'Type', 'Device');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'type', 'Type')
            ->verifyLength($params, 'type', 'Type')
            ->verifyNotEmpty($params, 'workflow', 'Workflow')
            ->verifyInArray($params, 'workflow', 'Workflow', Device::getWorkflows());

        return $this;
    }
}