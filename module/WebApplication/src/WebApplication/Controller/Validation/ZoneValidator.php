<?php

namespace WebApplication\Controller\Validation;

use WebApplication\Entity\Zone;

class ZoneValidator extends BaseValidator
{
    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getZoneModel(), 'name', 'Name', 'Zone');

        return $this;
    }

    public function validateUpdate(array $params, Zone $zone)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($zone->getName() != $params['name']) {
            $this->verifyDuplicate($params, $this->getZoneModel(), 'name', 'Name', 'Zone');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'name', 'Name')
             ->verifyLength($params, 'name', 'Name')
             ->verifyInArray($params, 'sound', 'Sound', Zone::getSounds());

        return $this;
    }
} 