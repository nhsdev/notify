<?php

namespace WebApplication\Controller\Validation;

use WebApplication\Entity\Location;

class LocationValidator extends BaseValidator
{
    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getLocationModel(), 'name', 'Name', 'Location');

        return $this;
    }

    public function validateUpdate(array $params, Location $location, $fromPalatiumCare = false)
    {
        $this->resetErrors();

        if ($fromPalatiumCare) {
            $this->verifyExistence($params, $this->getZoneModel(), 'zoneId', 'Zone');

            return $this;
        }

        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($location->getName() != $params['name']) {
            $this->verifyDuplicate($params, $this->getLocationModel(), 'name', 'Name', 'Location');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'name', 'Name')
             ->verifyLength($params, 'name', 'Name')
             ->verifyNotEmpty($params, 'zoneId', 'Zone')
             ->verifyExistence($params, $this->getZoneModel(), 'zoneId', 'Zone')
             ->verifyInArray($params, 'behaviour', 'Behaviour', Location::getBehaviours());

        return $this;
    }
} 