<?php

namespace WebApplication\Controller\Validation;

use WebApplication\Entity\Hop;

class HopValidator extends BaseValidator
{
    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getHopModel(), 'name', 'Name', 'Repeater')
             ->verifyDuplicate($params, $this->getHopModel(), 'publicId', 'ID', 'Repeater');

        return $this;
    }

    public function validateUpdate(array $params, Hop $hop)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($hop->getName() != $params['name']) {
            $this->verifyDuplicate($params, $this->getHopModel(), 'name', 'Name', 'Repeater');
        }

        if ($hop->getPublicId() != $params['publicId']) {
            $this->verifyDuplicate($params, $this->getHopModel(), 'publicId', 'ID', 'Repeater');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'name', 'Name')
             ->verifyLength($params, 'name', 'Name')
             ->verifyNotEmpty($params, 'publicId', 'Repeater ID')
             ->verifyLength($params, 'publicId', 'Repeater ID')
             ->verifyInArray($params, 'type', 'Type', Hop::getTypes());

        return $this;
    }
} 