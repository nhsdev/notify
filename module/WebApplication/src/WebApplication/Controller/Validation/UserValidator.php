<?php

namespace WebApplication\Controller\Validation;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\User;

class UserValidator extends BaseValidator
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
    }

    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getUserModel(), 'email', 'Email', 'Employee')
             ->verifyDuplicate($params, $this->getUserModel(), 'username', 'Name', 'Employee')
             ->verifyDuplicate($params, $this->getUserModel(), 'voipName', 'VoIP Number', 'Employee');

        if (empty($params['password']) || empty($params['passwordVerify'])) {
            $this->errors['password'] = 'You must enter a password and confirm it';
        } elseif ($params['password'] != $params['passwordVerify']) {
            $this->errors['password'] = 'Password fields are not the same';
        }

        if (!empty($params['role']) && $params['role'] == User::ROLE_MOBILE_USER) {
            $this->verifyNotEmpty($params, 'voipPassword', 'VoIP password');
        }

        return $this;
    }

    public function validateUpdate(array $params, User $user)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($user->getEmail() != $params['email']) {
            $this->verifyDuplicate($params, $this->getUserModel(), 'email', 'Email', 'Employee');
        }

        if ($user->getUsername() != $params['username']) {
            $this->verifyDuplicate($params, $this->getUserModel(), 'username', 'Name', 'Employee');
        }

        if (!empty($params['voipName']) && ($params['voipName'] != $user->getVoipName())) {
            $this->verifyDuplicate($params, $this->getUserModel(), 'voipName', 'VoIP Number', 'Employee');
        }

        if ($params['password'] !== $params['passwordVerify']) {
            $this->errors['password'] = 'Password fields are not the same';
        }

        if (!empty($params['role'])
            && $params['role'] == User::ROLE_MOBILE_USER && $user->getRole() != User::ROLE_MOBILE_USER
        ) {
            $this->verifyNotEmpty($params, 'voipPassword', 'VoIP password');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'email', 'Email')
             ->verifyLength($params, 'email', 'Email')
             ->verifyEmail($params, 'email')
             ->verifyNotEmpty($params, 'username', 'Name')
             ->verifyLength($params, 'username', 'Name')
             ->verifyDigits($params, 'password', 'Password')
             ->verifyDigits($params, 'voipName', 'VoIP Number')
             ->verifyRange($params, 'voipName', 'VoIP Number', 1000, 9999)
             ->verifyLength($params, 'voipPassword', 'VoIP password', 80)
             ->verifyLength($params, 'voipCallerId', 'VoIp Caller Name', 80)
             ->verifyInArray($params, 'role', 'Role', User::getRoles());

        if (!empty($params['role']) && $params['role'] == User::ROLE_MOBILE_USER) {
            $this->verifyNotEmpty($params, 'voipName', 'VoIP Number')
                 ->verifyNotEmpty($params, 'voipCallerId', 'VoIp Caller Name')
                 ->verifyNotEmpty($params, 'zonesIds', 'Zones');
        }

        return $this;
    }
} 