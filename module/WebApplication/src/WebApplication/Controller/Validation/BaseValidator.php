<?php

namespace WebApplication\Controller\Validation;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Controller\ModelShortcut;
use Zend\Validator\EmailAddress;
use Zend\Validator\Digits;
use WebApplication\Model\BaseModel;

abstract class BaseValidator
{
    use ModelShortcut;

    /** @var ServiceManager */
    protected $serviceManager;

    protected $errors = null;

    protected $validators = array();

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;

        $this->validators = array(
            'email' => new EmailAddress(),
            'digits' => new Digits()
        );
    }

    abstract protected function validateFields(array $params);

    public function getErrors()
    {
        return $this->errors;
    }

    public function isValid()
    {
        return is_null($this->errors);
    }

    protected function resetErrors()
    {
        $this->errors = null;

        return $this;
    }

    protected function getServiceLocator()
    {
        return $this->serviceManager;
    }

    protected function verifyNotEmpty(array $params, $key, $fieldName)
    {
        if (empty($params[$key])) {
            $this->errors[$key] = "$fieldName must not be empty";
        }

        return $this;
    }

    protected function verifyLength(array $params, $key, $fieldName, $length = 255)
    {
        if (!empty($params[$key]) && strlen($params[$key]) > $length) {
            $this->errors[$key] = "$fieldName length must be less than $length";
        }

        return $this;
    }

    protected function verifyRange(array $params, $key, $fieldName, $from, $to)
    {
        if (!empty($params[$key]) && $this->validators['digits']->isValid($params[$key])
            && ($params[$key] < $from || $params[$key] > $to)
        ) {
            $this->errors[$key] = "$fieldName must be in $from ... $to";
        }

        return $this;
    }

    protected function verifyEmail(array $params, $key)
    {
        if (!empty($params[$key]) && !$this->validators['email']->isValid($params[$key])) {
            $this->errors[$key] = 'Invalid email address';
        }

        return $this;
    }

    protected function verifyDigits(array $params, $key, $fieldName)
    {
        if (!empty($params[$key]) && !$this->validators['digits']->isValid($params[$key])) {
            $this->errors[$key] = "$fieldName must contain only digits";
        }

        return $this;
    }

    protected function verifyInArray(array $params, $key, $fieldName, array $haystack)
    {
        if (empty($params[$key]) || !in_array($params[$key], $haystack)) {
            $this->errors[$key] = "Invalid $fieldName parameter";
        }
    }

    protected function verifyExistence(array $params, BaseModel $model, $key, $fieldName)
    {
        if (!empty($params[$key]) && !$model->findById($params[$key])) {
            $this->errors[$key] = "$fieldName does not exist";
        }

        return $this;
    }

    protected function verifyDuplicate(array $params, BaseModel $model, $key, $fieldName, $categoryName)
    {
        if (!empty($params[$key]) && $model->findOneBy(array($key => $params[$key]))) {
            $this->errors[$key] = "$categoryName with such $fieldName already exists";
        }

        return $this;
    }
} 