<?php

namespace WebApplication\Controller\Validation;

use WebApplication\Entity\Handset;

class HandsetValidator extends BaseValidator
{
    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getHandsetModel(), 'name', 'Name', 'Handset')
             ->verifyDuplicate($params, $this->getHandsetModel(), 'publicId', 'ID', 'Handset');

        return $this;
    }

    public function validateUpdate(array $params, Handset $handset)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($handset->getName() != $params['name']) {
            $this->verifyDuplicate($params, $this->getHandsetModel(), 'name', 'Name', 'Handset');
        }

        if ($handset->getPublicId() != $params['publicId']) {
            $this->verifyDuplicate($params, $this->getHandsetModel(), 'publicId', 'ID', 'Handset');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'name', 'Name')
             ->verifyLength($params, 'name', 'Name')
             ->verifyLength($params, 'displayName', 'Display Name')
             ->verifyNotEmpty($params, 'publicId', 'Handset ID')
             ->verifyLength($params, 'publicId', 'Handset ID');

        return $this;
    }
} 