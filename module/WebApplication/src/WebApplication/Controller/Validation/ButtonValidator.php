<?php

namespace WebApplication\Controller\Validation;

use WebApplication\Entity\Button;

class ButtonValidator extends BaseValidator
{
    public function validateCreate(array $params)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        $this->verifyDuplicate($params, $this->getButtonModel(), 'publicId', 'ID', 'Button');

        return $this;
    }

    public function validateUpdate(array $params, Button $button)
    {
        $this->resetErrors();
        $this->validateFields($params);

        if (!$this->isValid()) {
            return $this;
        }

        if ($button->getPublicId() != $params['publicId']) {
            $this->verifyDuplicate($params, $this->getButtonModel(), 'publicId', 'ID', 'Button');
        }

        return $this;
    }

    /** @Override */
    protected function validateFields(array $params)
    {
        $this->verifyNotEmpty($params, 'publicId', 'ID')
             ->verifyLength($params, 'publicId', 'ID')
             ->verifyNotEmpty($params, 'locationId', 'Location')
             ->verifyExistence($params, $this->getLocationModel(), 'locationId', 'Location');

        return $this;
    }
} 