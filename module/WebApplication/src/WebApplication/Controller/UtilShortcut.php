<?php

namespace WebApplication\Controller;

trait UtilShortcut
{
    protected function getParamsFromRequest(array $params)
    {
        $result = array();

        foreach ($params as $param) {
            $result[$param] = $this->params()->{$this->getRequest()->isPost() ? 'fromPost' : 'fromQuery'}($param);
        }

        return $result;
    }
}