<?php

namespace WebApplication\Controller;

use Zend\Http\Request;
use WebApplication\Entity\Button;

class ButtonController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'buttons' => $this->getButtonModel()->findAll()
        );
    }

    public function addAction()
    {
        $buttons = $this->getButtonModel()->findAll();
        $locations = $this->getLocationModel()->findAll();

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('publicId', 'locationId'));

            $validationResult = $this->getButtonValidator()->validateCreate($params);
            if (!$validationResult->isValid()) {
                return array(
                    'buttons' => $buttons,
                    'locations' => $locations,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getButtonModel()->create($params);
            $this->flashMessenger()->addMessage(sprintf('Button "%s" was created', $params['publicId']));

            return $this->redirect()->toUrl(self::BUTTON_URL);
        }

        return array(
            'buttons' => $buttons,
            'locations' => $locations
        );
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Button $button */
        if (!$button = $this->getButton($id)) {
            return $this->redirect()->toUrl(self::BUTTON_URL);
        }

        $buttons = $this->getButtonModel()->findAll();
        $locations = $this->getLocationModel()->findAll();

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('publicId', 'locationId'));

            $validationResult = $this->getButtonValidator()->validateUpdate($params, $button);
            if (!$validationResult->isValid()) {
                return array(
                    'buttons' => $buttons,
                    'locations' => $locations,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getButtonModel()->update($button, $params);
            $this->flashMessenger()->addMessage(sprintf('Button "%s" was updated', $params['publicId']));

            return $this->redirect()->toUrl(self::BUTTON_URL);
        }

        return array(
            'buttons' => $buttons,
            'locations' => $locations,
            'params' => array(
                'publicId' => $button->getPublicId(),
                'locationId' => $button->getLocation() ? $button->getLocation()->getId() : null
            )
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');

        /** @var Button $button */
        if (!$button = $this->getButton($id)) {
            return $this->redirect()->toUrl(self::BUTTON_URL);
        }

        if ($this->getAlertModel()->findOneBy(array('button' => $button))) {
            $this->flashMessenger()->addErrorMessage(
                sprintf('You can\'t delete button "%s" because it is used in the alert', $button->getPublicId())
            );
            return $this->redirect()->toUrl(self::BUTTON_URL);
        }

        $this->getButtonModel()->delete($button);
        $this->flashMessenger()->addMessage(sprintf('Button "%s" was deleted', $button->getPublicId()));

        return $this->redirect()->toUrl(self::BUTTON_URL);
    }

    private function getButton($id)
    {
        /** @var Button $button */
        if (!$button = $this->getButtonModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Button with id: %s not found', $id));
            return false;
        }

        if ($button->getPalatiumCareId()) {
            $this->flashMessenger()->addErrorMessage('Sorry, you can\'t modify data from Palatium Care');
            return false;
        }

        return $button;
    }
}