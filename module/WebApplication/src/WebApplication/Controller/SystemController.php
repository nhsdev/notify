<?php

namespace WebApplication\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Json\Json;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\AggregateResolver;
use Zend\View\Resolver\TemplatePathStack;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use WebApplication\Entity\User;
use WebApplication\Entity\Alert;
use ServerApi\Controller\Plugin\PushNotificationPlugin;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 * @method PushNotificationPlugin PushNotificationPlugin()
 */
class SystemController extends AbstractActionController
{
    use ModelShortcut;
    use UtilShortcut;

    const ROUTE_LOGIN = 'zfcuser/login';
    const ROUTE_HOME  = 'system';

    /** @Override */
    public function onDispatch(MvcEvent $e)
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute('zfcuser/login');
        }

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $alerts = $this->getAlertModel()->getAlerts(array('closed' => null), 'system');

        $devices = $this->getDeviceModel()->findAll();

        $params['devicesIds'] = array_map(function($device) {
            return $device->getId();
        }, $devices);

        return array(
            'alerts' => $alerts,
            'users'=> $this->getUserModel()->findLoggedMobileUsers(),
            'serverDateTime' => (new \DateTime())->format('m/d/Y H:i:s'),
            'isAdmin' => $this->zfcUserAuthentication()->getIdentity()->getRole() == User::ROLE_ADMIN,
            'type' => 'system',
            'devices' => $devices,
            'params' => $params
        );
    }

    public function updateAction()
    {
        $params = $this->getParamsFromRequest(
            array('devicesIds')
        );

        $alerts = $this->getAlertModel()->getAlerts(array('closed' => null, 'device' => $params['devicesIds']), 'system');

        $users = $this->getUserModel()->findLoggedMobileUsers();

        return $this->getResponse()->setContent(
            Json::encode(array(
                'alerts' => $this->renderHtml(
                        'web-application/system/alerts-data.phtml',
                        array(
                            'alerts' => $alerts,
                            'isAdmin' => $this->zfcUserAuthentication()->getIdentity()->getRole() == User::ROLE_ADMIN,
                            'type' => 'system'
                        )
                    ),
                'users' => $this->renderHtml(
                        'web-application/system/users-data.phtml',
                        array('users' => $users)
                    ),
                'serverDateTime' => (new \DateTime())->format('m/d/Y H:i:s')
            ))
        );
    }

    public function deleteAction()
    {
        if ($this->zfcUserAuthentication()->getIdentity()->getRole() != User::ROLE_ADMIN) {
            return $this->redirect()->toRoute(self::ROUTE_HOME);
        }

        $id = $this->params('id');
        /** @var Alert $alert */

        if ($alert = $this->getAlertModel()->findById($id) && null !== $alert && 'Alert' !== $alert->getType()) {
            $pushTokens = array();
            $locationName = null;
            if ($alert->getLocation()) {
                $pushTokens = $this->getHandsetModel()->getPushTokensByLocationId(
                    $alert->getLocation()->getId()
                );
                $locationName = $alert->getLocation()->getName();
            }

            $this->getAlertModel()->delete($alert);

            $this->PushNotificationPlugin()->pushNotification(
                array('action' => 'updateAlerts', 'id' => $id),
                $pushTokens,
                'Alert Updated: Location ' . $locationName
            );
        }

        return $this->redirect()->toRoute(self::ROUTE_HOME);
    }


    //TODO remove in future
    public function truncateAction()
    {
        if ($this->zfcUserAuthentication()->getIdentity()->getRole() == User::ROLE_ADMIN) {
            $alerts = $this->getAlertModel()->getAlerts(array('completed' => null), 'index');

            $this->getAlertModel()->deleteNotCompleted();

            $pushTokens = array();

            foreach ($alerts as $alert) {
                if ($alert->getLocation()) {
                    $tokens = $this->getHandsetModel()->getPushTokensByLocationId(
                        $alert->getLocation()->getId()
                    );
                    $pushTokens = array_merge($pushTokens, $tokens);
                }
            }
            $pushTokens = array_unique($pushTokens);

            $this->PushNotificationPlugin()->pushNotification(
                array('action' => 'updateAlerts'),
                $pushTokens
            );
        }

        $this->redirect()->toRoute(self::ROUTE_HOME);
    }

    private function renderHtml($template, array $data = null)
    {
        $view = new ViewModel($data);
        $view->setTemplate($template);
        $view->setTerminal(true);

        $resolver = new AggregateResolver();
        $resolver->attach(
            new TemplatePathStack(array('script_paths' => array(__DIR__ . '/../../../view/')))
        );

        $renderer = new PhpRenderer();
        $renderer->setResolver($resolver);

        return $renderer->render($view);
    }

}