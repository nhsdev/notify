<?php

namespace WebApplication\Controller;

use WebApplication\Controller\Validation\UserValidator;
use WebApplication\Controller\Validation\ZoneValidator;
use WebApplication\Controller\Validation\HandsetValidator;
use WebApplication\Controller\Validation\LocationValidator;
use WebApplication\Controller\Validation\HopValidator;
use WebApplication\Controller\Validation\ButtonValidator;
use WebApplication\Controller\Validation\DeviceValidator;

trait ValidatorShortcut
{
    /** @return UserValidator */
    protected function getUserValidator()
    {
        return $this->getServiceLocator()->get('Validation\User');
    }

    /** @return ZoneValidator */
    protected function getZoneValidator()
    {
        return $this->getServiceLocator()->get('Validation\Zone');
    }

    /** @return HandsetValidator */
    protected function getHandsetValidator()
    {
        return $this->getServiceLocator()->get('Validation\Handset');
    }

    /** @return LocationValidator */
    protected function getLocationValidator()
    {
        return $this->getServiceLocator()->get('Validation\Location');
    }

    /** @return HopValidator */
    protected function getHopValidator()
    {
        return $this->getServiceLocator()->get('Validation\Hop');
    }

    /** @return ButtonValidator */
    protected function getButtonValidator()
    {
        return $this->getServiceLocator()->get('Validation\Button');
    }

    /** @return DeviceValidator */
    protected function getDeviceValidator()
    {
        return $this->getServiceLocator()->get('Validation\Device');
    }
}