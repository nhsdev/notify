<?php

namespace WebApplication\Controller;

use Zend\Http\Request;
use WebApplication\Entity\Hop;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
class HopController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'hops' => $this->getHopModel()->findAll()
        );
    }

    public function addAction()
    {
        $hops = $this->getHopModel()->findAll();
        $types = Hop::getTypes();
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'publicId' ,'type'));

            $validationResult = $this->getHopValidator()->validateCreate($params);
            if (!$validationResult->isValid()) {
                return array(
                    'hops' => $hops,
                    'types' => $types,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getHopModel()->create($params);
            $this->flashMessenger()->addMessage(sprintf('Repeater "%s" was created', $params['name']));

            return $this->redirect()->toUrl(self::HOP_URL);
        }

        return array('hops' => $hops, 'types' => $types);
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Hop $hop */
        if (!$hop = $this->getHopModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Repeater with id:%s not found', $id));
            return $this->redirect()->toUrl(self::HOP_URL);
        }

        $hops = $this->getHopModel()->findAll();
        $types = Hop::getTypes();
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('name', 'publicId' ,'type'));

            $validationResult = $this->getHopValidator()->validateUpdate($params, $hop);
            if (!$validationResult->isValid()) {
                return array(
                    'hops' => $hops,
                    'types' => $types,
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getHopModel()->update($hop, $params);
            $this->flashMessenger()->addMessage(sprintf('Repeater "%s" was updated', $params['name']));

            return $this->redirect()->toUrl(self::HOP_URL);
        }

        return array(
            'hops' => $hops,
            'types' => $types,
            'params' => array(
                'name' => $hop->getName(),
                'publicId' => $hop->getPublicId(),
                'type' => $hop->getType()
            )
        );
    }

    public function deleteAction()
    {
        $id = $this->params('id');

        /** @var Hop $hop */
        if (!$hop = $this->getHopModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Repeater with id:%s not found', $id));
            return $this->redirect()->toUrl(self::HOP_URL);
        }

        $this->getHopModel()->delete($hop);
        $this->flashMessenger()->addMessage(sprintf('Repeater "%s" was deleted', $hop->getName()));

        return $this->redirect()->toUrl(self::HOP_URL);
    }
}