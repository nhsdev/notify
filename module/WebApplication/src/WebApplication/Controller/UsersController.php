<?php

namespace WebApplication\Controller;

use WebApplication\Entity\User;
use Zend\View\Model\ViewModel;
use ZfcUser\Controller\UserController;
use Zend\Session\Container;
use Zend\Stdlib\ResponseInterface as Response;
use ZfcUser\Controller\Plugin\ZfcUserAuthentication;
use ZfcUser\Authentication\Adapter\AdapterChain as AuthAdapter;
use ZfcUser\Options\ModuleOptions as ZfcUserModuleOptions;
use Zend\Http\Request;
use Zend\Form\Form;

/**
 * @method ZfcUserAuthentication zfcUserAuthentication()
 */
class UsersController extends UserController
{
    use ModelShortcut;
    use ValidatorShortcut;
    use UtilShortcut;

    const ROUTE_LOGOUT     = 'zfcuser/logout';
    const CONTROLLER_NAME  = 'webapplication';
    const ROUTE_HOME       = 'home';


    public function indexAction()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute(static::ROUTE_LOGIN);
        }

        return new ViewModel();
    }

    public function loginAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        /** @var Form $form */
        $form = $this->getLoginForm();

        $redirect = $this->getOptions()->getUseRedirectParameterIfPresent() && $request->getQuery()->get('redirect')
            ? $request->getQuery()->get('redirect')
            : false;

        if (!$request->isPost()) {
            return array(
                'loginForm' => $form,
                'redirect'  => $redirect,
                'enableRegistration' => $this->getOptions()->getEnableRegistration(),
            );
        }

        $form->setData($request->getPost());

        if (!$form->isValid()) {
            $this->flashMessenger()->setNamespace('zfcuser-login-form')->addMessage($this->failedLoginMessage);
            return $this->redirect()->toUrl(
                sprintf(
                    '%s%s',
                    $this->url()->fromRoute(static::ROUTE_LOGIN),
                    $redirect ? "?redirect=$redirect" : ""
                )
            );
        }

        // clear adapters
        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();


        return $this->forward()->dispatch(static::CONTROLLER_NAME, array('action' => 'authenticate'));
    }

    public function logoutAction()
    {
        if ($this->zfcUserAuthentication()->getIdentity()->getSession() != null) {
            $this->getUserModel()->deleteSession($this->zfcUserAuthentication()->getIdentity());
            $this->getLoginHistoryModel()->trackLogout($this->zfcUserAuthentication()->getIdentity());
        }

        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthAdapter()->logoutAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();

        $redirect = $this->params()->fromPost('redirect', $this->params()->fromQuery('redirect', false));

        if ($this->getOptions()->getUseRedirectParameterIfPresent() && $redirect) {
            return $this->redirect()->toUrl($redirect);
        }

        return $this->redirect()->toRoute($this->getOptions()->getLogoutRedirectRoute());
    }

    public function authenticateAction()
    {
        if ($this->zfcUserAuthentication()->getAuthService()->hasIdentity()) {
            return $this->redirect()->toRoute($this->getOptions()->getLoginRedirectRoute());
        }

        /** @var AuthAdapter $adapter */
        $adapter = $this->zfcUserAuthentication()->getAuthAdapter();
        $redirect = $this->params()->fromPost('redirect', $this->params()->fromQuery('redirect', false));

        $result = $adapter->prepareForAuthentication($this->getRequest());

        // Return early if an adapter returned a response
        if ($result instanceof Response) {
            return $result;
        }

        $auth = $this->zfcUserAuthentication()->getAuthService()->authenticate($adapter);

        if (!$auth->isValid()) {
            $this->flashMessenger()->setNamespace('zfcuser-login-form')->addMessage($this->failedLoginMessage);
            $adapter->resetAdapters();

            return $this->redirect()->toUrl(
                $this->url()->fromRoute(static::ROUTE_LOGIN) . ($redirect ? '?redirect=' . $redirect : '')
            );
        }

        if ($this->getOptions()->getUseRedirectParameterIfPresent() && $redirect) {
            return $this->redirect()->toUrl($redirect);
        }

        if ($this->zfcUserAuthentication()->hasIdentity()
            && (!$this->zfcUserAuthentication()->getIdentity()->getEnabled()
                || $this->zfcUserAuthentication()->getIdentity()->getRole() == User::ROLE_MOBILE_USER)
        ) {
            $this->flashMessenger()->setNamespace('zfcuser-login-form')->addMessage(
                'Your account is disabled or you are not allowed to enter Admin panel due to account role.'
            );

            return $this->redirect()->toRoute(static::ROUTE_LOGOUT);
        }

        if ($this->zfcUserAuthentication()->hasIdentity()
            && $this->zfcUserAuthentication()->getIdentity()->getSession() == null
        ) {
            $this->getUserModel()->addSession($this->zfcUserAuthentication()->getIdentity());
            $this->getLoginHistoryModel()->trackLogin($this->zfcUserAuthentication()->getIdentity());
        }

        return $this->redirect()->toRoute(static::ROUTE_HOME);
    }

    /**
     * @Override
     * @return ZfcUserModuleOptions
     */
    public function getOptions()
    {
        if (!$this->options instanceof ZfcUserModuleOptions) {
            $this->setOptions($this->getServiceLocator()->get('zfcuser_module_options'));
        }

        return $this->options;
    }
}