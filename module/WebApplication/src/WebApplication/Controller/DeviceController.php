<?php

namespace WebApplication\Controller;

use Zend\Http\Request;
use WebApplication\Entity\Device;

class DeviceController extends AbstractAdminController
{
    public function indexAction()
    {
        return array(
            'devices' => $this->getDeviceModel()->findAll()
        );
    }

    public function addAction()
    {
        $this->flashMessenger()->addErrorMessage('Sorry, you can\'t add device');
        return $this->redirect()->toUrl(self::DEVICE_URL);

//        $devices = $this->getDeviceModel()->findAll();
//
//        /** @var Request $request */
//        $request = $this->getRequest();
//
//        if ($request->isPost()) {
//            $params = $this->getParamsFromRequest(array('type', 'workflow'));
//
//            $validationResult = $this->getDeviceValidator()->validateCreate($params);
//            if (!$validationResult->isValid()) {
//                return array(
//                    'devices' => $devices,
//                    'workflows' => Device::getWorkflows(),
//                    'errors' => $validationResult->getErrors(),
//                    'params' => $params
//                );
//            }
//
//            $this->getDeviceModel()->create($params);
//            $this->flashMessenger()->addMessage(sprintf('Device "%s" was created', $params['type']));
//
//            return $this->redirect()->toUrl(self::DEVICE_URL);
//        }
//
//        return array(
//            'devices' => $devices,
//            'workflows' => Device::getWorkflows(),
//        );
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Device $device*/
        if (!$device = $this->getDevice($id)) {
            return $this->redirect()->toUrl(self::DEVICE_URL);
        }

        $devices = $this->getDeviceModel()->findAll();

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $params = $this->getParamsFromRequest(array('type', 'workflow'));

            $validationResult = $this->getDeviceValidator()->validateUpdate($params, $device);
            if (!$validationResult->isValid()) {
                return array(
                    'devices' => $devices,
                    'workflows' => Device::getWorkflows(),
                    'errors' => $validationResult->getErrors(),
                    'params' => $params
                );
            }

            $this->getDeviceModel()->update($device, $params);
            $this->flashMessenger()->addMessage(sprintf('Device "%s" was updated', $device->getType()));

            return $this->redirect()->toUrl(self::DEVICE_URL);
        }

        return array(
            'devices' => $devices,
            'workflows' => Device::getWorkflows(),
            'params' => array(
                'type' => $device->getType(),
                'workflow' => $device->getWorkflow()
            )
        );
    }

    public function deleteAction()
    {
        $this->flashMessenger()->addErrorMessage('Sorry, you can\'t delete device');
        return $this->redirect()->toUrl(self::DEVICE_URL);

//        $id = $this->params('id');
//
//        /** @var Device $device */
//        if (!$device = $this->getDevice($id)) {
//            return $this->redirect()->toUrl(self::DEVICE_URL);
//        }
//
//        if ($this->getAlertModel()->findOneBy(array('device' => $device))) {
//            $this->flashMessenger()->addErrorMessage(
//                sprintf('You can\'t delete device "%s" because it is used in the alert', $device->getType())
//            );
//            return $this->redirect()->toUrl(self::DEVICE_URL);
//        }
//
//        $this->getDeviceModel()->delete($device);
//        $this->flashMessenger()->addMessage(sprintf('Device "%s" was deleted', $device->getType()));
//
//        return $this->redirect()->toUrl(self::DEVICE_URL);
    }

    private function getDevice($id)
    {
        /** @var Device $device */
        if (!$device= $this->getDeviceModel()->findById($id)) {
            $this->flashMessenger()->addErrorMessage(sprintf('Device with id: %s not found', $id));
            return false;
        }

        return $device;
    }
}