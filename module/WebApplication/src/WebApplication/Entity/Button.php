<?php

namespace WebApplication\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="buttons")
 */
class Button
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="palatium_care_id", type="integer", nullable=true, unique=true)
     */
    protected $palatiumCareId;

    /**
     * @var string
     * @ORM\Column(name="public_id", type="string", length=255, unique=true, nullable=false)
     */
    protected $publicId;

    /**
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $location;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getPalatiumCareId()
    {
        return $this->palatiumCareId;
    }

    /**
     * @param string $palatiumCareId
     * @return Button
     */
    public function setPalatiumCareId($palatiumCareId)
    {
        $this->palatiumCareId = $palatiumCareId;

        return $this;
    }

    /**
     * @param string $publicId
     * @return Button
     */
    public function setPublicId($publicId)
    {
        $this->publicId = $publicId;

        return $this;
    }

    /**
     * @return string 
     */
    public function getPublicId()
    {
        return $this->publicId;
    }

    /**
     * Set location
     *
     * @param Location $location
     * @return Button
     */
    public function setLocation(Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }
}