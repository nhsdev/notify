<?php

namespace WebApplication\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="alerts")
 */
class Alert
{
    const STATUS_UNTAKEN = 1;
    const STATUS_TAKEN = 2;
    const STATUS_CLOSED = 3;
    const STATUS_COMPLETED = 4;

    const REPEAT_TIMEOUT = 30;
    const CLOSE_TIMEOUT = 5;

    const TIME_INTERVAL_FORMAT = '%H:%I:%S';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="palatium_care_id", type="integer", nullable=true, unique=true)
     */
    protected $palatiumCareId;

    /**
     * @ORM\ManyToOne(targetEntity="Hop")
     * @ORM\JoinColumn(name="hop_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $hop;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $near;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $opened;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $taken;

    /**
     * @var string
     * @ORM\Column(name="to_take", type="string", length=32, nullable=true)
     */
    protected $toTake;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $closed;

    /**
     * @var string
     * @ORM\Column(name="to_close", type="string", length=32, nullable=true)
     */
    protected $toClose;

    /**
     * @var boolean
     * @ORM\Column(name="is_closed", type="boolean", nullable=true)
     */
    protected $isClosed;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $completed;

    /**
     * @var string
     * @ORM\Column(name="to_complete", type="string", length=32, nullable=true)
     */
    protected $toComplete;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $employee;

    /**
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $location;

    /**
     * @ORM\OneToOne(targetEntity="Device")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $device;

    /**
     * @var ArrayCollection $zone
     *
     * @ORM\ManyToMany(targetEntity="Resident")
     * @ORM\JoinTable(
     *      name="alerts_residents",
     *      joinColumns={@ORM\JoinColumn(name="alert_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="resident_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $residents;

    /**
     * @var string
     * @ORM\Column(name="date_time", type="string", length=80, nullable=true)
     */
    protected $dateTime;

    /**
     * @var string
     * @ORM\Column(name="ask_date_time", type="string", length=80, nullable=true)
     */
    protected $askDateTime;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=80, nullable=true)
     */
    protected $type;

    public function __construct()
    {
        $this->residents = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getPalatiumCareId()
    {
        return $this->palatiumCareId;
    }

    /**
     * @param string $palatiumCareId
     * @return Alert
     */
    public function setPalatiumCareId($palatiumCareId)
    {
        $this->palatiumCareId = $palatiumCareId;

        return $this;
    }

    /**
     * Set opened
     *
     * @param \DateTime $opened
     * @return Alert
     */
    public function setOpened($opened)
    {
        $this->opened = $opened;

        return $this;
    }

    /**
     * Get opened
     *
     * @return \DateTime 
     */
    public function getOpened()
    {
        return $this->opened;
    }

    /**
     * Set taken
     *
     * @param \DateTime $taken
     * @return Alert
     */
    public function setTaken($taken)
    {
        $this->taken = $taken;

        return $this;
    }

    /**
     * Get taken
     *
     * @return \DateTime 
     */
    public function getTaken()
    {
        return $this->taken;
    }

    /**
     * Set closed
     *
     * @param \DateTime $closed
     * @return Alert
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return \DateTime 
     */
    public function getClosed()
    {
        return $this->closed;
    }


    /**
     * Delay close
     *
     * @return $this
     */
    public function delayClose()
    {
        $this->isClosed = true;

        return $this;
    }

    /**
     * Get closed
     *
     * @return int
     */
    public function getDelayClose()
    {
        return $this->isClosed;
    }

    /**
     * Set completed
     *
     * @param \DateTime $completed
     * @return Alert
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return \DateTime 
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set employee
     *
     * @param User $employee
     * @return Alert
     */
    public function setEmployee(User $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return User
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set location
     *
     * @param Location $location
     * @return Alert
     */
    public function setLocation(Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \WebApplication\Entity\Location 
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Set toTake
     *
     * @param string $toTake
     * @return Alert
     */
    public function setToTake($toTake)
    {
        $this->toTake = $toTake;

        return $this;
    }

    /**
     * Get toTake
     *
     * @return string 
     */
    public function getToTake()
    {
        return $this->toTake;
    }

    /**
     * Set toClose
     *
     * @param string $toClose
     * @return Alert
     */
    public function setToClose($toClose)
    {
        $this->toClose = $toClose;

        return $this;
    }

    /**
     * Get toClose
     *
     * @return string 
     */
    public function getToClose()
    {
        return $this->toClose;
    }

    /**
     * Set toComplete
     *
     * @param string $toComplete
     * @return Alert
     */
    public function setToComplete($toComplete)
    {
        $this->toComplete = $toComplete;

        return $this;
    }

    /**
     * Get toComplete
     *
     * @return string 
     */
    public function getToComplete()
    {
        return $this->toComplete;
    }

    /**
     * @param Hop $hop
     * @return Alert
     */
    public function setHop(Hop $hop = null)
    {
        $this->hop = $hop;

        return $this;
    }

    /**
     * @return Hop
     */
    public function getHop()
    {
        return $this->hop;
    }

    /**
     * @param $near
     * @return Alert
     */
    public function setNear($near)
    {
        $this->near = $near;

        return $this;
    }

    /**
     * @return string
     */
    public function getNear()
    {
        return $this->near;
    }

    /**
     * @param $device
     * @return Alert
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @return Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Add resident
     *
     * @param Resident $resident
     * @return $this
     */
    public function addResident(Resident $resident)
    {
        $this->residents->add($resident);

        return $this;
    }

    /**
     * Remove resident
     *
     * @param Resident $resident
     * @return $this
     */
    public function removeResident(Resident $resident)
    {
        $this->residents->removeElement($resident);

        return $this;
    }

    /**
     * @return $this
     */
    public function truncateResidents()
    {
        $this->residents = new ArrayCollection();

        return $this;
    }

    /**
     * Get residents
     *
     * @return ArrayCollection
     */
    public function getResidents()
    {
        return $this->residents;
    }

    /**
     * @return array
     */
    public function getResidentsIds()
    {
        $result = array();
        if ($this->residents->count()) {
            /** @var Resident $resident */
            foreach ($this->residents->toArray() as $resident) {
                $result[] = $resident->getId();
            }
        }

        return $result;
    }

    /**
     * @param $dateTime
     * @return $this
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @param $askDateTime
     * @return $this
     */
    public function setAskDateTime($askDateTime)
    {
        $this->askDateTime = $askDateTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getAskDateTime()
    {
        return $this->askDateTime;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public static function getMaintenanceTypes()
    {
        return array('No Signal', 'Low Battery', 'Power Lost');
    }

}