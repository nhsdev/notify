<?php
namespace WebApplication\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="settings")
 *
 */

class Setting
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="server_ip", type="string", length=50, nullable=true)
     */
    protected $serverIp;

    /**
     * @var string
     * @ORM\Column(name="console_name", type="string", length=50, nullable=true)
     */
    protected $consoleName;

    /**
     * @var string
     * @ORM\Column(name="admin_unlock_code", type="string", length=50, nullable=true)
     */
    protected $adminUnlockCode;

    /**
     * @var int
     * @ORM\Column(name="wall_mode", type="integer", length=11, nullable=true)
     */
    protected $wallMode;

    /**
     * @param string $adminUnlockCode
     */
    public function setAdminUnlockCode($adminUnlockCode)
    {
        $this->adminUnlockCode = $adminUnlockCode;
    }

    /**
     * @return string
     */
    public function getAdminUnlockCode()
    {
        return $this->adminUnlockCode;
    }

    /**
     * @param string $consoleName
     */
    public function setConsoleName($consoleName)
    {
        $this->consoleName = $consoleName;
    }

    /**
     * @return string
     */
    public function getConsoleName()
    {
        return $this->consoleName;
    }

    /**
     * @param string $serverIp
     */
    public function setServerIp($serverIp)
    {
        $this->serverIp = $serverIp;
    }

    /**
     * @return string
     */
    public function getServerIp()
    {
        return $this->serverIp;
    }

    /**
     * @param int $wallMode
     */
    public function setWallMode($wallMode)
    {
        $this->wallMode = $wallMode;
    }

    /**
     * @return int
     */
    public function getWallMode()
    {
        return $this->wallMode;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}