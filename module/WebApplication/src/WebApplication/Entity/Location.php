<?php
namespace WebApplication\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="locations")
 *
 */

class Location
{
    const BEHAVIOUR_STATIC = 'static';
    const BEHAVIOUR_MOBILE = 'mobile';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="palatium_care_id", type="integer", nullable=true, unique=true)
     */
    protected $palatiumCareId;

    /**
     * @var string
     * @ORM\Column(name="name",type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone_id", referencedColumnName="id")
     */
    protected $zone;

    /**
     * @var string
     * @ORM\Column(name="behavior",type="string", length=255, nullable=true)
     */
    protected $behavior;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $enabled;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getPalatiumCareId()
    {
        return $this->palatiumCareId;
    }

    /**
     * @param string $palatiumCareId
     * @return Location
     */
    public function setPalatiumCareId($palatiumCareId)
    {
        $this->palatiumCareId = $palatiumCareId;

        return $this;
    }

    /**
     * @param string $name
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set behavior
     *
     * @param string $behavior
     * @return Location
     */
    public function setBehavior($behavior)
    {
        $this->behavior = $behavior;

        return $this;
    }

    /**
     * Get behavior
     *
     * @return string 
     */
    public function getBehavior()
    {
        return $this->behavior;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Location
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set zone
     *
     * @param Zone $zone
     * @return Location
     */
    public function setZone(Zone $zone = null)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return Zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    public static function getBehaviours()
    {
        return array(self::BEHAVIOUR_MOBILE, self::BEHAVIOUR_STATIC);
    }
}