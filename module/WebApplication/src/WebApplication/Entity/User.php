<?php

namespace WebApplication\Entity;

//use BjyAuthorize\Provider\Role\ProviderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ZfcUser\Entity\UserInterface;

/**
 * An example of how to implement a role aware user entity.
 *
 * @ORM\Entity
 * @ORM\Table(name="users")
 *
 * @author Tom Oram <tom@scl.co.uk>
 */
//class User implements UserInterface, ProviderInterface
class User implements UserInterface
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_MOBILE_USER = 'mobile user';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true,  length=255)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="display_name", type="string", length=50, nullable=true)
     */
    protected $displayName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    protected $password;

    /**
     * @var int
     */
    protected $state;

    /**
     * @var ArrayCollection $zone
     *
     * @ORM\ManyToMany(targetEntity="Zone")
     * @ORM\JoinTable(
     *      name="users_zones",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="zone_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $zones;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true,  length=50, nullable=true)
     */

    protected $session;

    /**
     * @var string
     * @ORM\Column(name="auth_token", type="string", unique=true,  length=50, nullable=true)
     */
    protected $authToken;

    /**
     * @var string
     * @ORM\Column(name="voip_name", type="string", unique=true, length=80, unique=true, nullable=true)
     */
    protected $voipName;

    /**
     * @var string
     * @ORM\Column(name="voip_password", type="string", length=80, nullable=true)
     */
    protected $voipPassword;

    /**
     * @var string
     * @ORM\Column(name="voip_caller_id", type="string", length=80, nullable=true)
     */
    protected $voipCallerId;

    /**
     * @var integer
     * @ORM\Column(type="integer", length=4, nullable=true)
     */
    protected $pin;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $enabled = 1;

    /**
     * @ORM\OneToOne(targetEntity="Handset")
     * @ORM\JoinColumn(name="handset_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $handset;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    protected $role;


    public function __construct()
    {
        $this->zones = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     *
     * @return void
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param int $state
     *
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get role.
     *
     * @return array
     */
//    public function getRoles()
//    {
//        return $this->roles->getValues();
//    }
//
//    /**
//     * Add a role to the user.
//     *
//     * @param Role $role
//     *
//     * @return void
//     */
//    public function addRole($role)
//    {
//        $this->roles[] = $role;
//    }

    /**
     * Set session
     *
     * @param string $session
     * @return User
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string 
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set authToken
     *
     * @param string $authToken
     * @return User
     */
    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;

        return $this;
    }

    /**
     * Get authToken
     *
     * @return string 
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * Set voipName
     *
     * @param string $voipName
     * @return User
     */
    public function setVoipName($voipName)
    {
        $this->voipName = $voipName;

        return $this;
    }

    /**
     * Get voipName
     *
     * @return string 
     */
    public function getVoipName()
    {
        return $this->voipName;
    }

    /**
     * Set voipPassword
     *
     * @param string $voipPassword
     * @return User
     */
    public function setVoipPassword($voipPassword)
    {
        $this->voipPassword = $voipPassword;

        return $this;
    }

    /**
     * Get voipPassword
     *
     * @return string 
     */
    public function getVoipPassword()
    {
        return $this->voipPassword;
    }

    /**
     * @param string $voipCallerId
     * @return User
     */
    public function setVoipCallerId($voipCallerId)
    {
        $this->voipCallerId = $voipCallerId;

        return $this;
    }

    /**
     * @return string
     */
    public function getVoipCallerId()
    {
        return $this->voipCallerId;
    }

    /**
     * Set pin
     *
     * @param integer $pin
     * @return User
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return integer 
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;    }


    /**
     * Set handset
     *
     * @param Handset $handset
     * @return User
     */
    public function setHandset(Handset $handset = null)
    {
        $this->handset = $handset;

        return $this;
    }

    /**
     * Get handset
     *
     * @return Handset
     */
    public function getHandset()
    {
        return $this->handset;
    }

    /**
     * Add zone
     *
     * @param Zone $zone
     * @return User
     */
    public function addZone(Zone $zone)
    {
        $this->zones->add($zone);

        return $this;
    }

    /**
     * Remove zone
     *
     * @param Zone $zone
     * @return User
     */
    public function removeZone(Zone $zone)
    {
        $this->zones->removeElement($zone);

        return $this;
    }

    /**
     * @return User
     */
    public function truncateZones()
    {
        $this->zones = new ArrayCollection();

        return $this;
    }

    /**
     * Get zone
     *
     * @return ArrayCollection
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * @return array
     */
    public function getZonesIds()
    {
        $result = array();
        if ($this->zones->count()) {
            /** @var Zone $zone */
            foreach ($this->zones->toArray() as $zone) {
                $result[] = $zone->getId();
            }
        }

        return $result;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    public static function getRoles()
    {
        return array(self::ROLE_MOBILE_USER, self::ROLE_USER, self::ROLE_ADMIN);
    }
}