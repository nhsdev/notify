<?php

namespace WebApplication\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="zones")
 *
 */
class Zone
{
    const SOUND_TRITONE = 'BeepPing';
    const SOUND_KLAXON = 'Click';
    const SOUND_BEEPERHALF = 'ATone';
    const SOUND_BEEPERQRT = 'DoorBell';

    const PALATIUM_CARE_ZONE = 'Zone1';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="sound", type="string", length=255, nullable=true)
     */
    protected $sound;

    /**
     * @var int
     * @ORM\Column(name="sound_count", type="integer", nullable=true)
     */
    protected $soundCount;

    /**
     * @var int
     * @ORM\Column(name="sound_interval", type="integer", nullable=true)
     */
    protected $soundInterval;

    /**
     * @var ArrayCollection $user
     *
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(
     *      name="users_zones",
     *      joinColumns={@ORM\JoinColumn(name="zone_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $users;


    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $sound
     */
    public function setSound($sound)
    {
        $this->sound = $sound;
    }

    /**
     * @return string
     */
    public function getSound()
    {
        return $this->sound;
    }

    /**
     * @param int $soundCount
     */
    public function setSoundCount($soundCount)
    {
        $this->soundCount = $soundCount;
    }

    /**
     * @return int
     */
    public function getSoundCount()
    {
        return $this->soundCount;
    }

    /**
     * @param int $soundInterval
     */
    public function setSoundInterval($soundInterval)
    {
        $this->soundInterval = $soundInterval;
    }

    /**
     * @return int
     */
    public function getSoundInterval()
    {
        return $this->soundInterval;
    }

    /**
     * @param $name
     * @return Zone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add user
     *
     * @param User $user
     * @return Zone
     */
    public function addUser(User $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return Zone
     */
    public function truncateUsers()
    {
        $this->users = new ArrayCollection();

        return $this;
    }

    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public static function getSounds()
    {
        return array(self::SOUND_TRITONE, self::SOUND_KLAXON, self::SOUND_BEEPERHALF, self::SOUND_BEEPERQRT);
    }

    public static function getSoundIntervals()
    {
        return array(15 => '15 sec', 30 => '30 sec', 45 => '45 sec', 60 => '1 min', 120 => '2 min', 180 => '3 min', 300 => '5 min');
    }
}