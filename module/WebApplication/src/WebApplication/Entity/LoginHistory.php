<?php

namespace WebApplication\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="login_history")
 *
 */
class LoginHistory
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $employee;

    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $handset;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $login;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $logout;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employee
     *
     * @param string $employee
     * @return LoginHistory
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return string 
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set login
     *
     * @param \DateTime $login
     * @return LoginHistory
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return \DateTime 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set logout
     *
     * @param \DateTime $logout
     * @return LoginHistory
     */
    public function setLogout($logout)
    {
        $this->logout = $logout;

        return $this;
    }

    /**
     * Get logout
     *
     * @return \DateTime 
     */
    public function getLogout()
    {
        return $this->logout;
    }

    /**
     * Set handset
     *
     * @param string $handset
     * @return LoginHistory
     */
    public function setHandset($handset)
    {
        $this->handset = $handset;

        return $this;
    }

    /**
     * Get handset
     *
     * @return string 
     */
    public function getHandset()
    {
        return $this->handset;
    }
}