<?php
namespace WebApplication\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="residents")
 *
 */

class Resident
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="firstName",type="string", length=255, nullable=false)
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(name="lastName",type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * @var string
     * @ORM\Column(name="nickname",type="string", length=255, nullable=true)
     */
    protected $nickname;

    /**
     * @var ArrayCollection $zone
     *
     * @ORM\ManyToMany(targetEntity="Alert")
     * @ORM\JoinTable(
     *      name="alerts_residents",
     *      joinColumns={@ORM\JoinColumn(name="resident_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="alert_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $alerts;

    public function __construct()
    {
        $this->alerts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $nickname
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * Add alert
     *
     * @param Alert $alert
     * @return $this
     */
    public function addAlert(Alert $alert)
    {
        $this->alerts->add($alert);

        return $this;
    }

    /**
     * Remove alert
     *
     * @param Alert $alert
     * @return $this
     */
    public function removeAlert(Alert $alert)
    {
        $this->alerts->removeElement($alert);

        return $this;
    }

    /**
     * @return $this
     */
    public function truncateAlerts()
    {
        $this->alerts = new ArrayCollection();

        return $this;
    }

    /**
     * Get alerts
     *
     * @return ArrayCollection
     */
    public function getAlerts()
    {
        return $this->alerts;
    }
}