<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Hop;

class HopModel extends BaseModel
{
    const TMP_NAME_PREFIX = 'tmp_';

    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Hop');
    }

    /**
     * @param $params
     * @return Hop
     * @throws \InvalidArgumentException
     */
    public function create($params)
    {
        if (empty($params['publicId'])) {
            throw new \InvalidArgumentException('Hop publicId must not be empty');
        }

        $hop = new Hop();
        $hop->setPublicId($params['publicId']);
        $hop->setType(!empty($params['type']) ? $params['type'] : Hop::TYPE_REPEATER);

        $this->entityManager->persist($hop);
        $this->entityManager->flush();

        $hop->setName(!empty($params['name']) ? $params['name'] : self::TMP_NAME_PREFIX . $hop->getId());

        $this->entityManager->persist($hop);
        $this->entityManager->flush();

        return $hop;
    }

    /**
     * @param Hop $hop
     * @param array $params
     */
    public function update(Hop $hop, array $params)
    {
        if (!empty($params['name'])) {
            $hop->setName($params['name']);
        }

        if (!empty($params['publicId'])) {
            $hop->setPublicId($params['publicId']);
        }

        if (!empty($params['type'])) {
            $hop->setType($params['type']);
        }

        $this->entityManager->persist($hop);
        $this->entityManager->flush();
    }

    /**
     * @param Hop $hop
     */
    public function delete(Hop $hop)
    {
        $this->entityManager->remove($hop);
        $this->entityManager->flush();
    }
}