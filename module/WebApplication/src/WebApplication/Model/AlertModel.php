<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Alert;
use WebApplication\Entity\Button;
use WebApplication\Entity\Hop;
use WebApplication\Entity\User;
use WebApplication\Entity\Location;
use WebApplication\Entity\Device;
use Doctrine\DBAL\Types\Type as DoctrineType;

class AlertModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Alert');
    }

    /**
     * @param Alert $alert
     * @return int|null
     */
    public function getAlertState(Alert $alert)
    {
        $state = null;

        if ($alert->getOpened() && !$alert->getTaken() && !$alert->getClosed() && !$alert->getCompleted()) {
            $state = Alert::STATUS_UNTAKEN;
        } elseif ($alert->getOpened() && $alert->getTaken() && !$alert->getClosed() && !$alert->getCompleted()) {
            $state = Alert::STATUS_TAKEN;
        } elseif ($alert->getOpened() && $alert->getTaken() && $alert->getClosed() && !$alert->getCompleted()) {
            $state = Alert::STATUS_CLOSED;
        } elseif ($alert->getOpened() && $alert->getTaken() && $alert->getClosed() && $alert->getCompleted()) {
            $state = Alert::STATUS_COMPLETED;
        }

        return $state;
    }

    /**
     * @param Button $button
     * @param Hop $hop
     * @param null $palatiumCareId
     * @param null $near
     * @return Alert
     * @throws \LogicException
     */
    public function createAlertByButton(Button $button, Hop $hop = null, $palatiumCareId = null, $near = null)
    {
        /** @var Alert $alert */
        $alert = $this->findOneBy(array('button' => $button), array('id' => 'desc'));

        if (empty($alert)) {
            return $this->create($button, $hop, $palatiumCareId, $near);
        }

        if (!$alert->getCompleted()) {
            throw new \LogicException("Failed: Can't send another alert because previous one does not complete");
        }

        if ($alert->getCompleted()
            && $this->calculateSecondsDiff(new \DateTime(), $alert->getClosed()) < Alert::CLOSE_TIMEOUT) {
            throw new \LogicException(
                sprintf('Failed: Can send another alert only after %s seconds', Alert::CLOSE_TIMEOUT)
            );
        }

        return $this->create($button, $hop, $palatiumCareId, $near);
    }

    /**
     * @param Location $location
     * @param $dateTime
     * @param Hop $hop
     * @param Device $device
     * @param null $residents
     * @param null $near
     * @param null $type
     * @return Alert
     * @throws \LogicException
     */
    public function createAlertByLocation(Location $location, $dateTime, Hop $hop = null, Device $device = null, $residents = null, $near = null, $type = null, $ackDateTime = null)
    {
        $alert = $this->findOneBy(array('location' => $location, 'dateTime' => $dateTime), array('id' => 'desc'));

        if (empty($alert)) {
            return $this->createV2($location, $dateTime, $hop, $device, $residents, $near, $type, $ackDateTime);
        }

        if (!$alert->getCompleted()) {
            throw new \LogicException("Failed: Can't send another alert because previous one does not complete");
        }

        if ($alert->getCompleted()
            && $this->calculateSecondsDiff(new \DateTime(), $alert->getClosed()) < Alert::CLOSE_TIMEOUT) {
            throw new \LogicException(
                sprintf('Failed: Can send another alert only after %s seconds', Alert::CLOSE_TIMEOUT)
            );
        }

        return $this->createV2($location, $dateTime, $hop, $device, $residents, $near, $type, $ackDateTime);
    }

    public function resetAlertByLocation($dateTime)
    {
        /** @var Alert $alert */
        $alert = $this->findOneBy(array('dateTime' => $dateTime), array('id' => 'desc'));

        if (empty($alert)) {
            throw new \LogicException('Failed: There is no opening alert to close it');
        }

        if ($alert->getClosed()) {
            throw new \LogicException('Failed: There is no opening alert to close it');
        }

        if (!in_array($alert->getType(), Alert::getMaintenanceTypes()) && !$alert->getTaken()) {
            $alert->delayClose();
            $this->entityManager->persist($alert);
            $this->entityManager->flush();
//            throw new \LogicException("Failed: Can't reset the alert while nobody take it");
        }

        if (in_array($alert->getType(), Alert::getMaintenanceTypes()) || $alert->getTaken()) {
            $this->closeAlert($alert);
        }

        if ($alert->getClosed() && $alert->getDevice() && $alert->getDevice()->getWorkflow() === Device::WORKFLOW_NO_ACTION) {
            $this->completeAlert($alert);
        }

        return $alert;
    }

    public function resetAlertByButton(Button $button)
    {
        /** @var Alert $alert */
        $alert = $this->findOneBy(array('button' => $button), array('id' => 'desc'));

        if (empty($alert) || $alert->getClosed()) {
            throw new \LogicException('Failed: There is no opening alert to close it');
        }

        if (!$alert->getTaken()) {
            throw new \LogicException("Failed: Can't reset the alert while nobody take it");
        }

        $alert->setClosed(new \DateTime());
        $alert->setToClose($alert->getOpened()->diff($alert->getClosed())->format(Alert::TIME_INTERVAL_FORMAT));

        $this->entityManager->persist($alert);
        $this->entityManager->flush();

        return $alert;
    }

    /**
     * @param Button $button
     * @param Hop $hop
     * @param null $palatiumCareId
     * @param null $near
     * @return Alert
     */
    public function create(Button $button, Hop $hop = null, $palatiumCareId = null, $near = null)
    {
        $alert = new Alert();
        $alert->setButton($button);
        $alert->setLocation($button->getLocation());
        $alert->setOpened(new \DateTime());

        if (!is_null($hop)) {
            $alert->setHop($hop);
        }

        if (!empty($palatiumCareId)) {
            $alert->setPalatiumCareId($palatiumCareId);
        }

        if (!empty($near)) {
            $alert->setNear($near);
        }

        $this->entityManager->persist($alert);
        $this->entityManager->flush();

        return $alert;
    }

    /**
     * @param Location $location
     * @param $dateTime
     * @param Hop $hop
     * @param Device $device
     * @param null $residents
     * @param null $near
     * @param null $type
     * @return Alert
     */
    public function createV2(Location $location, $dateTime, Hop $hop = null, Device $device = null, $residents = null, $near = null, $type = null, $ackDateTime = null)
    {
        $alert = new Alert();
        $alert->setLocation($location);
        $alert->setDateTime($dateTime);
        $alert->setOpened(new \DateTime());

        if (!is_null($ackDateTime)) {
            $alert->setAskDateTime($ackDateTime);
        }

        if (!is_null($hop)) {
            $alert->setHop($hop);
        }

        if (!is_null($device)) {
            $alert->setDevice($device);
        }
        
        if (!is_null($residents) && is_array($residents)) {
            foreach ($residents as $resident) {
                $alert->addResident($resident);
            }
        }

        if (!empty($near)) {
            $alert->setNear($near);
        }

        if (!empty($type)) {
            $alert->setType($type);
        }

        $this->entityManager->persist($alert);
        $this->entityManager->flush();

        return $alert;
    }

    /**
     * @param Alert $alert
     * @param User $user
     */
    public function takeAlertByUser(Alert $alert, User $user)
    {
        $alert->setEmployee($user);
        $alert->setTaken(new \DateTime());

        $end = $alert->getTaken();
        $start = $alert->getOpened();
        $alert->setToTake($start->diff($end)->format(Alert::TIME_INTERVAL_FORMAT));

        $this->entityManager->persist($alert);
        $this->entityManager->flush();
    }

    public function closeAlert(Alert $alert)
    {
        $alert->setClosed(new \DateTime());
        $alert->setToClose($alert->getOpened()->diff($alert->getClosed())->format(Alert::TIME_INTERVAL_FORMAT));
        $this->entityManager->persist($alert);
        $this->entityManager->flush();
    }

    /**
     * @param Alert $alert
     */
    public function completeAlert(Alert $alert)
    {
        $alert->setCompleted(new \DateTime());

        $end = $alert->getCompleted();
        $start = $alert->getOpened();
        $alert->setToComplete($start->diff($end)->format(Alert::TIME_INTERVAL_FORMAT));

        $this->entityManager->persist($alert);
        $this->entityManager->flush();
    }

    /**
     * @param array $zonesIds
     * @return array
     */
    public function findUncompletedByZones(array $zonesIds)
    {
        if (empty($zonesIds)) {
            return array();
        }

        $query = $this->entityManager
            ->createQuery(
                "SELECT a FROM WebApplication\Entity\Alert a
                    JOIN a.location l
                WHERE l.zone in (:zonesIds) AND a.completed is null"
            )->setParameter('zonesIds', $zonesIds);

        return $query->getResult();
    }

    /**
     * @param array $params
     * @return array
     */
    public function getAlertsStats(array $params)
    {
        list($from, $to) = $this->prepareDates($params);

        $sql = $this->entityManager->createQueryBuilder();

        $sql->select('a', 'd')
            ->from('\WebApplication\Entity\Alert', 'a')
            ->leftJoin('a.device', 'd')
            //->where($sql->expr()->isNotNull('a.taken'))
            ->where($sql->expr()->isNotNull('a.closed'))
            //->andWhere('a.completed IS NOT NULL')
            ->andWhere('(
              (a.completed IS NOT NULL AND d.workflow = :actionWorkflow) OR
              (d.workflow = :noActionWorkflow) OR
              (a.device is null)
            )')
            ->andWhere($sql->expr()->gte('a.opened', ':dateStart'))
            ->andWhere($sql->expr()->lte('a.opened', ':dateEnd'))
            ->orderBy('a.closed', 'DESC')
            ->setParameter('actionWorkflow', 'action')
            ->setParameter('noActionWorkflow', 'no action')
            //->setParameter('types', Alert::getMaintenanceTypes())
            ->setParameter('dateStart', $from)
            ->setParameter('dateEnd', $to);

        if (!empty($params['userId'])) {
            $sql
                ->andWhere($sql->expr()->eq('a.employee', ':Employee'))
                ->setParameter('Employee', $params['userId']);
        }

        if (!empty($params['locationId'])) {
            $sql
                ->andWhere($sql->expr()->eq('a.location', ':Location'))
                ->setParameter('Location', $params['locationId']);
        }

        if (isset($params['offset'])) {
            $sql->setFirstResult($params['offset']);
        }

        if (!empty($params['limit'])) {
            $sql->setMaxResults($params['limit']);
        }

        return $sql->getQuery()->getResult();
    }

    public function countAlertsStats(array $params)
    {
        list($from, $to) = $this->prepareDates($params);

        $sql = $this->entityManager->createQueryBuilder();

        $sql->select('count(a.id)')
            ->from('\WebApplication\Entity\Alert', 'a')
            ->leftJoin('a.device', 'd')
            ->where($sql->expr()->isNotNull('a.closed'))
            ->andWhere('(
              (a.completed IS NOT NULL AND d.workflow = :actionWorkflow) OR
              (d.workflow = :noActionWorkflow) OR
              (a.device is null)
            )')
            ->andWhere($sql->expr()->gte('a.opened', ':dateStart'))
            ->andWhere($sql->expr()->lte('a.opened', ':dateEnd'))
            ->setParameter('actionWorkflow', 'action')
            ->setParameter('noActionWorkflow', 'no action')
            ->setParameter('dateStart', $from)
            ->setParameter('dateEnd', $to);

        if (!empty($params['userId'])) {
            $sql
                ->andWhere($sql->expr()->eq('a.employee', ':Employee'))
                ->setParameter('Employee', $params['userId']);
        }


        if (!empty($params['locationId'])) {
            $sql
                ->andWhere($sql->expr()->eq('a.location', ':Location'))
                ->setParameter('Location', $params['locationId']);
        }

        return (int) $sql->getQuery()->getSingleScalarResult();
    }

    /**
     * @param array $params
     * @return array
     */
    public function getSummaryStats(array $params)
    {
        list($from, $to) = $this->prepareDates($params);

        $stmt = $this->entityManager->getConnection()->prepare(
                "SELECT a.user_id,
                        u.name as username,
                        COUNT(a.id) as count,
                        MIN(a.to_take) as minTake,
                        MAX(a.to_take) as maxTake
                FROM alerts a
                  JOIN users u ON a.user_id = u.id
                WHERE a.taken IS NOT NULL AND a.closed IS NOT NULL and a.completed IS NOT NULL
                  AND a.opened >= :from AND a.opened <= :to
                GROUP BY user_id"
        );

        $stmt->bindValue('from', $from, DoctrineType::DATETIME);
        $stmt->bindValue('to', $to,DoctrineType::DATETIME);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param Alert $alert
     */
    public function delete(Alert $alert)
    {
        $this->entityManager->remove($alert);
        $this->entityManager->flush();
    }

    public function deleteNotCompleted()
    {
        $this->entityManager->getConnection()->executeQuery(
            'DELETE FROM alerts WHERE completed IS NULL'
        );
    }

    public function getAlerts($criteria, $type)
    {
        $alerts = $this->findBy($criteria);

        $getSystemAlerts = function($alertsData) {
            $alerts = [];
            foreach ($alertsData as $alert) {
                if ($alert->getType() != 'Alert') {
                    $alerts[] = $alert;
                }
            }
            return $alerts;
        };

        $getAlerts = function($alertsData) {
            $alerts = [];
            foreach ($alertsData as $alert) {
                if ($alert->getType() == 'Alert') {
                    $alerts[] = $alert;
                }
            }
            return $alerts;
        };

        $alerts = ($type == 'system') ? $getSystemAlerts($alerts) : $getAlerts($alerts);

        return $alerts;
    }
}