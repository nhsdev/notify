<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class BaseModel
{
    /** @var ServiceManager */
    protected $serviceManager;
    /** @var EntityManager */
    protected $entityManager;
    /** @var EntityRepository */
    protected $repository;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        $this->entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $id
     * @return null|object
     */
    public function findById($id)
    {
        return $this->repository->find((int) $id);
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findBy($criteria, $orderBy);
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @return null|object
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @param array $params
     * @return array
     */
    protected function prepareDates(array $params)
    {
        $from = !empty($params['dateFrom']) ? new \DateTime($params['dateFrom']) : new \DateTime('2000-01-01');
        $to = !empty($params['dateTo']) ?  new \DateTime($params['dateTo']) : new \DateTime('2999-01-01');

        if (isset($params['hoursFrom']) && isset($params['minutesFrom'])) {
            $from->setTime($params['hoursFrom'], $params['minutesFrom']);
        }

        if (isset($params['hoursTo']) && isset($params['minutesTo'])) {
            $to->setTime($params['hoursTo'], $params['minutesTo']);
        }

        return array($from, $to);
    }

    /**
     * @param \DateTime $date1
     * @param \DateTime $date2
     * @return number
     */
    protected function calculateSecondsDiff(\DateTime $date1, \DateTime $date2)
    {
        return abs($date1->getTimestamp() - $date2->getTimestamp());
    }
} 