<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;

class AsteriskModel
{
    private $connection = null;

    public function __construct(ServiceManager $serviceManager)
    {
        if (empty($serviceManager->get('Config')['asterisk'])) {
            throw new \OutOfBoundsException('Asterisk connection config not found');
        }

        $config = $serviceManager->get('Config')['asterisk'];

        $this->connection = new \PDO(
            sprintf("mysql:host=%s;dbname=%s", $config['host'], $config['dbname']),
            $config['user'],
            $config['password']
        );
    }

    /**
     * @param $name
     * @return array|null
     */
    public function findByName($name)
    {
        $stmt = $this->connection->prepare("SELECT * FROM `sip_buddies` WHERE name = ?");
        $stmt->execute(array($name));

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param array $params
     */
    public function insert(array $params)
    {
        $stmt = $this->connection->prepare(
            "INSERT INTO sip_buddies (name, callerid, defaultuser, secret) VALUES (?, ?, ?, ?)"
        );

        $stmt->execute(array(
            $params['voipName'], $params['voipCallerId'], $params['voipCallerId'], $params['voipPassword']
        ));
    }

    /**
     * @param int $id
     * @param array $params
     */
    public function updateById($id, array $params)
    {
        $sql = sprintf(
            'UPDATE sip_buddies SET name = ?, callerid = ?, defaultuser = ?%s WHERE id = ?',
            !empty($params['voipPassword']) ? ', secret = ?' : ''
        );

        $values = array($params['voipName'], $params['voipCallerId'], $params['voipCallerId']);
        if (!empty($params['voipPassword'])) {
            array_push($values, $params['voipPassword']);
        }
        array_push($values, $id);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($values);
    }

    /**
     * @param $authToken
     * @param $voipName
     */
    public function updateAuthTokenByName($authToken, $voipName)
    {
        $this->connection
             ->prepare('UPDATE sip_buddies SET auth_token = ? WHERE name = ?')
             ->execute(array($authToken, $voipName));
    }

    /**
     * @param int $id
     */
    public function deleteById($id)
    {
        $this->connection->exec(
            sprintf('DELETE FROM sip_buddies WHERE id = %s', (int) $id)
        );
    }
} 