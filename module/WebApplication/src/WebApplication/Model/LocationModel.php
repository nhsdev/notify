<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Location;

class LocationModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Location');
    }

    /**
     * @param array $params
     * @return Location
     * @throws \InvalidArgumentException
     */
    public function create(array $params)
    {
        if (empty($params['name'])) {
            throw new \InvalidArgumentException('Invalid name argument');
        }

        $location = new Location();
        $location->setName($params['name']);

        if (!empty($params['behaviour'])) {
            $location->setBehavior($params['behaviour']);
        }

        $location->setEnabled(!empty($params['enabled']) && $params['enabled'] == 'on' ? 1 : 0);

        if (!empty($params['zoneId'])) {
            $zone = $this->serviceManager->get('Model\Zone')->findById($params['zoneId']);
            $location->setZone($zone);
        }

        $this->entityManager->persist($location);
        $this->entityManager->flush();

        return $location;
    }

    /**
     * @param Location $location
     * @param array $params
     * @param bool $fromPalatiumCare
     */
    public function update(Location $location, array $params, $fromPalatiumCare = false)
    {
        if (empty($location->getZone()) || $location->getZone()->getId() != $params['zoneId']) {
            $zone = $this->serviceManager->get('Model\Zone')->findById($params['zoneId']);
            if (!empty($zone)) {
                $location->setZone($zone);
            }
        }

        if (!$fromPalatiumCare) {
            if (!empty($params['name'])) {
                $location->setName($params['name']);
            }

            if (!empty($params['behaviour'])) {
                $location->setBehavior($params['behaviour']);
            }

            $location->setEnabled(!empty($params['enabled']) && $params['enabled'] == 'on' ? 1 : 0);
        }

        $this->entityManager->persist($location);
        $this->entityManager->flush();
    }

    /**
     * @param Location $location
     */
    public function delete(Location $location)
    {
        $this->entityManager->remove($location);
        $this->entityManager->flush();
    }
}