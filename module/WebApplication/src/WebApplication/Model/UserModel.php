<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\User;
use WebApplication\Entity\Handset;
use Zend\Crypt\Password\Bcrypt;

class UserModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\User');
    }

    /**
     * @return array
     */
    public function findLoggedMobileUsers()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('u')
            ->from('\WebApplication\Entity\User', 'u')
            ->where('u.authToken IS NOT NULL')
            ->andWhere($query->expr()->eq('u.role', ':Role'))
            ->setParameter('Role', User::ROLE_MOBILE_USER);

        return $query->getQuery()->getResult();
    }

    /**
     * @param array $params
     * @param $passwordCost
     * @throws \InvalidArgumentException
     */
    public function create(array $params, $passwordCost)
    {
        if (empty($params['email'])) {
            throw new \InvalidArgumentException('Invalid email argument');
        }

        if (empty($params['username'])) {
            throw new \InvalidArgumentException('Invalid username argument');
        }

        if (empty($params['password'])) {
            throw new \InvalidArgumentException('Invalid password argument');
        }

        $user = new User();
        $user->setEmail($params['email']);
        $user->setUsername($params['username']);

        if (!empty($params['voipName'])) {
            $user->setVoipName($params['voipName']);
        }

        if (!empty($params['voipPassword'])) {
            $user->setVoipPassword($params['voipPassword']);
        }

        if (!empty($params['voipCallerId'])) {
            $user->setVoipCallerId($params['voipCallerId']);
        }

        if (!empty($params['pin'])) {
            $user->setPin($params['pin']);
        }

        if (!empty($params['role'])) {
            $user->setRole($params['role']);
        }

        if (!empty($params['zonesIds'])) {
            foreach ($params['zonesIds'] as $zoneId) {
                $zone = $this->serviceManager->get('Model\Zone')->findById($zoneId);
                if (!empty($zone)) {
                    $user->addZone($zone);
                }
            }
        }

        $user->setEnabled(!empty($params['enabled']) && $params['enabled'] == 'on' ? 1 : 0);

        $crypt = new Bcrypt();
        $crypt->setCost($passwordCost);
        $user->setPassword($crypt->create($params['password']));

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @param array $params
     * @param $passwordCost
     * @throws \InvalidArgumentException
     */
    public function update(User $user, array $params, $passwordCost)
    {
        if (empty($params['role'])) {
            throw new \InvalidArgumentException('Role param is required');
        }

        if (!empty($params['email'])) {
            $user->setEmail($params['email']);
        }

        if (!empty($params['username'])) {
            $user->setUsername($params['username']);
        }

        if (!empty($params['pin'])) {
            $user->setPin($params['pin']);
        }

        $user->setRole($params['role']);

        if ($params['role'] == User::ROLE_MOBILE_USER) {
            if (!empty($params['voipName'])) {
                $user->setVoipName($params['voipName']);
            }

            if (!empty($params['voipPassword'])) {
                $user->setVoipPassword($params['voipPassword']);
            }

            if (!empty($params['voipCallerId'])) {
                $user->setVoipCallerId($params['voipCallerId']);
            }

            if (!empty($params['zonesIds'])) {
                $user->truncateZones();
                foreach ($params['zonesIds'] as $zoneId) {
                    $zone = $this->serviceManager->get('Model\Zone')->findById($zoneId);
                    if (!empty($zone)) {
                        $user->addZone($zone);
                    }
                }
            }
        } else {
            $user->setAuthToken(null);
            $user->setVoipName(null);
            $user->setVoipPassword(null);
            $user->setVoipCallerId(null);
            $user->truncateZones();
        }

        $enabled = isset($params['enabled']) && $params['enabled'] == 'on' ? 1 : 0;
        $user->setEnabled($enabled);

        if (!$enabled) {
            $user->setAuthToken(null);
            $user->setSession(null);
            $this->serviceManager->get('Model\LoginHistory')->trackLogout($user);
        }

        if (!empty($params['password'])) {
            $crypt = new Bcrypt();
            $crypt->setCost($passwordCost);
            $user->setPassword($crypt->create($params['password']));
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @param array $zonesIds
     */
    public function attachZones(User $user, array $zonesIds)
    {
        $user->truncateZones();
        foreach ($zonesIds as $zoneId) {
            $zone = $this->serviceManager->get('Model\Zone')->findById($zoneId);
            if (!empty($zone)) {
                $user->addZone($zone);
            }
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @param Handset $handset
     * @param $pushToken
     * @return bool
     */
    public function attachHandset(User $user, Handset $handset, $pushToken = null)
    {
        $oldUser = $handset->getUser();
        if (!empty($oldUser) && $user->getId() == $oldUser->getId()) {
            return false;
        }

        if (!empty($oldUser)) {
            $oldUser->setHandset(null)->setAuthToken(null)->setSession(null);

            $this->entityManager->persist($oldUser);
            $this->entityManager->flush();
        }

        /** @var Handset $oldHandset */
        $oldHandset = $user->getHandset();
        if (!empty($oldHandset) && $oldHandset != $handset) {
            $oldHandset->setUser(null);

            $this->entityManager->persist($oldHandset);
            $this->entityManager->flush();
        }

        $user->setHandset($handset);
        $handset->setUser($user);

        if (!is_null($pushToken)) {
            $handset->setPushToken($pushToken);
        }

        $this->entityManager->persist($handset);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param User $user
     * @param Handset $handset
     */
    public function detachHandset(User $user, Handset $handset)
    {
        $user->setHandset(null);
        $handset->setUser(null);

        $this->entityManager->persist($handset);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     */
    public function addSession(User $user)
    {
        $authToken = $this->createAuthToken();
        $user->setAuthToken($authToken);
        $user->setSession(md5($authToken));

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     */
    public function deleteSession(User $user)
    {
        $user->setSession(null);
        $user->setAuthToken(null);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    private function createAuthToken()
    {
        return sha1(uniqid());
    }
}