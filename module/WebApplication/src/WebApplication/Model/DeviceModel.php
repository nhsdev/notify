<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Device;

class DeviceModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Device');
    }

    /**
     * @param array $params
     * @return Device
     * @throws \InvalidArgumentException
     */
    public function create(array $params)
    {
        if (empty($params['type'])) {
            throw new \InvalidArgumentException('Invalid type argument');
        }

        $device = new Device();
        $device->setType($params['type']);

        if (!empty($params['workflow'])) {
            $device->setWorkflow($params['workflow']);
        }

        $this->entityManager->persist($device);
        $this->entityManager->flush();

        return $device;
    }

    /**
     * @param Device $device
     * @param array $params
     */
    public function update(Device $device, array $params)
    {
        if (!empty($params['type'])) {
            $device->setType($params['type']);
        }

        if (!empty($params['workflow'])) {
            $device->setWorkflow($params['workflow']);
        }

        $this->entityManager->persist($device);
        $this->entityManager->flush();
    }

    /**
     * @param Device $device
     */
    public function delete(Device $device)
    {
        $this->entityManager->remove($device);
        $this->entityManager->flush();
    }

}