<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Resident;

class ResidentModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Resident');
    }

    /**
     * @param array $params
     * @return Resident
     * @throws \InvalidArgumentException
     */
    public function create(array $params)
    {
        if (empty($params['firstName'])) {
            throw new \InvalidArgumentException('Invalid name argument');
        }

        $resident = new Resident();
        $resident->setFirstName($params['firstName']);

        if (!empty($params['lastName'])) {
            $resident->setLastName($params['lastName']);
        }

        if (!empty($params['nickname'])) {
            $resident->setNickname($params['nickname']);
        }

        $this->entityManager->persist($resident);
        $this->entityManager->flush();

        return $resident;
    }

    /**
     * @param Resident $resident
     * @param array $params
     */
    public function update(Resident $resident, array $params)
    {
        if (!empty($params['firstName'])) {
            $resident->setFirstName($params['firstName']);
        }

        if (!empty($params['lastName'])) {
            $resident->setLastName($params['lastName']);
        }

        if (!empty($params['nickname'])) {
            $resident->setNickname($params['nickname']);
        }

        $this->entityManager->persist($resident);
        $this->entityManager->flush();
    }

    /**
     * @param Resident $resident
     */
    public function delete(Resident $resident)
    {
        $this->entityManager->remove($resident);
        $this->entityManager->flush();
    }
}