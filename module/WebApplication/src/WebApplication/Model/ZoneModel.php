<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Zone;

class ZoneModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Zone');
    }

    /**
     * @param array $params
     * @throws \InvalidArgumentException
     */
    public function create(array $params)
    {
        if (empty($params['name'])) {
            throw new \InvalidArgumentException('Invalid name argument');
        }

        $zone = new Zone();
        $zone->setName($params['name']);

        if (!empty($params['sound'])) {
            $zone->setSound($params['sound']);
        }

        if (!empty($params['soundCount'])) {
            $zone->setSoundCount($params['soundCount']);
        }

        if (!empty($params['soundInterval'])) {
            $zone->setSoundInterval($params['soundInterval']);
        }

        $this->entityManager->persist($zone);
        $this->entityManager->flush();
    }

    /**
     * @param Zone $zone
     * @param array $params
     */
    public function update(Zone $zone, array $params)
    {
        if (!empty($params['name'])) {
            $zone->setName($params['name']);
        }

        if (!empty($params['sound'])) {
            $zone->setSound($params['sound']);
        }

        if (!empty($params['soundCount'])) {
            $zone->setSoundCount($params['soundCount']);
        }

        if (!empty($params['soundInterval'])) {
            $zone->setSoundInterval($params['soundInterval']);
        }

        $this->entityManager->persist($zone);
        $this->entityManager->flush();
    }

    /**
     * @param Zone $zone
     */
    public function delete(Zone $zone)
    {
        $this->entityManager->remove($zone);
        $this->entityManager->flush();
    }
}