<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Button;
use WebApplication\Entity\Location;

class ButtonModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Button');
    }

    /**
     * @param array $params
     * @return Button
     * @throws \InvalidArgumentException
     */
    public function create(array $params)
    {
        if (empty($params['publicId'])) {
            throw new \InvalidArgumentException('Button publicId must not be empty');
        }

        $button = new Button();
        $button->setPublicId($params['publicId']);

        if (!empty($params['locationId'])) {
            $button->setLocation($this->serviceManager->get('Model\Location')->findById($params['locationId']));
        }

        $this->entityManager->persist($button);
        $this->entityManager->flush();

        return $button;
    }

    /**
     * @param Button $button
     * @param array $params
     */
    public function update(Button $button, array $params)
    {
        if (!empty($params['publicId'])) {
            $button->setPublicId($params['publicId']);
        }

        if (isset($params['locationId'])) {
            $location = $this->serviceManager->get('Model\Location')->findById($params['locationId']);
            if (!empty($location)) {
                $button->setLocation($location);
            }
        }

        $this->entityManager->persist($button);
        $this->entityManager->flush();
    }

    /**
     * @param Button $button
     */
    public function delete(Button $button)
    {
        $this->entityManager->remove($button);
        $this->entityManager->flush();
    }
}