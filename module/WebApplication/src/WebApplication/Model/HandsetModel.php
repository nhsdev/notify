<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\Handset;

class HandsetModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\Handset');
    }

    /**
     * @param array $params
     * @throws \InvalidArgumentException
     */
    public function create(array $params)
    {
        if (empty($params['name'])) {
            throw new \InvalidArgumentException('Invalid name argument');
        }

        if (empty($params['publicId'])) {
            throw new \InvalidArgumentException('Invalid publicId argument');
        }

        $handset = new Handset();
        $handset->setName($params['name']);
        $handset->setPublicId($params['publicId']);

        if (isset($params['displayName'])) {
            $handset->setDisplayName($params['displayName']);
        }

        $this->entityManager->persist($handset);
        $this->entityManager->flush();
    }

    public function update(Handset $handset, array $params)
    {
        if (isset($params['name'])) {
            $handset->setName($params['name']);
        }

        if (isset($params['displayName'])) {
            $handset->setDisplayName($params['displayName']);
        }

        if (isset($params['publicId'])) {
            $handset->setPublicId($params['publicId']);
        }

        if (isset($params['pushToken'])) {
            $handset->setPushToken($params['pushToken']);
        }

        $this->entityManager->persist($handset);
        $this->entityManager->flush();
    }

    /**
     * @param Handset $handset
     */
    public function delete(Handset $handset)
    {
        $this->entityManager->remove($handset);
        $this->entityManager->flush();
    }

    /**
     * @param int $locationId
     * @return array
     */
    public function getPushTokensByLocationId($locationId)
    {
        if (empty($locationId)) {
            return array();
        }

        $stmt = $this->entityManager->getConnection()->prepare(
            "SELECT h.push_token as pushToken FROM handsets h
              JOIN users u ON h.user_id = u.id
              JOIN users_zones uz ON uz.user_id = u.id
              JOIN zones z ON uz.zone_id = z.id
              JOIN locations l ON l.zone_id = z.id
            WHERE h.push_token IS NOT NULL
              AND u.auth_token IS NOT NULL
              AND l.id = ?"
        );

        $stmt->execute(array($locationId));

        $result = $stmt->fetchAll();

        return array_map(
            function($element) {
                return $element['pushToken'];
            }, $result
        );
    }
}