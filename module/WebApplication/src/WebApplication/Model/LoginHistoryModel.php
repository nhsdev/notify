<?php

namespace WebApplication\Model;

use Zend\ServiceManager\ServiceManager;
use WebApplication\Entity\LoginHistory;
use WebApplication\Entity\User;

class LoginHistoryModel extends BaseModel
{
    /** @Override */
    public function __construct(ServiceManager $serviceManager)
    {
        parent::__construct($serviceManager);
        $this->repository = $this->entityManager->getRepository('WebApplication\Entity\LoginHistory');
    }

    /**
     * @param array $params
     * @return array
     */
    public function getStatsData(array $params)
    {
        list($from, $to) = $this->prepareDates($params);

        $sql = $this->entityManager->createQueryBuilder();

        $sql->select('l')
            ->from('\WebApplication\Entity\LoginHistory', 'l')
            ->where($sql->expr()->gte('l.login', ':dateStart'))
            ->andWhere($sql->expr()->lte('l.login', ':dateEnd'))
            ->orderBy('l.login', 'DESC')
            ->setParameter('dateStart', $from)
            ->setParameter('dateEnd', $to);

        if (!empty($params['user'])) {
            $sql->andWhere($sql->expr()->eq('l.employee', ':Employee'))
                ->setParameter('Employee', $params['user']->getUsername());
        }

        if (!empty($params['handset'])) {
            $sql->andWhere($sql->expr()->eq('l.handset', ':Handset'))
                ->setParameter('Handset', $params['handset']->getName());
        }

        if (isset($params['offset'])) {
            $sql->setFirstResult($params['offset']);
        }

        if (!empty($params['limit'])) {
            $sql->setMaxResults($params['limit']);
        }

        return $sql->getQuery()->getResult();
    }

    public function countStatsData(array $params)
    {
        list($from, $to) = $this->prepareDates($params);

        $sql = $this->entityManager->createQueryBuilder();

        $sql->select('count(l.id)')
            ->from('\WebApplication\Entity\LoginHistory', 'l')
            ->where($sql->expr()->gte('l.login', ':dateStart'))
            ->andWhere($sql->expr()->lte('l.login', ':dateEnd'))
            ->setParameter('dateStart', $from)
            ->setParameter('dateEnd', $to);

        if (!empty($params['user'])) {
            $sql->andWhere($sql->expr()->eq('l.employee', ':Employee'))
                ->setParameter('Employee', $params['user']->getUsername());
        }

        if (!empty($params['handset'])) {
            $sql->andWhere($sql->expr()->eq('l.handset', ':Handset'))
                ->setParameter('Handset', $params['handset']->getName());
        }

        if (isset($params['offset'])) {
            $sql->setFirstResult($params['offset']);
        }

        if (!empty($params['limit'])) {
            $sql->setMaxResults($params['limit']);
        }

        return (int) $sql->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User $user
     */
    public function trackLogin(User $user)
    {
        $this->makeLogoutOldRecords($user);

        $login = new LoginHistory();
        $login->setEmployee($user->getUsername());
        $login->setHandset($user->getHandset() ? $user->getHandset()->getName() : null);
        $login->setLogin(new \DateTime());
        $login->setLogout(null);

        $this->entityManager->persist($login);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     */
    public function trackLogout(User $user)
    {
        /** @var LoginHistory $login */
        $login = $this->findOneBy(
            array('employee' => $user->getUsername(), 'logout' => null),
            array('id' => 'DESC')
        );

        if ($login) {
            $login->setLogout(new \DateTime());
            $this->entityManager->persist($login);
            $this->entityManager->flush();
        }
    }

    /**
     * @param User $user
     */
    private function makeLogoutOldRecords(User $user)
    {
        $this->entityManager->getConnection()->executeUpdate(
            sprintf(
                "UPDATE login_history SET logout = '%s' WHERE logout IS NULL AND employee = '%s'",
                (new \DateTime())->format('Y/m/d H:i:s'),
                $user->getUsername()
            )
        );
    }
}