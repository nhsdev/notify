<?php

namespace ServerApi;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use ServerApi\Controller\Utils\PushLogger;
use ServerApi\Controller\Utils\ApiLogger;
use Zend\Log\Writer\Stream as LogStream;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                /* push notifications logger */
                'PushLogger' => function ($sm) {
                    if (!is_dir('./data/logs') ) {
                        umask(0);
                        mkdir('./data/logs', 0777, true);
                    }

                    if (!is_dir('./data/logs/pushes')) {
                        umask(0);
                        mkdir('./data/logs/pushes', 0777, true);
                    }

                    $logger = new PushLogger();
                    $logger->addWriter(
                        new LogStream(sprintf('./data/logs/pushes/%s.log', date('Y-m-d')))
                    );

                    return $logger;
                },
                /* ServerApi logger */
                'ApiLogger' => function ($sm) {
                    if (!is_dir('./data/logs')) {
                        umask(0);
                        mkdir('./data/logs', 0777, true);
                    }

                    if (!is_dir('./data/logs/api')) {
                        umask(0);
                        mkdir('./data/logs/api', 0777, true);
                    }

                    $logger = new Apilogger();
                    $logger->addWriter(
                        new LogStream(sprintf('./data/logs/api/%s.log', date('Y-m-d')))
                    );

                    return $logger;
                },
            )
        );
    }
}
