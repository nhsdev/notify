<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'ServerApi\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'server-api' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'ServerApi\Controller',
                        'controller'    => 'Application',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            'receiver' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/receiver[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'ServerApi\Controller\Receiver',
                        'action'     => 'index',
                    ),
                ),
            ),
            'alerts' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/palatiumCare[/:facilityCode]',
                    'constraints' => array(
                        'facilityCode' => '[a-zA-Z0-9]*'
                    ),
                    'defaults' => array(
                        'controller' => 'ServerApi\Controller\Listen',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'ServerApi\Controller\Application' => 'ServerApi\Controller\ApplicationController',
            'ServerApi\Controller\Index' => 'ServerApi\Controller\IndexController',
            'ServerApi\Controller\Receiver' => 'ServerApi\Controller\ReceiverController',
            'ServerApi\Controller\Listen' => 'ServerApi\Controller\ListenController',
        ),
    ),

    'controller_plugins' => array(
        'invokables' => array(
            'PushNotificationPlugin' => 'ServerApi\Controller\Plugin\PushNotificationPlugin',
        )
    ),

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'externalApi' => array(
        'apple' => array(
            'key' => sprintf('%s%s', __DIR__, APP_ENV == 'prod' ? '/notifyProd.pem' : '/notifyDev.pem'),
            'password' => '',
        )
    ),
    'notify' => array(
        'url' => 'http://127.0.0.1/receiver',
        'responseTimeout' => 3,
    ),
);
