<?php
namespace ServerApi\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use ZendService\Apple\Apns\Client\Message as AppleApnsClient;
use ZendService\Apple\Apns\Response\Message as PushResponse;
use ZendService\Apple\Exception\RuntimeException;
use ServerApi\Controller\Utils\PushLogger;

class PushNotificationPlugin extends AbstractPlugin
{
    const PUSH_TOKEN_LENGTH = 64;

    /**
     * @param array $data
     * @param array $tokens
     * @param string $apsAlert
     * @param int $badge
     * @param string|null $sound
     * @param bool $isSilentPush
     * @param bool $addApsAlertSuffix
     */
    public function pushNotification(
        array $data,
        array $tokens,
        $apsAlert = '',
        $badge = 1,
        $sound = null,
        $isSilentPush = false,
        $addApsAlertSuffix = false
    ) {
        $config = $this->getController()->getServiceLocator()->get('Config');

        foreach ($tokens as $token) {
            $client = new AppleApnsClient();
            $client->open(
                APP_ENV == 'prod' ? AppleApnsClient::PRODUCTION_URI : AppleApnsClient::SANDBOX_URI,
                $config['externalApi']['apple']['key'],
                $config['externalApi']['apple']['password']
            );

            if (strlen($token) != self::PUSH_TOKEN_LENGTH) {
                continue;
            }

            $message = new CustomApnsMessage();
            $message->setId('notify');
            $message->setToken($token);
            $message->setBadge($badge);
            $message->setAlert($apsAlert . ($addApsAlertSuffix ? ' Push' : ''));
            $message->setCustom($data);

            $isSilentPush = isset($data['alert']) && !empty($data['alert']['repeatCount']) && !empty($data['alert']['repeatInterval']);

            $message->setSilentPushFlag($isSilentPush);

            if (!is_null($sound)) {
                $message->setSound($sound . '.mp3');
            }

            /** @var PushLogger $logger */
            $logger = $this->getController()->getServiceLocator()->get('PushLogger');
            $logger->debug("$apsAlert Push. On '$token' token SENDING ...");

            try {
                /** @var PushResponse $response */
                $response = $client->send($message);
                $logger->info($this->getResponseMessageByCode($response->getCode()));
            } catch (RuntimeException $e) {
                echo $e->getMessage() . PHP_EOL;
                $logger->err("ERROR: {$e->getMessage()}");
                exit(1);
            }

            $client->close();
        }
    }

    private function getResponseMessageByCode($code)
    {
        switch($code) {
            case 0:
                return 'OK';
            case 1:
                return 'PROCESSING ERROR';
            case 2:
                return 'MISSING TOKEN';
            case 3:
                return 'MISSING TOPIC';
            case 4:
                return 'MISSING PAYLOAD';
            case 5:
                return 'INVALID TOKEN SIZE';
            case 6:
                return 'INVALID TOPIC SIZE';
            case 7:
                return 'INVALID PAYLOAD SIZE';
            case 8:
                return 'INVALID TOKEN';
            case 255:
                return 'UNKNOWN ERROR';
            default:
                return 'UNKNOWN CODE STATUS';
        }
    }
}