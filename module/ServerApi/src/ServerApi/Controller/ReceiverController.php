<?php

namespace ServerApi\Controller;

use WebApplication\Entity\Alert;
use WebApplication\Entity\Hop;
use WebApplication\Entity\Location;
use WebApplication\Entity\Device;

class ReceiverController extends AbstractApiController
{
    public function indexAction()
    {
        $data = $this->processPostData($this->getRequest());

        $locationName = isset($data['location']) ? $data['location'] : null;
        $hopId = isset($data['hopID']) ? $data['hopID'] : null;
        $near = isset($data['near']) ? $data['near'] : null;
        $deviceType = isset($data['deviceType']) ? $data['deviceType'] : null;
        $type = isset($data['type']) ? $data['type'] : null;
        $residentsInitials = isset($data['residents']) ? $data['residents'] : null;
        $dateTime = isset($data['dateTime']) ? $data['dateTime'] : null;

        $ackDateTime = isset($data['ackDateTime']) ? $data['ackDateTime'] : null;

        $resetPress = isset($data['resetPress']) ? $data['resetPress'] : null;
        $buttonRelease = isset($data['buttonRelease']) ? $data['buttonRelease'] : null;
        $isReset = $resetPress || $buttonRelease;

        if (empty($locationName) && !$isReset) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'location is required');
        }

        if (empty($dateTime)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'event date time is required');
        }

        /** @var Location $location*/
        if (!empty($locationName)) {
            $location = $this->getLocationModel()->findOneBy(array('name' => $locationName));
            if (empty($location)) {
                $location = $this->getLocationModel()->create(array('name' => $locationName, 'behaviour' => 'mobile', 'enabled' => 'on'));
            }
        }

        /** @var Hop $hop */
        $hop = $this->getHop($hopId);

        /** @var Device $device */
        $device = $this->getDevice($deviceType);

        /** @var array $residents */
        $residents = $this->getResidents($residentsInitials);

        try {
            /** @var Alert $alert */
            $alert = $isReset
                ? $this->getAlertModel()->resetAlertByLocation($dateTime)
                : $this->getAlertModel()->createAlertByLocation($location, $dateTime, $hop, $device, $residents, $near, $type, $ackDateTime);
        } catch (\LogicException $e) {
            return $this->sendResponse(self::HTTP_NOT_ACCEPTABLE, null, $e->getMessage());
        }

        $sound = null;
        if ($this->getAlertModel()->getAlertState($alert) == Alert::STATUS_UNTAKEN && !$isReset) {
            $sound = $this->getSoundByLocation($location);
        }

        if ($alert->getType() === 'Alert' && !$alert->getDelayClose()) {
            $this->sendAlertPushNotification($alert, $sound);
        }

        return $this->sendSuccessResponse();
    }

    private function getHop($hopId)
    {
        if (empty($hopId)) {
            return null;
        }

        $hop = $this->getHopModel()->findOneBy(array('publicId' => $hopId));
        if (empty($hop)) {
            $hop = $this->getHopModel()->create(array('publicId' => $hopId));
        }

        return $hop;
    }

    private function getDevice($type)
    {
        if (empty($type)) {
            return null;
        }

        $device = $this->getDeviceModel()->findOneBy(array('type' => $type));
        if (empty($device)) {
            $device = $this->getDeviceModel()->create(array('type' => $type, 'workflow' => 'no action'));
        }

        return $device;
    }

    private function getResidents($data)
    {
        if (empty($data)) {
            return null;
        }

        $residents = [];

        foreach ($data as $row) {
            $resident = $this->getResidentModel()->findOneBy(array('firstName' => $row['firstName'], 'lastName' => $row['lastName']));
            if (empty($resident)) {
                $resident = $this->getResidentModel()->create(array('firstName' => $row['firstName'], 'lastName' => $row['lastName']));
            }
            $residents[] = $resident;
        }

        return $residents;
    }
}