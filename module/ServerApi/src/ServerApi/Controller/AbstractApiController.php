<?php

namespace ServerApi\Controller;

use WebApplication\Controller\ModelShortcut;
use Zend\Mvc\Controller\AbstractActionController;
use WebApplication\Entity\Button;
use WebApplication\Entity\Location;
use WebApplication\Entity\Device;
use Zend\Stdlib\RequestInterface;
use Zend\Json\Json;
use ServerApi\Controller\Plugin\PushNotificationPlugin;
use WebApplication\Entity\Alert;
use ServerApi\Controller\Utils\ApiLogger;

/**
 * @method PushNotificationPlugin PushNotificationPlugin()
 */
abstract class AbstractApiController extends AbstractActionController
{
    const HTTP_STATUS_OK = 200;
    const HTTP_STATUS_UNAUTHORIZED = 401;
    const HTTP_STATUS_FORBIDDEN = 403;
    const HTTP_STATUS_NOT_FOUND = 404;
    const HTTP_NOT_ACCEPTABLE = 406;

    const FACILITY_CODE = 'deb769';

    use ModelShortcut;

    protected function getSoundByButton(Button $button)
    {
        return $button->getLocation() && $button->getLocation()->getZone() && $button->getLocation()->getZone()->getSound()
            ? $button->getLocation()->getZone()->getSound()
            : null;
    }

    protected function getSoundByLocation(Location $location)
    {
        return $location->getZone() && $location->getZone()->getSound()
            ? $location->getZone()->getSound()
            : null;
    }

    protected function processPostData(RequestInterface $request)
    {
        return Json::decode($request->getContent(), Json::TYPE_ARRAY);
    }

    public function sendAlertPushNotification(Alert $alert, $sound = null)
    {
        if (is_null($alert->getLocation())) {
            return null;
        }

        if (in_array($alert->getType(), Alert::getMaintenanceTypes())) {
            return null;
        }

        $pushTokens = $this->getHandsetModel()->getPushTokensByLocationId(
            $alert->getLocation()->getId()
        );

        if ($pushTokens) {
            $alert = $this->getAlertInfo($alert);
            unset($alert['near']);
            $this->PushNotificationPlugin()->pushNotification(
                array('alert' => $alert),
                $pushTokens,
                'New Alert: ' . $alert['locationName'],
                1,
                $sound
            );
        }
    }

    protected function getAlertInfo(Alert $alert)
    {
        $near = $alert->getHop() ? $alert->getHop()->getName() : $alert->getNear();

        return array(
            'id' => $alert->getId(),
            'timeOpened' => date_format($alert->getOpened(), 'd/m/Y H:i:s'),
            'employeeName' => $alert->getEmployee() ? $alert->getEmployee()->getUsername() : null,
            'employeeId' => $alert->getEmployee() ? $alert->getEmployee()->getId() : null,
            'locationName' => $alert->getLocation() ? $alert->getLocation()->getName() : null,
            'state' => $this->getAlertModel()->getAlertState($alert),
            'repeatCount' => $alert->getLocation() && $alert->getLocation()->getZone() ? $alert->getLocation()->getZone()->getSoundCount() : null,
            'repeatInterval' => $alert->getLocation() && $alert->getLocation()->getZone() ? $alert->getLocation()->getZone()->getSoundInterval() : null,
            'near' => $near,
            'type' => $alert->getDevice() ? $alert->getDevice()->getType() : null,
            'workflow' => $alert->getDevice() ? $alert->getDevice()->getWorkflow() : null,
            'resident' => (count($alert->getResidents()->toArray()) == 1) ? $alert->getResidents()->toArray()[0]->getName() : null
        );
    }

    /** @return ApiLogger */
    protected function getApiLogger()
    {
        return $this->getServiceLocator()->get('ApiLogger');
    }

    protected function sendResponse($code, $data, $error = null)
    {
        return $this->getResponse()->setContent(
            Json::encode(array('code' => $code, 'result' => $data, 'error' => $error))
        );
    }

    protected function sendInvalidTokenResponse()
    {
        return $this->sendResponse(self::HTTP_STATUS_FORBIDDEN, null , 'Invalid token');
    }

    protected function sendSuccessResponse()
    {
        return $this->sendResponse(self::HTTP_STATUS_OK, array('success' => 1), null);
    }
}