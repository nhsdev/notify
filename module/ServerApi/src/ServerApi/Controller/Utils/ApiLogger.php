<?php

namespace ServerApi\Controller\Utils;

use Zend\Log\Logger as ZendLogger;

class ApiLogger extends ZendLogger
{
    /** @Override */
    public function log($priority, $message, $extra = array())
    {
        if (!API_LOGS) {
            return;
        }

        parent::log($priority, $message, $extra);
    }
} 