<?php

namespace ServerApi\Controller\Utils;

use Zend\Log\Logger as ZendLogger;

class PushLogger extends ZendLogger
{
    /** @Override */
    public function log($priority, $message, $extra = array())
    {
        if (!PUSH_LOGS) {
            return;
        }

        parent::log($priority, $message, $extra);
    }
} 