<?php

namespace ServerApi\Controller;

use Zend\Http\Response as HttpResponse;

class ListenController extends AbstractApiController
{
    private $lastCurlCode = 0;
    private $lastHttpCode = 200;

    protected function logResult() {

        $format = function($str) {
            $str = preg_replace("/(\r\n){2,}/iU", "\r\n", $str);
            $str = preg_replace("/\r\n/i", "; ", $str);
            return $str;
        };

        $request = $format($this->getRequest()->toString());
        $response = $format($this->getResponse()->toString());

        $logger   = $this->getApiLogger();
        $logger->debug(sprintf("REQUEST:  %s", $request));
        $logger->debug(sprintf("RESPONSE: %s\r\n", $response));
    }

    public function indexAction()
    {
        $facilityCode = $this->getEvent()->getRouteMatch()->getParam('facilityCode');
        $dateTime = $this->params()->fromQuery('eventDateTime');

        if ($facilityCode !== self::FACILITY_CODE) {
            $response = $this->sendResponse(self::HTTP_NOT_ACCEPTABLE, null, 'Incorrect facility code');
            $this->logResult();
            return $response;
        }

        if (empty($dateTime)) {
            $response = $this->sendResponse(self::HTTP_NOT_ACCEPTABLE, null, 'Event date time is required');
            $this->logResult();
            return $response;
        }

        $this->processAlerts();

        if (0 === $this->getLastCurlCode()) {
            if (200 === $this->getLastHttpCode()) {
                $response = $this->sendSuccessResponse();
            } else {
                $httpResponse = new HttpResponse();
                $httpResponse->setStatusCode($this->getLastHttpCode());
                $response = $this->sendResponse($this->getLastHttpCode(), null, $httpResponse->getReasonPhrase());
            }
        } else {
            $error_message = curl_strerror($this->getLastCurlCode());
            $error_message = sprintf("cURL error (%s): %s", $this->getLastCurlCode(), $error_message);
            $response = $this->sendResponse(HttpResponse::STATUS_CODE_400, null, $error_message);
        }

        $this->logResult();

        return $response;
    }

    private function processAlerts()
    {
        $request = $this->getEvent()->getRequest();
        $method = strtolower($request->getMethod());
        switch ($method) {
            case 'post':
                $alert = $this->getPostAlert();
                $this->createNewAlert($alert);
                break;
            case 'patch':
                $alert = $this->getPatchAlert();
                $this->closeAlert($alert);
                break;
        }
    }

    private function getPostAlert()
    {
        $result = json_decode($this->getRequest()->getContent(), true);
        $alert  = [];

        if (isset($result['resident'])) {
            $alert['residents'][] = $result['resident'];
        } else if (isset($result['residents'])) {
            $alert['residents'] = $result['residents'];
        } else $alert['residents'] = null;

        $alert['location']   = isset($result['location']['name']) ? $result['location']['name'] : null;
        $alert['deviceType'] = isset($result['device']['type']) ? $result['device']['type'] : null;
        $alert['type']       = isset($result['type']) ? $result['type'] : null;
        $alert['near']       = isset($result['near']) && is_array($result['near']) ? $result['near'][0]['name'] : null;
        //      $alert['near'] = isset($result['device']['area']) ? $result['device']['area'] : null;

        $alert['ackDateTime'] = isset($result['ackDateTime']) ? $result['ackDateTime'] : null;

        return $alert;
    }

    private function getPatchAlert()
    {
        $alert = [];
        $alert['dateTime'] = $this->params()->fromQuery('eventDateTime');
        $alert['resetPress'] = true;

        return $alert;
    }

    private function createNewAlert(array $alert)
    {
        if (empty($alert)) {
            return;
        }

        $dateTime = $this->params()->fromQuery('eventDateTime');
        $requestData = array(
            'location' => $alert['location'],
            'near' => $alert['near'],
            'type' => $alert['type'],
            'deviceType' => $alert['deviceType'],
            'residents' => $alert['residents'],
            'dateTime' => $dateTime,
        );
        if(null !== $alert['ackDateTime']) {
            $requestData['ackDateTime'] = $alert['ackDateTime'];
        }

        $this->sendHttpRequest(
            $this->getServiceLocator()->get('Config')['notify']['url'],
            $this->getServiceLocator()->get('Config')['notify']['responseTimeout'],
            'post',
            json_encode(
                $requestData
            ),
            array('Content-Type:application/json')
        );
    }

    private function closeAlert($alert)
    {
        if (empty($alert)) {
            return;
        }

        $this->sendHttpRequest(
            $this->getServiceLocator()->get('Config')['notify']['url'],
            $this->getServiceLocator()->get('Config')['notify']['responseTimeout'],
            'post',
            json_encode(array('dateTime' => $alert['dateTime'], 'resetPress' => true)),
            array('Content-Type:application/json')
        );

    }

    private function sendHttpRequest($url, $timeout, $type = 'get', $params = null, array $headers = null)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout*2);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout*2);

        if ($type == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);

        $this->initErrors($ch);

        curl_close($ch);

        return $response;
    }


    protected function initErrors($ch) {
        $this->lastCurlCode = 0;
        $this->lastHttpCode = 200;
        $this->setLastCurlCode($ch);
        $this->setLastHttpCode($ch);
    }

    protected function setLastCurlCode($ch) {
        $this->lastCurlCode = curl_errno($ch);
    }

    protected function setLastHttpCode($ch) {
        $this->lastHttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    }

    protected function getLastCurlCode() {
        return $this->lastCurlCode;
    }

    protected function getLastHttpCode() {
        return $this->lastHttpCode;
    }

}