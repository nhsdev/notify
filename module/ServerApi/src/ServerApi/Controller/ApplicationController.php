<?php

namespace ServerApi\Controller;

use Zend\Crypt\Password\Bcrypt;
use WebApplication\Entity\User;
use WebApplication\Entity\Zone;
use WebApplication\Entity\Alert;
use WebApplication\Entity\Handset;
use WebApplication\Entity\Device;
use ServerApi\Controller\Plugin\PushNotificationPlugin;

/**
 * @method PushNotificationPlugin PushNotificationPlugin()
 */
class ApplicationController extends AbstractApiController
{
    private $asteriskMessages = array(
        'sendAsteriskCallPush' => 'You\'ve got an incoming call from %s',
        'sendAsteriskMsgPush' => 'You\'ve got a message from %s',
        'sendAsteriskMissedCallPush' => 'You\'ve missed a call from %s'
    );

    public function indexAction()
    {

    }

    public function loginAction()
    {
        $data = $this->processPostData($this->getRequest());
        $username = isset($data['username']) ? $data['username'] : null;
        $password = isset($data['password']) ? $data['password'] : null;
        $pushToken = isset($data['pushToken']) ? $data['pushToken'] : null;
        $handsetID = isset($data['handsetID']) ? $data['handsetID'] : null;

        if (empty($username) || empty($password)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'Username and password are required');
        }

        if (empty($handsetID)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'HandsetID is required');
        }

        /** @var User $user */
        $user = $this->getUserModel()->findOneBy(array('username' => $username));
        if (empty($user)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, sprintf('User %s not found', $username));
        }

        if (!(new Bcrypt())->verify($password, $user->getPassword())) {
            return $this->sendResponse(self::HTTP_STATUS_UNAUTHORIZED, null, 'Bad credentials');
        }

        if (!$user->getEnabled() || $user->getRole() != User::ROLE_MOBILE_USER) {
            return $this->sendResponse(self::HTTP_STATUS_FORBIDDEN, null, 'User has no rights to logged in mobile application');
        }

        /** @var Handset $handset */
        $handset = $this->getHandsetModel()->findOneBy(array('publicId' => $handsetID));
        if (empty($handset)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, sprintf('Handset %s not found', $handsetID));
        }

        if (!is_null($user->getAuthToken()) && $handset->getUser() != $user) {
            return $this->sendResponse(self::HTTP_NOT_ACCEPTABLE, null, 'User is already logged in on another device');
        }

        $this->getUserModel()->attachHandset($user, $handset, $pushToken);

        $this->getUserModel()->addSession($user);
        $this->getLoginHistoryModel()->trackLogin($user);
        $this->getAsteriskModel()->updateAuthTokenByName($user->getAuthToken(), $user->getVoipName());

        return $this->sendResponse(self::HTTP_STATUS_OK, array('user' => $this->collectUserInfo($user)), null);
    }

    public function forceLoginAction()
    {
        $data = $this->processPostData($this->getRequest());
        $username = isset($data['username']) ? $data['username'] : null;
        $password = isset($data['password']) ? $data['password'] : null;
        $pushToken = isset($data['pushToken']) ? $data['pushToken'] : null;
        $handsetID = isset($data['handsetID']) ? $data['handsetID'] : null;

        if (empty($username) || empty($password)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'Username and password are required');
        }

        if (empty($handsetID)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'HandsetID is required');
        }

        /** @var User $user */
        $user = $this->getUserModel()->findOneBy(array('username' => $username));
        if (empty($user)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, sprintf('User %s not found', $username));
        }

        if (!$user->getEnabled() || $user->getRole() != User::ROLE_MOBILE_USER) {
            return $this->sendResponse(self::HTTP_STATUS_FORBIDDEN, null, 'User has no rights to logged in mobile application');
        }

        if (is_null($user->getAuthToken())) {
            return $this->sendResponse(
                self::HTTP_STATUS_NOT_FOUND, null, sprintf('User %s does not logged in', $username)
            );
        }

        if (!(new Bcrypt())->verify($password, $user->getPassword())) {
            return $this->sendResponse(self::HTTP_STATUS_UNAUTHORIZED, null, 'Bad credentials');
        }

        /** @var Handset $handset */
        $handset = $this->getHandsetModel()->findOneBy(array('publicId' => $handsetID));
        if (empty($handset)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, sprintf('Handset %s not found', $handsetID));
        }

        $this->PushNotificationPlugin()->pushNotification(
            array('action' => 'forceLogout'),
            $user->getHandset() ? array($user->getHandset()->getPushToken()) : array(),
            'Authentication',
            1,
            null,
            false,
            true
        );

        $this->getUserModel()->attachHandset($user, $handset, $pushToken);

        $this->getUserModel()->addSession($user);
        $this->getLoginHistoryModel()->trackLogout($user);
        $this->getLoginHistoryModel()->trackLogin($user);
        $this->getAsteriskModel()->updateAuthTokenByName($user->getAuthToken(), $user->getVoipName());

        return $this->sendResponse(self::HTTP_STATUS_OK, array('user' => $this->collectUserInfo($user)), null);
    }

    public function logoutAction()
    {
        /** @var User $user */
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        if ($handset = $user->getHandset()) {
            $this->getUserModel()->detachHandset($user, $handset);
        }

        $this->getUserModel()->deleteSession($user);
        $this->getLoginHistoryModel()->trackLogout($user);
        $this->getAsteriskModel()->updateAuthTokenByName($user->getAuthToken(), $user->getVoipName());

        return $this->sendSuccessResponse();
    }

    public function getUsersAction()
    {
        $users = $this->getUserModel()->findBy(array('role' => User::ROLE_MOBILE_USER));

        if (empty($users)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'No users found');
        }

        $usersInfo = array();
        /** @var User $user */
        foreach ($users as $user) {
            $usersInfo[] = array(
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'sip-name' => $user->getVoipName(),
                'sip-caller-name' => $user->getVoipCallerId()
            );
        }

        return $this->sendResponse(self::HTTP_STATUS_OK, array('users' => $usersInfo), null);
    }

    public function getCoworkersAction()
    {
        if (!$this->checkAuthTokenAndReturnUser()) {
            return $this->sendInvalidTokenResponse();
        }

        $users = $this->getUserModel()->findLoggedMobileUsers();

        $usersData = array();
        /** @var User $user */
        foreach ($users as $user) {
            $usersData[] = array('id' => $user->getId(),
                'username' => $user->getUsername(),
                'sip-name' => $user->getVoipName(),
                'sip-caller-name' => $user->getVoipCallerId(),
                'deviceName' => $user->getHandset() ? $user->getHandset()->getDisplayName() : null
            );
        }

        return $this->sendResponse(self::HTTP_STATUS_OK, array('users' => $usersData), null);
    }

    public function takeAlertAction()
    {
        /** @var User $user */
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        $data = $this->processPostData($this->getRequest());
        $alertId = isset($data['alertId']) ? $data['alertId'] : null;

        if (empty($alertId)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'alertId is required');
        }

        /** @var Alert $alert */
        $alert = $this->getAlertModel()->findById($alertId);
        if (empty($alert)) {
            return $this->sendResponse(
                self::HTTP_STATUS_NOT_FOUND, null, sprintf('Alert with id:% not found', $alertId)
            );
        }

        if ($alert->getTaken()) {
            return $this->sendResponse(
                self::HTTP_NOT_ACCEPTABLE, null, 'Alert was already taken'
            );
        }

        $this->getAlertModel()->takeAlertByUser($alert, $user);

        if ($alert->getDelayClose()) {
            $this->getAlertModel()->closeAlert($alert);
            if ($alert->getDevice() && $alert->getDevice()->getWorkflow() === Device::WORKFLOW_NO_ACTION) {
                $this->getAlertModel()->completeAlert($alert);
            }
        }

        if ($alert->getType() === 'Alert') {
            $this->sendAlertPushNotification($alert);
        }

        return $this->sendSuccessResponse();
    }

    public function completeAlertAction()
    {
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        $data = $this->processPostData($this->getRequest());
        $alertId = isset($data['alertId']) ? $data['alertId'] : null;

        if (empty($alertId)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'alertId is required');
        }

        /** @var Alert $alert */
        $alert = $this->getAlertModel()->findById($alertId);
        if (empty($alert)) {
            return $this->sendResponse(
                self::HTTP_STATUS_NOT_FOUND, null, sprintf('Alert with id:% not found', $alertId)
            );
        }

        $this->getAlertModel()->completeAlert($alert);

        if ($alert->getType() === 'Alert') {
            $this->sendAlertPushNotification($alert);
        }

        return $this->sendSuccessResponse();
    }

    public function getZonesAction()
    {
        /** @var User $user */
        if (!$user = $this->checkAuthTokenAndReturnUser()) {
            return $this->sendInvalidTokenResponse();
        }

        $zones = $this->getZoneModel()->findAll();
        if (empty($zones)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'No zones found');
        }

        $userZones = $user->getZonesIds();

        $zonesInfo = array();
        /** @var Zone $zone */
        foreach ($zones as $zone) {
            $zonesInfo[] = array(
                'id' => $zone->getId(),
                'zoneName' => $zone->getName(),
                'zoneSound' => $zone->getSound(),
                'active' => in_array($zone->getId(), $userZones) ? 1 : 0
            );
        }

        return $this->sendResponse(self::HTTP_STATUS_OK, array('zones' => $zonesInfo), null);
    }

    public function getAlertsAction()
    {
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        $data = $this->processPostData($this->getRequest());
        $zonesIds = isset($data['zonesIds']) ? $data['zonesIds'] : null;

        if (empty($zonesIds)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'zonesIds is required');
        }

        $alerts = $this->getAlertModel()->findUncompletedByZones($zonesIds);

        $alertsInfo = array();
         /** @var Alert $alert */
        foreach ($alerts as $alert) {
            if($alert->getType() === 'Alert') {
                $alertsInfo[] = $this->getAlertInfo($alert);
            }
        }

        return $this->sendResponse(self::HTTP_STATUS_OK, array('alerts' => $alertsInfo), null);
    }

    public function attachZonesToUserAction()
    {
        /** @var User $user */
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        $data = $this->processPostData($this->getRequest());
        $zonesIds = isset($data['zonesIds']) ? $data['zonesIds'] : null;

        if (empty($zonesIds)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'zonesIds is required');
        }

        $this->getUserModel()->attachZones($user, $zonesIds);

        return $this->sendSuccessResponse();
    }

    public function registerPushNotificationAction()
    {
        /** @var User $user */
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        $data = $this->processPostData($this->getRequest());
        $pushToken = isset($data['pushToken']) ? $data['pushToken'] : null;

        if (strlen($pushToken) != PushNotificationPlugin::PUSH_TOKEN_LENGTH) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'Push Token must be 64 characters');
        }

        /** @var Handset $handset */
        $handset = $this->getHandsetModel()->findOneBy(array('user' => $user));
        if (empty($handset)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'Handset does not exist');
        }

        $this->getHandsetModel()->update($handset, array('pushToken' => $pushToken));

        return $this->sendSuccessResponse();
    }

    protected function collectUserInfo(User $user)
    {
        return array(
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'authToken' => $user->getAuthToken(),
            'sip-name' => $user->getVoipName(),
            'sip-caller-name' => $user->getVoipCallerId(),
            'sip-password' => $user->getVoipPassword()
        );
    }

    public function checkAuthTokenAction()
    {
        return $this->checkAuthTokenAndReturnUser()
            ? $this->sendSuccessResponse()
            : $this->sendInvalidTokenResponse();
    }

    public function sendAsteriskPushAction()
    {
        /** @var User $user */
        $user = $this->checkAuthTokenAndReturnUser();
        if (empty($user)) {
            return $this->sendInvalidTokenResponse();
        }

        $handset = $user->getHandset();
        if (empty($handset)) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'No handset found for user');
        }

        $data = $this->processPostData($this->getRequest());
        $messageType = isset($data['type']) ? $data['type'] : null;

        if (!isset($this->asteriskMessages[$messageType])) {
            return $this->sendResponse(self::HTTP_STATUS_NOT_FOUND, null, 'Incorrect type param');
        }

        /** @var User $callerUser */
        $callerUser = $this->checkAuthTokenAndReturnUser('authTokenFrom');
        $callerUserName = $callerUser ? $callerUser->getUsername() : null;

        $this->PushNotificationPlugin()->pushNotification(
            array('action' => $messageType),
            array($handset->getPushToken()),
            sprintf($this->asteriskMessages[$messageType], $callerUserName),
            1,
            Zone::SOUND_TRITONE,
            true,
            false
        );

        return $this->sendSuccessResponse();
    }

    private function checkAuthTokenAndReturnUser($tokenName = 'authToken')
    {
        $data = $this->processPostData($this->getRequest());
        if (!isset($data[$tokenName])) {
            return null;
        }

        return $this->getUserModel()->findOneBy(array('authToken' => $data[$tokenName]));
    }
}