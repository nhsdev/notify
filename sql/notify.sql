use `notify`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alerts
-- ----------------------------
DROP TABLE IF EXISTS `alerts`;
CREATE TABLE `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `button_id` int(11) NOT NULL,
  `hop_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `opened` datetime DEFAULT NULL,
  `taken` datetime DEFAULT NULL,
  `to_take` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `to_close` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `completed` datetime DEFAULT NULL,
  `to_complete` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F77AC06BA123E519` (`button_id`),
  KEY `IDX_F77AC06BBC3870B6` (`hop_id`),
  KEY `IDX_F77AC06BA76ED395` (`user_id`),
  KEY `IDX_F77AC06B64D218E` (`location_id`),
  CONSTRAINT `FK_F77AC06B64D218E` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_F77AC06BA123E519` FOREIGN KEY (`button_id`) REFERENCES `buttons` (`id`),
  CONSTRAINT `FK_F77AC06BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_F77AC06BBC3870B6` FOREIGN KEY (`hop_id`) REFERENCES `hops` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for buttons
-- ----------------------------
DROP TABLE IF EXISTS `buttons`;
CREATE TABLE `buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `public_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3A06AC3D9F35DE89` (`public_id`),
  UNIQUE KEY `UNIQ_3A06AC3D64D218E` (`location_id`),
  CONSTRAINT `FK_3A06AC3D64D218E` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=705 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for handsets
-- ----------------------------
DROP TABLE IF EXISTS `handsets`;
CREATE TABLE `handsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `public_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `push_token` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6D8E54B5DF09E16C` (`public_id`),
  UNIQUE KEY `UNIQ_6D8E54B55E237E06` (`name`),
  UNIQUE KEY `UNIQ_6D8E54B5A76ED395` (`user_id`),
  CONSTRAINT `FK_6D8E54B5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for hops
-- ----------------------------
DROP TABLE IF EXISTS `hops`;
CREATE TABLE `hops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('repeater','receiver') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'repeater',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BE545DE33B01599` (`public_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for locations
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `behavior` enum('mobile','static') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mobile',
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5E9E89CBDE27CB46` (`name`),
  KEY `IDX_5E9E89CB9F2C3FAB` (`zone_id`),
  CONSTRAINT `FK_5E9E89CB9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1727 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for login_history
-- ----------------------------
DROP TABLE IF EXISTS `login_history`;
CREATE TABLE `login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` datetime NOT NULL,
  `logout` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `console_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_unlock_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wall_mode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `session` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voip_name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voip_password` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `handset_id` int(11) DEFAULT NULL,
  `role` enum('admin','user','mobile user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mobile user',
  `voip_caller_id` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`name`),
  UNIQUE KEY `UNIQ_8D93D649D044D5D4` (`session`),
  UNIQUE KEY `UNIQ_8D93D649EDF51E90` (`auth_token`),
  UNIQUE KEY `UNIQ_8D93D6497B9FCB0B` (`voip_name`),
  UNIQUE KEY `UNIQ_8D93D6497103CFD1` (`handset_id`),
  CONSTRAINT `FK_8D93D6497103CFD1` FOREIGN KEY (`handset_id`) REFERENCES `handsets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for users_zones
-- ----------------------------
DROP TABLE IF EXISTS `users_zones`;
CREATE TABLE `users_zones` (
  `zone_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`zone_id`),
  KEY `IDX_DA6A8CCE9F2C3FAB` (`zone_id`),
  KEY `IDX_DA6A8CCEA76ED395` (`user_id`),
  CONSTRAINT `FK_DA6A8CCE9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DA6A8CCEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for zones
-- ----------------------------
DROP TABLE IF EXISTS `zones`;
CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sound` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A0EBC007CCDBBFA3` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
