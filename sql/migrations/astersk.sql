SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cdr
-- ----------------------------
DROP TABLE IF EXISTS `cdr`;
CREATE TABLE `cdr` (
  `calldate`    DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clid`        VARCHAR(80)  NOT NULL DEFAULT '',
  `src`         VARCHAR(80)  NOT NULL DEFAULT '',
  `dst`         VARCHAR(80)  NOT NULL DEFAULT '',
  `dcontext`    VARCHAR(80)  NOT NULL DEFAULT '',
  `channel`     VARCHAR(80)  NOT NULL DEFAULT '',
  `dstchannel`  VARCHAR(80)  NOT NULL DEFAULT '',
  `lastapp`     VARCHAR(80)  NOT NULL DEFAULT '',
  `lastdata`    VARCHAR(80)  NOT NULL DEFAULT '',
  `duration`    INT(11)      NOT NULL DEFAULT '0',
  `billsec`     INT(11)      NOT NULL DEFAULT '0',
  `disposition` VARCHAR(45)  NOT NULL DEFAULT '',
  `amaflags`    INT(11)      NOT NULL DEFAULT '0',
  `accountcode` VARCHAR(20)  NOT NULL DEFAULT '',
  `uniqueid`    VARCHAR(32)  NOT NULL DEFAULT '',
  `userfield`   VARCHAR(255) NOT NULL DEFAULT ''
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

-- ----------------------------
-- Table structure for sip_buddies
-- ----------------------------
DROP TABLE IF EXISTS `sip_buddies`;
CREATE TABLE `sip_buddies` (
  `id`                INT(11)     NOT NULL AUTO_INCREMENT,
  `name`              VARCHAR(80) NOT NULL,
  `callerid`          VARCHAR(80) DEFAULT NULL,
  `defaultuser`       VARCHAR(80) NOT NULL,
  `regexten`          VARCHAR(80) NOT NULL,
  `secret`            VARCHAR(80) DEFAULT NULL,
  `accountcode`       VARCHAR(20) DEFAULT NULL,
  `context`           VARCHAR(80) DEFAULT 'default',
  `amaflags`          VARCHAR(7) DEFAULT NULL,
  `callgroup`         VARCHAR(10) DEFAULT NULL,
  `canreinvite`       CHAR(3) DEFAULT 'yes',
  `defaultip`         VARCHAR(15) DEFAULT NULL,
  `dtmfmode`          VARCHAR(7) DEFAULT NULL,
  `fromuser`          VARCHAR(80) DEFAULT NULL,
  `fromdomain`        VARCHAR(80) DEFAULT NULL,
  `fullcontact`       VARCHAR(80) DEFAULT NULL,
  `host`              VARCHAR(31) NOT NULL DEFAULT 'dynamic',
  `insecure`          VARCHAR(4) DEFAULT NULL,
  `language`          CHAR(2) DEFAULT NULL,
  `mailbox`           VARCHAR(50) DEFAULT NULL,
  `md5secret`         VARCHAR(80) DEFAULT NULL,
  `nat`               VARCHAR(20) NOT NULL DEFAULT 'force_rport,comedia',
  `deny`              VARCHAR(95) DEFAULT NULL,
  `permit`            VARCHAR(95) DEFAULT NULL,
  `mask`              VARCHAR(95) DEFAULT NULL,
  `pickupgroup`       VARCHAR(10) DEFAULT NULL,
  `port`              VARCHAR(5)  NOT NULL,
  `qualify`           CHAR(3) DEFAULT NULL,
  `restrictcid`       CHAR(1) DEFAULT NULL,
  `rtptimeout`        CHAR(3) DEFAULT NULL,
  `rtpholdtimeout`    CHAR(3) DEFAULT NULL,
  `type`              VARCHAR(6)  NOT NULL DEFAULT 'friend',
  `disallow`          VARCHAR(100) DEFAULT 'all',
  `allow`             VARCHAR(100) DEFAULT 'g729;speex;gsm;ulaw;alaw',
  `musiconhold`       VARCHAR(100) DEFAULT 'default',
  `regseconds`        INT(11)     NOT NULL DEFAULT '0',
  `ipaddr`            VARCHAR(15) NOT NULL,
  `cancallforward`    CHAR(3) DEFAULT 'yes',
  `lastms`            INT(11)     NOT NULL DEFAULT '0',
  `useragent`         CHAR(255) DEFAULT NULL,
  `regserver`         VARCHAR(100) DEFAULT NULL,
  `callbackextension` VARCHAR(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
)
  ENGINE =MyISAM
  AUTO_INCREMENT =893
  DEFAULT CHARSET =utf8;
