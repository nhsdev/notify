SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert
-- ----------------------------
DROP TABLE IF EXISTS `alert`;
CREATE TABLE `alert` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `button_id`   VARCHAR(255)
                COLLATE utf8_unicode_ci NOT NULL,
  `opened`      DATETIME DEFAULT NULL,
  `taken`       DATETIME DEFAULT NULL,
  `closed`      DATETIME DEFAULT NULL,
  `completed`   DATETIME DEFAULT NULL,
  `user_id`     INT(11) DEFAULT NULL,
  `location_id` INT(11) DEFAULT NULL,
  `toTake`      VARCHAR(50)
                COLLATE utf8_unicode_ci DEFAULT NULL,
  `toClose`     VARCHAR(50)
                COLLATE utf8_unicode_ci DEFAULT NULL,
  `toComplete`  VARCHAR(50)
                COLLATE utf8_unicode_ci DEFAULT NULL,
  `hop_id`      INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_17FD46C1A76ED395` (`user_id`),
  KEY `IDX_17FD46C164D218E` (`location_id`),
  KEY `IDX_17FD46C1BC3870B6` (`hop_id`),
  CONSTRAINT `FK_17FD46C164D218E` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`)
    ON DELETE SET NULL,
  CONSTRAINT `FK_17FD46C1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE SET NULL,
  CONSTRAINT `FK_17FD46C1BC3870B6` FOREIGN KEY (`hop_id`) REFERENCES `hop` (`id`)
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  AUTO_INCREMENT =125
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for button
-- ----------------------------
DROP TABLE IF EXISTS `button`;
CREATE TABLE `button` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `location_id` INT(11) DEFAULT NULL,
  `buttonId`    VARCHAR(255)
                COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3A06AC3D9F35DE89` (`buttonId`),
  UNIQUE KEY `UNIQ_3A06AC3D64D218E` (`location_id`),
  CONSTRAINT `FK_3A06AC3D64D218E` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`)
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  AUTO_INCREMENT =6
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for handset
-- ----------------------------
DROP TABLE IF EXISTS `handset`;
CREATE TABLE `handset` (
  `id`        INT(11) NOT NULL AUTO_INCREMENT,
  `user_id`   INT(11) DEFAULT NULL,
  `handsetId` VARCHAR(255)
              COLLATE utf8_unicode_ci NOT NULL,
  `name`      VARCHAR(255)
              COLLATE utf8_unicode_ci NOT NULL,
  `pushToken` VARCHAR(255)
              COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6D8E54B5DF09E16C` (`handsetId`),
  UNIQUE KEY `UNIQ_6D8E54B55E237E06` (`name`),
  UNIQUE KEY `UNIQ_6D8E54B5A76ED395` (`user_id`),
  CONSTRAINT `FK_6D8E54B5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  AUTO_INCREMENT =6
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for hop
-- ----------------------------
DROP TABLE IF EXISTS `hop`;
CREATE TABLE `hop` (
  `id`    INT(11) NOT NULL AUTO_INCREMENT,
  `hopId` VARCHAR(255)
          COLLATE utf8_unicode_ci NOT NULL,
  `name`  VARCHAR(255)
          COLLATE utf8_unicode_ci DEFAULT NULL,
  `type`  VARCHAR(255)
          COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BE545DE33B01599` (`hopId`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =10
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `zone_id`       INT(11) DEFAULT NULL,
  `location_name` VARCHAR(255)
                  COLLATE utf8_unicode_ci DEFAULT NULL,
  `behavior`      VARCHAR(255)
                  COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled`       TINYINT(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5E9E89CBDE27CB46` (`location_name`),
  KEY `IDX_5E9E89CB9F2C3FAB` (`zone_id`),
  CONSTRAINT `FK_5E9E89CB9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =11
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for login_history
-- ----------------------------
DROP TABLE IF EXISTS `login_history`;
CREATE TABLE `login_history` (
  `id`       INT(11) NOT NULL AUTO_INCREMENT,
  `employee` VARCHAR(255)
             COLLATE utf8_unicode_ci NOT NULL,
  `handset`  VARCHAR(255)
             COLLATE utf8_unicode_ci DEFAULT NULL,
  `login`    DATETIME NOT NULL,
  `logout`   DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =88
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `server_ip`         VARCHAR(50)
                      COLLATE utf8_unicode_ci DEFAULT NULL,
  `console_name`      VARCHAR(50)
                      COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_unlock_code` VARCHAR(50)
                      COLLATE utf8_unicode_ci DEFAULT NULL,
  `wall_mode`         INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `username`     VARCHAR(255)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email`        VARCHAR(255)
                 COLLATE utf8_unicode_ci NOT NULL,
  `displayName`  VARCHAR(50)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password`     VARCHAR(128)
                 COLLATE utf8_unicode_ci NOT NULL,
  `session`      VARCHAR(50)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  `authToken`    VARCHAR(50)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  `voipName`     VARCHAR(255)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  `voipPassword` VARCHAR(128)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin`          INT(11) DEFAULT NULL,
  `enabled`      TINYINT(1) NOT NULL,
  `handset_id`   INT(11) DEFAULT NULL,
  `role`         VARCHAR(128)
                 COLLATE utf8_unicode_ci NOT NULL,
  `callerId`     VARCHAR(80)
                 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649D044D5D4` (`session`),
  UNIQUE KEY `UNIQ_8D93D649EDF51E90` (`authToken`),
  UNIQUE KEY `UNIQ_8D93D6497B9FCB0B` (`voipName`),
  UNIQUE KEY `UNIQ_8D93D6497103CFD1` (`handset_id`),
  CONSTRAINT `FK_8D93D6497103CFD1` FOREIGN KEY (`handset_id`) REFERENCES `handset` (`id`)
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  AUTO_INCREMENT =25
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for user_zone
-- ----------------------------
DROP TABLE IF EXISTS `user_zone`;
CREATE TABLE `user_zone` (
  `zone_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `zone_id`),
  KEY `IDX_DA6A8CCE9F2C3FAB` (`zone_id`),
  KEY `IDX_DA6A8CCEA76ED395` (`user_id`),
  CONSTRAINT `FK_DA6A8CCE9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `FK_DA6A8CCEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;

-- ----------------------------
-- Table structure for zone
-- ----------------------------
DROP TABLE IF EXISTS `zone`;
CREATE TABLE `zone` (
  `id`        INT(11) NOT NULL AUTO_INCREMENT,
  `zone_name` VARCHAR(255)
              COLLATE utf8_unicode_ci NOT NULL,
  `sound`     VARCHAR(255)
              COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A0EBC007CCDBBFA3` (`zone_name`)
)
  ENGINE =InnoDB
  AUTO_INCREMENT =10
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;