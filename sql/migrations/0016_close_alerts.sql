BEGIN;
  use `notify`;

  ALTER TABLE `alerts`
        ADD COLUMN `is_closed` TINYINT(1) DEFAULT NULL AFTER `to_close`;

COMMIT;