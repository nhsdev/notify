BEGIN;
  use `notify`;
  ALTER TABLE `alerts` 
    ADD COLUMN `ask_date_time` VARCHAR(80) NULL DEFAULT NULL AFTER `date_time`;
COMMIT;