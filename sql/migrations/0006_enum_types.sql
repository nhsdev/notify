BEGIN;
  ALTER TABLE hops CHANGE type type ENUM('repeater', 'receiver') NOT NULL DEFAULT 'repeater';
  UPDATE locations SET behavior = LOWER(behavior);
  ALTER TABLE locations CHANGE behavior behavior ENUM('mobile', 'static') NOT NULL DEFAULT 'mobile';
  ALTER TABLE users CHANGE role role ENUM('admin', 'user', 'mobile user') NOT NULL DEFAULT 'mobile user';
COMMIT;