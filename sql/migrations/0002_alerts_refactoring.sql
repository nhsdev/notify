
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for alerts
-- ----------------------------
DROP TABLE IF EXISTS `alerts`;
CREATE TABLE `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `button_id` int(11) NOT NULL,
  `hop_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `opened` datetime DEFAULT NULL,
  `taken` datetime DEFAULT NULL,
  `to_take` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `to_close` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `completed` datetime DEFAULT NULL,
  `to_complete` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F77AC06BA123E519` (`button_id`),
  KEY `IDX_F77AC06BBC3870B6` (`hop_id`),
  KEY `IDX_F77AC06BA76ED395` (`user_id`),
  KEY `IDX_F77AC06B64D218E` (`location_id`),
  CONSTRAINT `FK_F77AC06B64D218E` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_F77AC06BA123E519` FOREIGN KEY (`button_id`) REFERENCES `buttons` (`id`),
  CONSTRAINT `FK_F77AC06BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_F77AC06BBC3870B6` FOREIGN KEY (`hop_id`) REFERENCES `hops` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
