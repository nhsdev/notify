BEGIN;
  ALTER TABLE locations
    ADD palatium_care_id INT AFTER id,
    ADD CONSTRAINT location_palatium_care_id UNIQUE (palatium_care_id);

  SET FOREIGN_KEY_CHECKS=0;

  ALTER TABLE buttons
    ADD palatium_care_id INT AFTER id,
    ADD CONSTRAINT button_palatium_care_id UNIQUE (palatium_care_id),
    DROP INDEX UNIQ_3A06AC3D64D218E,
    DROP FOREIGN KEY FK_3A06AC3D64D218E,
    ADD CONSTRAINT fk_button_location FOREIGN KEY (location_id) REFERENCES locations(id) ON DELETE SET NULL;

  SET FOREIGN_KEY_CHECKS=1;

  ALTER TABLE alerts
      ADD palatium_care_id INT AFTER id,
      ADD near VARCHAR(255) DEFAULT NULL AFTER hop_id,
      ADD CONSTRAINT alert_palatium_care_id UNIQUE (palatium_care_id);
COMMIT;