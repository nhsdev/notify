DELIMITER //
DROP TRIGGER IF EXISTS location_bi_zone_id//
DROP TRIGGER IF EXISTS location_bu_zone_id//
CREATE DEFINER=root@localhost TRIGGER location_bi_zone_id
    BEFORE INSERT ON `locations`
    FOR EACH ROW

BEGIN
    if (new.zone_id is null) then
      set new.zone_id = (select z.id from zones z where z.name='Zone1');
    end if;
END//
CREATE DEFINER=root@localhost TRIGGER location_bu_zone_id
    BEFORE UPDATE ON `locations`
    FOR EACH ROW

BEGIN
    if (new.zone_id is null) then
      set new.zone_id = (select z.id from zones z where z.name='Zone1');
    end if;
END//
DELIMITER ;