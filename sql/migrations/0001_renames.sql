BEGIN;
  RENAME TABLE alert TO alerts;
  RENAME TABLE button TO buttons;
  RENAME TABLE handset TO handsets;
  RENAME TABLE hop TO hops;
  RENAME TABLE location TO locations;
  RENAME TABLE user TO users;
  RENAME TABLE zone TO zones;
  RENAME TABLE user_zone TO users_zones;
COMMIT;

ALTER TABLE locations CHANGE location_name name VARCHAR(255) NOT NULL;
ALTER TABLE locations CHANGE enabled enabled TINYINT(1) NOT NULL;

ALTER TABLE zones CHANGE zone_name name VARCHAR(255) NOT NULL;

ALTER TABLE alerts CHANGE toTake to_take VARCHAR(32);
ALTER TABLE alerts CHANGE toClose to_close VARCHAR(32);
ALTER TABLE alerts CHANGE toComplete to_complete VARCHAR(32);

ALTER TABLE buttons CHANGE buttonId public_id VARCHAR(255) NOT NULL;

ALTER TABLE hops CHANGE hopId public_id VARCHAR(255) NOT NULL;

ALTER TABLE handsets CHANGE handsetId public_id VARCHAR(255) NOT NULL;
ALTER TABLE handsets CHANGE pushToken push_token VARCHAR(255);

ALTER TABLE users CHANGE username name VARCHAR(255) NOT NULL;
ALTER TABLE users CHANGE displayName display_name VARCHAR(50);
ALTER TABLE users CHANGE authToken auth_token VARCHAR(50);
ALTER TABLE users CHANGE voipName voip_name VARCHAR(80);
ALTER TABLE users CHANGE voipPassword voip_password VARCHAR(80);
ALTER TABLE users CHANGE callerId voip_caller_id VARCHAR(80);