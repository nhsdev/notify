BEGIN;
  use `notify`;
  ALTER TABLE zones
      ADD sound_count INT(3) DEFAULT NULL,
      ADD sound_interval INT(11) DEFAULT NULL;
COMMIT;
