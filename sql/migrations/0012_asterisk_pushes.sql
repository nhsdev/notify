BEGIN;
  use `asterisk`;
  ALTER TABLE sip_buddies
      ADD auth_token VARCHAR(50) DEFAULT NULL AFTER secret;
COMMIT;