BEGIN;
  use `notify`;
  SET FOREIGN_KEY_CHECKS=0;

  ALTER TABLE `alerts`
      ADD `device_id` INT(11) DEFAULT NULL,
      ADD `date_time` VARCHAR(80) DEFAULT NULL,
      ADD `type` VARCHAR(80) DEFAULT NULL,
      DROP INDEX `IDX_F77AC06BA123E519`,
      DROP FOREIGN KEY `FK_F77AC06BA123E519`,
      DROP COLUMN `button_id`,
      ADD INDEX `alert_device` (`device_id`),
      ADD UNIQUE INDEX `alert_date_time` (`date_time`),
      ADD CONSTRAINT `fk_alert_device` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE SET NULL;

  SET FOREIGN_KEY_CHECKS=1;

  CREATE TABLE `devices` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(255) NOT NULL,
    `workflow` ENUM('action','no action') NOT NULL DEFAULT 'no action',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `device_type` (`type`)
  );

  CREATE TABLE `residents` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `firstName` VARCHAR(255) NOT NULL,
    `lastName` VARCHAR(255) NULL DEFAULT NULL,
    `nickname` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
  );

  CREATE TABLE `alerts_residents` (
    `resident_id` INT(11) NOT NULL,
    `alert_id` INT(11) NOT NULL,
    PRIMARY KEY (`alert_id`, `resident_id`),
    INDEX `IDX_alert` (`alert_id`),
    INDEX `IDX_resident` (`resident_id`),
    CONSTRAINT `FK_alert` FOREIGN KEY (`alert_id`) REFERENCES `alerts` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_residents` FOREIGN KEY (`resident_id`) REFERENCES `residents` (`id`) ON DELETE CASCADE
  );

COMMIT;