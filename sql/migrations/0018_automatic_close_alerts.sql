USE `notify`;

DELIMITER $$
DROP PROCEDURE IF EXISTS closeAlerts $$
CREATE PROCEDURE `closeAlerts`()
LANGUAGE SQL
NOT DETERMINISTIC
MODIFIES SQL DATA
  SQL SECURITY DEFINER
  COMMENT 'Automatic close alerts'
  BEGIN
    DECLARE colName TEXT;
    DECLARE cId INTEGER;
    DECLARE cOpened DATETIME;
    DECLARE cTaken DATETIME;
    DECLARE cToTake VARCHAR(32);
    DECLARE cClosed DATETIME;
    DECLARE cToClose VARCHAR(32);
    DECLARE cIsClosed TINYINT(1);
    DECLARE cCompleted DATETIME;
    DECLARE cToComplete VARCHAR(32);
    DECLARE cTyp VARCHAR(80);
    DECLARE cWorkflow VARCHAR(80) DEFAULT NULL;
    DECLARE done INT DEFAULT FALSE;
    DECLARE AlertsCursor CURSOR FOR
      SELECT
        `alerts`.`id`,
        `alerts`.`opened`,
        `alerts`.`taken`,
        `alerts`.`to_take`,
        `alerts`.`closed`,
        `alerts`.`to_close`,
        `alerts`.`is_closed`,
        `alerts`.`completed`,
        `alerts`.`to_complete`,
        `alerts`.`type`,
        `devices`.`workflow`
      FROM `alerts`
        LEFT JOIN `devices` ON `alerts`.`device_id` = `devices`.`id`
      WHERE (`alerts`.`closed` IS NULL)
            AND (`alerts`.`ask_date_time` IS NOT NULL)
            AND (UNIX_TIMESTAMP(`alerts`.`ask_date_time`) <= UNIX_TIMESTAMP(NOW()));

    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = TRUE;


    SELECT `column_name`
    INTO colName
    FROM `information_schema`.`COLUMNS`
    WHERE `table_schema` = DATABASE()
          AND `table_name` = 'alerts'
          AND `column_name` = 'ask_date_time';

    IF colName IS NULL
    THEN
      SIGNAL SQLSTATE 'ERR0R'
      SET MESSAGE_TEXT = `Not exists column 'ask_date_time' in db 'notify'`;
    END IF;


    OPEN AlertsCursor;
    WHILE done = FALSE DO FETCH AlertsCursor
    INTO cId, cOpened, cTaken, cToTake, cClosed, cToClose, cIsClosed, cCompleted, cToComplete, cTyp, cWorkflow;

      IF cClosed IS NOT NULL THEN
        SET @closed = cClosed;
      ELSE
        SET @closed = NOW();
      END IF;
      SET @toClose = TIMEDIFF(@closed, cOpened);

      SET @completed = cCompleted;
      IF (cWorkflow = 'no action' OR cTyp <> 'Alert') AND cCompleted IS NULL THEN
        SET @completed = NOW();
      END IF;
      SET @toComplete = TIMEDIFF(@completed, cOpened);

      IF cIsClosed <> 1 OR cIsClosed IS NULL THEN SET @isClosed = 1; END IF;

      IF (cTyp = 'Alert' AND cTaken IS NOT NULL) OR (cTyp <> 'Alert') THEN
        UPDATE `notify`.`alerts`
        SET
          `alerts`.`closed`      = @closed,
          `alerts`.`to_close`    = @toClose,
          `alerts`.`completed`   = @completed,
          `alerts`.`to_complete` = @toComplete,
          `alerts`.`is_closed`   = @isClosed
        WHERE `alerts`.`id` = cId;
      END IF;

    END WHILE;
    CLOSE AlertsCursor;
  END $$
DELIMITER ;