
USE `notify-palatium-care`;

DELIMITER $$

DROP PROCEDURE IF EXISTS importPalatiumCareData;
CREATE PROCEDURE importPalatiumCareData(zone_name VARCHAR(255))

BEGIN
-- Declaration section
    DECLARE done INT DEFAULT FALSE;
    DECLARE _location_name VARCHAR(255);
    DECLARE _button_public_id VARCHAR(255);
    DECLARE _zone_id INT;
    DECLARE _location_id INT;
    DECLARE _palatium_care_id INT;

    DECLARE cursor_locations CURSOR FOR
        SELECT
            LocationId AS palatium_care_id,
            LocationName AS location_name
        FROM _pc_locations;

    DECLARE cursor_buttons CURSOR FOR
        SELECT
            DeviceId AS palatium_care_id,
            DeviceKey AS button_public_id,
            LocationName AS location_name
        FROM (
             SELECT d.DeviceId, d.DeviceKey, l.LocationName  FROM _pc_devices d
                 JOIN _pc_device_uses du ON d.DeviceUseId = du.DeviceUseId
                 JOIN _pc_locations l ON d.LocationId = l.LocationId
             WHERE du.DeviceUseShortName IN ("Pull Cord", "Push Cord")

             UNION ALL

             SELECT d.DeviceId, d.DeviceKey, l.LocationName FROM _pc_devices d
                 JOIN _pc_device_uses du ON d.DeviceUseId = du.DeviceUseId
                 JOIN _pc_residents r ON d.ResidentId = r.ResidentId
                 JOIN _pc_locations l ON r.LocationId = l.LocationId
             WHERE du.DeviceUseShortName IN ("Pendant")
        ) devices
        ORDER BY palatium_care_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
-- End declaration section

-- Start process
    SELECT id FROM zones WHERE name = zone_name COLLATE utf8_unicode_ci INTO _zone_id;

#     IF _zone_id IS NULL THEN
#         SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = `Invalid zone name`;
#     END IF;

-- begin location execution --
    OPEN cursor_locations;

    fetch_locations: LOOP
        FETCH cursor_locations INTO _palatium_care_id, _location_name;

        IF done THEN
            SET done = FALSE;
            LEAVE fetch_locations;
        END IF;

        INSERT IGNORE INTO locations VALUES ('', _palatium_care_id, _zone_id, _location_name, 'mobile', 1);
    END LOOP fetch_locations;

    CLOSE cursor_locations;
-- end location execution --

-- begin buttons execution --
    OPEN cursor_buttons;

    fetch_buttons: LOOP
        FETCH cursor_buttons INTO _palatium_care_id, _button_public_id, _location_name;

        IF done THEN
            SET done = FALSE;
            LEAVE fetch_buttons;
        END IF;

        SET _location_id = NULL;
        IF _location_name IS NOT NULL THEN
            SELECT id FROM locations WHERE name = _location_name COLLATE utf8_unicode_ci INTO _location_id;
        END IF;

        INSERT IGNORE INTO buttons VALUES ('', _palatium_care_id, _location_id, _button_public_id);
    END LOOP fetch_buttons;

    CLOSE cursor_buttons;
-- end buttons execution --
END;
$$

DELIMITER ;

CALL importPalatiumCareData('Zone1');