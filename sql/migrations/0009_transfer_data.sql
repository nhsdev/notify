
USE `notify`;

DELIMITER $$

DROP PROCEDURE IF EXISTS buttonsTransfer;
CREATE PROCEDURE buttonsTransfer(zone_name VARCHAR(255))

BEGIN
-- Declaration section
    DECLARE done INT DEFAULT FALSE;
    DECLARE _location_name VARCHAR(255);
    DECLARE _button_public_id VARCHAR(255);
    DECLARE _zone_id INT;
    DECLARE _location_id INT;

    DECLARE cursor_locations CURSOR FOR
        SELECT DISTINCT l.LocationName AS location_name
            FROM _devices_to_transfer d
        JOIN _locations_to_transfer l ON d.LocationId = l.LocationId
            WHERE d.DeviceTypeKey IN ('INV-00', 'INV-19', 'INV-18', 'INV-RC', 'INV-RP', 'INV-0C');

    DECLARE cursor_buttons CURSOR FOR
        SELECT LOWER(SUBSTRING(d.deviceKey, 8)) AS button_public_id, l.LocationName AS location_name
            FROM _devices_to_transfer d
        JOIN _locations_to_transfer l ON d.LocationId = l.LocationId
            WHERE d.DeviceTypeKey IN ('INV-00', 'INV-19', 'INV-18', 'INV-RC', 'INV-RP', 'INV-0C');

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

-- End declaration section

-- Start process
    SELECT id FROM zones WHERE name = zone_name COLLATE utf8_unicode_ci INTO _zone_id;

-- begin location execution --
    OPEN cursor_locations;

    fetch_locations: LOOP
      FETCH cursor_locations INTO _location_name;

      IF done THEN
          SET done = FALSE;
          LEAVE fetch_locations;
      END IF;

      INSERT IGNORE INTO locations VALUES ('', _zone_id, _location_name, 'mobile', 1);
    END LOOP fetch_locations;

    CLOSE cursor_locations;
-- end location execution --

-- begin buttons execution --
    OPEN cursor_buttons;

    fetch_buttons: LOOP
        FETCH cursor_buttons INTO _button_public_id, _location_name;

        IF done THEN
            SET done = FALSE;
            LEAVE fetch_buttons;
        END IF;

        SELECT id FROM locations WHERE name = _location_name COLLATE utf8_unicode_ci INTO _location_id;

        INSERT IGNORE INTO buttons VALUES ('', _location_id, _button_public_id);
    END LOOP fetch_buttons;

    CLOSE cursor_buttons;

END;
$$

DELIMITER ;

CALL buttonsTransfer('Zone1');

DROP PROCEDURE buttonsTransfer;