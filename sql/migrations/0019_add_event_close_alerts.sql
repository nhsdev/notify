USE `notify`;

SET GLOBAL event_scheduler = ON;

DELIMITER $$
DROP EVENT IF EXISTS autoCloseAlerts $$
CREATE EVENT `autoCloseAlerts`
  ON SCHEDULE EVERY 1 MINUTE
  ON COMPLETION PRESERVE
  ENABLE
  COMMENT 'Exec automatic close alerts procedure'
DO BEGIN
  CALL closeAlerts();
END $$
DELIMITER ;