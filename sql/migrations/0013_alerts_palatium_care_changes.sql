BEGIN;
  SET FOREIGN_KEY_CHECKS=0;

  ALTER TABLE alerts DROP INDEX alert_palatium_care_id;
  CREATE INDEX alert_palatium_care_id ON alerts (palatium_care_id);

  SET FOREIGN_KEY_CHECKS=1;
COMMIT;